# SavePyVideo.org
## Hopefully a drop-in replacement for PyVideo.org

This is a quick and dirty proof of concept to replace PyVideo.org with a static
site. Right now it's mostly just cleaning up data and getting organized, thus
the mess.

## Current Goals
- static site
- a single yaml or ini for each video
    - contains all metadata about the video
    - jinja template builds into proper html page
- merge request to add new or update video/metadata
    - entirely automated
    - maintainers verify that link is python related video
    - verify that content is appropriate
    - after X approvals a build is trigged
    - if tests pass it's auto pushed to production
- as close to 100% python as we can get
    - other than gitlab/hub everything else should be python
- as close to 0 cost and 0 hours to maintain as possible
- bus factor over 10
- URLs should be consistent with pyvideo.org
    - pyvideo gives a 301 or premanent redirect
    - if it's broken a user should just need to put 'save' in front of it (ie,
      'http://pyvideo.org/some-talk' becomes 'http://savepyvideo.org/some-talk'

## Questions
- Check out the issue tracker.
- Open an [issue](https://gitlab.com/pinkhatbeard/SavePyVideo.org/issues) or a [merge request](https://gitlab.com/pinkhatbeard/SavePyVideo.org/merge_requests).
- Check out the [sprint board](https://tree.taiga.io/project/cldershem-savepyvideoorg).

## [Current Status/Sprint](https://tree.taiga.io/project/cldershem-savepyvideoorg)
