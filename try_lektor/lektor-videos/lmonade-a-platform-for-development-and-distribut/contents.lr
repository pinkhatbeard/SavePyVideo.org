_model: video
---
title: lmonade: a platform for development and distribution of scientific software; SciPy 2013 Presentation
---
id: 1965
---
date_created: 2013-07-04T10:08:31
---
date_updated: 2014-04-08T20:28:26.349
---
date_recorded: 2013-07-02
---
event: SciPy 2013
---
speakers: []
---
description_long: 
---
description_short: Authors: Erocal, Burcin, TU Kaiserslautern

Track: Reproducible Science

Most results in experimental mathematics are accompanied by software implementations which often push the boundaries of what can be computed in terms of mathematical theory and efficiency. Since new algorithms are built on existing ones, just as theorems are derived from existing results, it would be natural to expect that the code produced for one project will be useful later on, to both the same researcher and others.

While theorems blissfully stay intact over time, software deteriorates and ages. Implementations need to be updated with respect to changes in underlying libraries and hardware architectures. Even if up to date, software developed for a specific application area often needs to be adapted to new situations. Like proofs can be reused by taking some components intact and modifying certain parts, software needs similar adaptations to be reusable.

It is natural that researchers cannot commit any more time than absolutely necessary for distributing and maintaining their software. The lmonade project aims to provide infrastructure and tools to foster code sharing and openness in scientific software development by

simplifying the tasks of distributing software with its dependencies,
ensuring that it can be built on different platforms, and
making sure the software is compatible across new releases of its dependencies.
This is achieved through

a light-weight meta distribution which can be installed by a user without administrative rights. Building on the Gentoo Linux distribution and the Gentoo Prefix project, lmonade creates a uniform environment for software development where latest versions of scientific libraries can be found easily.
access to a continuous integration infrastructure to detect compatibility problems between new versions of packages automatically and warn authors.
By simplifying code sharing and distribution, especially when complex dependencies are involved, this platform enables researchers to build on existing tools without fear of losing users to baffling installation instructions.
---
tags: ['Tech']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=7RuSFfCv4Ak
---
copyright: http://www.youtube.com/t/terms
---
slug: lmonade-a-platform-for-development-and-distribut
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/7RuSFfCv4Ak/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/7RuSFfCv4Ak?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/7RuSFfCv4Ak?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: needs editing