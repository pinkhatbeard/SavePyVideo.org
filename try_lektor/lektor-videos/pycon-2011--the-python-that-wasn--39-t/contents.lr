_model: video
---
title: The Python That Wasn't
---
id: 406
---
date_created: 2012-02-23T04:20:00
---
date_updated: 2014-04-08T20:28:28.051
---
date_recorded: 2011-03-11
---
event: PyCon US 2011
---
speakers: ['Larry Hastings']
---
description_long: The Python That Wasn't

Presented by Larry Hastings

One reason for Python's success is its restraint in adding new language
features. Only the most essential changes make it--and for every change that
gets accepted, many more are rejected. Come learn about proposed changes to
the Python language that failed--what, how, and why.

Abstract

  * Quick overview of the process 
    * First ten years: send GvR a patch 
    * The modern approach: python-ideas, write a PEP, produce a reference implementation 
  * Discuss the "prickly" Python community 
    * This is a good thing! Only the best ideas survive the python-dev gauntlet! 
    * They do this not because they're mean, but because they care so much. 
    * We must have eternal vigilance to prevent unnecessary changes! 
  * A survey of some changes that didn't make it 
    * The switch/case statement (PEP 3103) 
    * The "freeze protocol" (PEP 351) 
    * The "dynamic attribute access" proposal from python-dev, 2007/02 
    * Many more possibilities await in the rejected PEPs! 
  * My message to the audience 
    * Start with a post to python-ideas, please! 
    * Don't be surprised if you get a negative reaction 
    * Don't let your fear of a negative reaction stop you from trying, necessarily 
    * Do your homework, and be your own worst critic 


---
description_short: 
---
tags: ['coredev', 'cpython', 'makingsausage', 'pycon', 'pycon2011']
---
duration: None
---
language: English
---
source_url: 
---
copyright: Creative Commons Attribution-NonCommercial-ShareAlike 3.0
---
slug: pycon-2011--the-python-that-wasn--39-t
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://a.images.blip.tv/Pycon-PyCon2011ThePythonThatWasnt367.png
---
embed: 
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: http://05d2db1380b6504cc981-8cbed8cf7e3a131cd8f1c3e383d10041.r93.cf2.rackcdn.com/pycon-us-2011/406_the-python-that-wasn-t.mp4
---
video_ogv_download_only: False
---
video_ogv_length: 132000761
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 