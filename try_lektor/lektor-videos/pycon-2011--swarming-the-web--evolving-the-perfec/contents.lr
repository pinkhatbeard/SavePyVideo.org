_model: video
---
title: Swarming the Web: Evolving the Perfect Config File
---
id: 405
---
date_created: 2012-02-23T04:20:00
---
date_updated: 2014-04-08T20:28:28.041
---
date_recorded: 2011-03-11
---
event: PyCon US 2011
---
speakers: ['Kurt Grandis']
---
description_long: Swarming the Web: Evolving the Perfect Config File

Presented by Kurt Grandis

Deployed web applications are typically run on top of stacks of highly
configurable middleware. The number of tunable parameters and their impact are
rarely fully explored. Using SciPy and a set of common Python-based web tools
this session will present a new method of automatically tuning a typical LAMP
stack for optimal performance.

Abstract

Deployed web applications typically run on top of stacks of configurable
technology (e.g. web servers, interface modules, software load balancers,
databases). Each of these components often has dozens of tuneable parameters.
How many times are those values typically tweaked before a final set of tuned
parameters are settled on? What criteria are typically used to determine the
optimal set?

This session presents a new method of automatically tuning a common LAMP stack
for optimal performance. We explore a solution using some common Python-based
automated deployment and load testing tools and dive into scientific computing
with SciPy.


---
description_short: 
---
tags: ['lamp', 'pycon', 'pycon2011', 'scipy', 'tuning', 'webapps']
---
duration: None
---
language: English
---
source_url: 
---
copyright: Creative Commons Attribution-NonCommercial-ShareAlike 3.0
---
slug: pycon-2011--swarming-the-web--evolving-the-perfec
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://a.images.blip.tv/Pycon-PyCon2011SwarmingTheWebEvolvingThePerfectConfigFile838.png
---
embed: 
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: http://05d2db1380b6504cc981-8cbed8cf7e3a131cd8f1c3e383d10041.r93.cf2.rackcdn.com/pycon-us-2011/405_swarming-the-web-evolving-the-perfect-config-file.mp4
---
video_ogv_download_only: False
---
video_ogv_length: 138952836
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 