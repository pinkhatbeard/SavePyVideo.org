_model: video
---
title: Perceptions of Matplotlib Colormaps
---
id: 2769
---
date_created: 2014-07-15T22:45:36.992
---
date_updated: 2014-07-15T23:10:00.909
---
date_recorded: 2014-07-14
---
event: SciPy 2014
---
speakers: ['Kristen M. Thyng']
---
description_long: The choice of colormap in a scientific figure significantly affects the way the presented information is perceived by the viewer. This follows on Damon McDougall's talk on how to choose a colormap for an application by delving deeper into several important issues and how well many of the available Matplotlib colormaps stand up against the concerns. For example, it is known that the human brain is better able to interpret changes in magnitude of the luminance and saturation of colors in colormaps instead of the hue. Also, some research has shown that logarithmic changes in brightness are perceived as linear changes. Next, being able to print a color plot in black and white from a published paper is sometimes mandatory and often desirable, and is related to the grey scale in a colormap. Finally, it is important to account for various types of color blindness when choosing a divergent colormap for the plot to be as accessible as possible. All of these concerns have implications for the design of colormaps, and will be examined in the context of the properties of the available Matplotlib colormaps in order to make a best choice for a given application.

Abstract and slides available [on Github.](https://github.com/dmcdougall/scipy14-colormaps)

---
description_short: On several issues related to the perception of colormaps...
---
tags: ['vis']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=rkDgBvT-giw
---
copyright: http://www.youtube.com/t/terms
---
slug: perceptions-of-matplotlib-colormaps
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/rkDgBvT-giw/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/rkDgBvT-giw?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/rkDgBvT-giw?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: 
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: 
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 