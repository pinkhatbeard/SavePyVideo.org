_model: video
---
title: Automatic code reviews
---
id: 2996
---
date_created: 2014-07-31T00:46:30.494
---
date_updated: 2014-07-31T23:16:24.643
---
date_recorded: 2014-07-23
---
event: EuroPython 2014
---
speakers: ['Carl Crowder']
---
description_long: Static analysis tools are a great idea in theory, but are not often really used in practice. These tools usually require quite a lot of initial effort to get set up in a way which produces meaningful output for you or your organisation's particular coding style and values. As a result, it's common to see initial enthusiasm replaced by ignoring the tools.

Such tools can be incredibly beneficial however, and even go so far as to provide an automatic code review, and this talk will explain what kind of benefits you can get from the tools, as well as explain what you can and cannot expect. 

This talk is aimed at experienced developers who are interested in improving their coding practices but who have either never tried static analysis tools, or who have not seen the upsides. It will hopefully also be useful to people who do use the tools, perhaps introducing them to new tools or concepts they were not aware of yet.
---
description_short: A lot of great Python tools exist to analyse and report on your codebase, but they can require a lot of  initial set up to be useful. Done right, they can be like an automatic code review. This talk will explain how to set up and get the best out of these tools, especially for an existing, mature codebase.
---
tags: []
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=ILBsJXRQ008
---
copyright: http://creativecommons.org/licenses/by/3.0/
---
slug: automatic-code-reviews
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i.ytimg.com/vi/ILBsJXRQ008/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/ILBsJXRQ008?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/ILBsJXRQ008?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: 
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: 
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 