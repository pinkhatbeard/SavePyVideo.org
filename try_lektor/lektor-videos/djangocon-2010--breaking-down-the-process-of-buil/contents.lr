_model: video
---
title: Breaking down the process of building a custom CMS
---
id: 61
---
date_created: 2012-02-23T04:20:00
---
date_updated: 2014-04-08T20:28:25.399
---
date_recorded: None
---
event: DjangoCon 2010
---
speakers: ['Brian Luft', 'Yann Malet']
---
description_long: Breaking down the process of building a custom CMS

Presented by Yann Malet, Brian Luft

The purpose of this talk is to share some of the important insights gained
over a couple years of working on custom CMS-type projects serving large
organizations. Acknowledging that approx. 30 minutes is a short time to cover
a broad subject, the focus of the talk would be on outlining a basic strategy
for managing large projects.

Abstract

Over the past couple of years, Brian and Yann's team have built and worked on
several large Django projects for large organizations. They will discuss how
to start and manage large CMS projects, with an emphasis on pointing people to
the best reference information and breaking down the common operations.

The primary discussion topics would cover:

  * Breaking Down the Job: Over the course of the project you'll end up accounting for thousands of details; here's how to figure out the first few Centering Around a Prototype: Hedging your bets for the inevitable "give me a ballpark figure" 
  * Dealing With Legacy Data Stores: Migrate legacy information early and often. 
  * Selecting 3rd Party Components: Reuse exiting app, fork it early or Trailblaze 
  * Bad News: Django Ain't Perfect - where it has weaknesses and how to compensate 

At the end of this talk the audience will be able to:

    * Break down this type of project into meaningful phases 
    * Migrate large dataset to populate the django models that are still under development. 
    * Have a rational and objective approach to select django reusable component and understand how to refine them during the different stages of the project lifecycle. 
    * Find and evaluate quality information about the tradeoffs Django presents in these types of projects 


---
description_short: 
---
tags: ['development', 'djangocon', 'djangocon2010', 'projectmanagement']
---
duration: None
---
language: English
---
source_url: 
---
copyright: Creative Commons Attribution-NonCommercial-ShareAlike 3.0
---
slug: djangocon-2010--breaking-down-the-process-of-buil
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://a.images.blip.tv/Robertlofthouse-BreakingDownTheProcessOfBuildingACustomCMS112-442.jpg
---
embed: 
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: http://05d2db1380b6504cc981-8cbed8cf7e3a131cd8f1c3e383d10041.r93.cf2.rackcdn.com/djangocon-2010/61_breaking-down-the-process-of-building-a-custom-cms.flv
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: 218350935
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 