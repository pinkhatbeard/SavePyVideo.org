_model: video
---
title: NumPy and IPython, SciPy2013 Tutorial, Part 1 of 2
---
id: 2176
---
date_created: 2013-07-04T10:09:08
---
date_updated: 2014-04-08T20:28:26.490
---
date_recorded: 2013-06-27
---
event: SciPy 2013
---
speakers: []
---
description_long: 
---
description_short: Presenter: Valentin Haenel

Description

This tutorial is a hands-on introduction to the two most basic building-blocks of the scientific Python stack: the enhanced interactive interpreter IPython and the fast numerical container Numpy. Amongst other things you will learn how to structure an interactive workflow for scientific computing and how to create and manipulate numerical data efficiently. You should have some basic familiarity with Python (variables, loops, functions) and basic command-line usage (executing commands, using history).

Outline

Ipython (1 hour)

Using the IPython notebook
Help system, magic functions, aliases and history
Numpy (3 hours)

Basic arrays, dtypes and numerical operations
Indexing, slicing, reshaping and broadcasting
Copies, views and fancy indexing
The tutorial will feature short bursts of small exercises every 5-10 minutes.

Required Packages

An install of Anaconda should be enough

Numpy (Version 1.6 or higher)
Ipython (Version 0.13 or higher)
Matplotlib (Version 1.2.1 or higher)
Documentation

I have converted a large part of the Numpy chaper from the Python Scientific Lecture Notes to IPython notebook format using the sphinx2ipynb converter from the nbconvert project. All materials are collected in my scipy2013-tutorial-numpy-ipython Gitub repository at https://github.com/esc/scipy2013-tutorial-numpy-ipython or http://git.io/bocNDg.
---
tags: ['Tech']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=UWmZAAfXds4
---
copyright: http://www.youtube.com/t/terms
---
slug: numpy-and-ipython-scipy2013-tutorial-part-1-of-1
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/UWmZAAfXds4/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/UWmZAAfXds4?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/UWmZAAfXds4?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: needs editing