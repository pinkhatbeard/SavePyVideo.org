_model: video
---
title: Eventlet: Asynchronous I/O with a synchronous interface (#141)
---
id: 244
---
date_created: 2012-02-23T04:20:00
---
date_updated: 2014-04-08T20:28:28.113
---
date_recorded: 2010-02-19
---
event: PyCon US 2010
---
speakers: ['Donovan Preston']
---
description_long: Eventlet: Asynchronous I/O with a synchronous interface

Presented by Donovan Preston

Network servers which scale to thousands of simultaneous connections have
always been possible in Python thanks to libraries such as asyncore and
twisted. More recently, FriendFeed's new open source project Tornado has
stirred debate in this area. These libraries allow a Python process to scale
to many simultaneous connections using non-blocking I/O (also known as
asynchronous I/O). However these projects require that the programmer learn a
custom API to abstract away the complexities of using a callback-style API.

Eventlet uses greenlet, which provides coroutines as described in "The Art of
Computer Programming", to implement efficient cooperative concurrency while
retaining synchronous semantics. Eventlet also provides an implementation of
the standard library's socket module. Code written to use Python's standard
socket library can be transparently converted to use nonblocking I/O and green
threads with eventlet. This leads to much greater code reuse and programmer
efficiency.

[http://www.eventlet.net/](http://www.eventlet.net/)


---
description_short: 
---
tags: ['asynchronous', 'eventlet', 'greenlet', 'i/o', 'pycon', 'pycon2010']
---
duration: None
---
language: English
---
source_url: 
---
copyright: Creative Commons Attribution-NonCommercial-ShareAlike 3.0
---
slug: pycon-2010--eventlet--asynchronous-i-o-with-a-syn
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://a.images.blip.tv/Pycon-PyCon2010EventletAsynchronousIOWithASynchronousInterface667-479.jpg
---
embed: 
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: http://05d2db1380b6504cc981-8cbed8cf7e3a131cd8f1c3e383d10041.r93.cf2.rackcdn.com/pycon-us-2010/244_eventlet-asynchronous-i-o-with-a-synchronous-interface-141.m4v
---
video_ogv_download_only: False
---
video_ogv_length: 157178869
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 