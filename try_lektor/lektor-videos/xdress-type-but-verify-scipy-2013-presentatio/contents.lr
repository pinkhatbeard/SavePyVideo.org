_model: video
---
title: XDress - Type, But Verify; SciPy 2013 Presentation
---
id: 2065
---
date_created: 2013-07-04T10:08:47
---
date_updated: 2014-04-08T20:28:26.379
---
date_recorded: 2013-07-02
---
event: SciPy 2013
---
speakers: []
---
description_long: 
---
description_short: Authors: Scopatz, Anthony, The University of Chicago & NumFOCUS, Inc.

Track: General

XDress is an automatic wrapper generator for C/C++ written in pure Python. Currently, xdress may generate Python bindings (via Cython) for C++ classes & functions and in-memory wrappers for C++ standard library containers (sets, vectors, maps). In the future, other tools and bindings will be supported.

The main enabling feature of xdress is a dynamic type system that was designed with the purpose of API generation in mind. This type system provides a canonical abstraction of various kinds of types: Base types (int, str, float, non-templated classes), refined types (even or odd ints, strings containing the letter 'a'), and dependent types (templates such arrays, maps, sets, vectors). This canonical form is itself hashable, being comprised only of strings, ints, and tuples.

On top of this type system, xdress provides a tool for auto-generating classes which are views into template instantiations of C++ standard library maps and sets. Additionally, this tool also creates custom numpy dtypes for any C++ type, class or struct. This allows the user to have numpy array views into C++ vectors.

Furthermore, xdress also has a tool which inspects a C++ code base and automatically generates Cython wrappers for all user-specified classes and functions. This significantly eases the burden of supporting mixed language projects.

The above code generators, however, are just the beginning. The xdress type system is flexible and powerful enough to engender a suite of other tools which take advantage of less obvious features. For example, an automatic verification & validation utility could take advantage of refinement type predicate functions to interdict parameter constraints into the API right under the users nose!

This talk will focus on xdress's type system and its use cases.
---
tags: ['Tech']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=a4kqO1IBZ4U
---
copyright: http://www.youtube.com/t/terms
---
slug: xdress-type-but-verify-scipy-2013-presentatio
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/a4kqO1IBZ4U/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/a4kqO1IBZ4U?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/a4kqO1IBZ4U?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: needs editing