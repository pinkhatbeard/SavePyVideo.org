_model: video
---
title: Rapid Python used on Big Data to Discover Human Genetic Variation
---
id: 414
---
date_created: 2012-02-23T04:20:00
---
date_updated: 2014-04-08T20:28:28.023
---
date_recorded: 2011-03-11
---
event: PyCon US 2011
---
speakers: ['Deniz Kural']
---
description_long: Rapid Python used on Big Data to Discover Human Genetic Variation

Presented by Deniz Kural

Advances in genome sequencing has enabled large-scale projects such as the
1000 Genomes Project to sequence genomes across diverse populations around the
world, resulting in very large data sets. I use Python for rapid development
of algorithms for processing & analyzing genomes and discovering thousands of
new variants, including "Mobile Elements" that copy&paste; themselves across
the genome.

Abstract

Recent advances in high-throughput sequencing now enables accurate sequencing
human genomes at a low cost & high speed. This technology is now used to
initiate projects involving large-scale sequencing of many genomes. The 1000
Genomes project aims to sequence 2500 genomes across 27 world populations, and
has initially completed its Pilot phase. The aim of the project is to discover
& characterize novel variants. These variants enable association studies that
investigate the link between genomic variation & phenotypes, including
disease.

A class of variants, known as "Structural Variants" represent a heterogenous
class of larger variants, such as inversions, duplications, deletions, and
various kinds of insertions.

I use Python to for rapid development of algorithms to process, analyze, and
annotate very large data sets. In particular, I focus on Mobile Elements,
pieces of DNA that copy&paste; across the genome. These elements constitute
roughly half of the genome, whereas protein-coding genes account for roughly
1.5 % of the genome.

I will discuss distributed computing, genomics, and big data within the
context of Python.


---
description_short: 
---
tags: ['bigdata', 'casestudy', 'dna', 'genomes', 'pycon', 'pycon2011']
---
duration: None
---
language: English
---
source_url: 
---
copyright: Creative Commons Attribution-NonCommercial-ShareAlike 3.0
---
slug: pycon-2011--rapid-python-used-on-big-data-to-disc
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://a.images.blip.tv/Pycon-PyCon2011RapidPythonUsedOnBigDataToDiscoverHumanGenet382.png
---
embed: 
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: http://05d2db1380b6504cc981-8cbed8cf7e3a131cd8f1c3e383d10041.r93.cf2.rackcdn.com/pycon-us-2011/414_rapid-python-used-on-big-data-to-discover-human-genetic-variation.mp4
---
video_ogv_download_only: False
---
video_ogv_length: 110103548
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 