_model: video
---
title: QNDArray: A Numpy Clone for C++/Qt
---
id: 1203
---
date_created: 2012-08-31T16:34:48
---
date_updated: 2014-04-08T20:28:27.156
---
date_recorded: 2012-07-18
---
event: SciPy 2012
---
speakers: ['Glen W. Mabey']
---
description_long: While Numpy/Scipy is an attractive implementation platform for many
algorithms, in some cases C++ is mandated by a customer. However, a foundation
of numpy's behavior is the notion of reference-counted instances, and
implementing an efficient, cross-platform mechanism for reference counting is
no trivial prerequisite.

The reference counting mechanisms already implemented in the Qt C++ toolkit
provide a cross-platform foundation upon which a numpy-like array class can be
built. In this talk one such implementation is discussed, QNDArray. In fact,
by mimicking the numpy behaviors, the job of implementing QNDArray became much
easier, as the task of "defining the behavior" became "adopting the behavior,"
to include function names.

In particular, the following aspects of the implementation were found to be
tricky and deserve discussion in this presentation:

    

  * slicing multidimensional arrays given the limitations of operator[] in C++,
  * const
  * partial specialization
  * implicit vs. explicit data sharing in Qt
QNDArray has been deployed in scientific research applications and currently
has the following features:

    

  * bit-packed boolean arrays
  * nascent masked array support
  * unit test suite that validates QNDArray behavior against numpy behavior
  * bounds checking with Q_ASSERT() (becomes a no-op in release mode)
  * memmap()ed arrays via QFile::map()
  * easily integrated as a QVariant value, leading to a natural mapping from QVariantMap to Python dict.
  * float16 implementation including in-place compare

The author has approval from his management to submit the source code for
QNDArray to the Qt Project and plans to have it freely available for download
via [http://qt.gitorious.org/](http://qt.gitorious.org/) before the SciPy
conference begins.


---
description_short: 
---
tags: ['General']
---
duration: None
---
language: English
---
source_url: http://youtube.com/watch?v=PLqIRtpUHGk
---
copyright: CC BY-SA
---
slug: qndarray-a-numpy-clone-for-cqt
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/PLqIRtpUHGk/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/PLqIRtpUHGk?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/PLqIRtpUHGk?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: http://s3.us.archive.org/nextdayvideo/enthought/scipy_2012/QNDArray_A_Numpy_Clone_for_CQt.mp4?Signature=ODStHEG40%2FmQ5sV78j11L%2FYj%2Fks%3D&Expires=1346381293&AWSAccessKeyId=FEWGReWX3QbNk0h3
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 