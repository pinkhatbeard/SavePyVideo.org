_model: video
---
title: Scientific Visualization with GR
---
id: 2949
---
date_created: 2014-07-31T00:46:25.094
---
date_updated: 2014-07-31T01:09:28.437
---
date_recorded: 2014-07-25
---
event: EuroPython 2014
---
speakers: ['Josef']
---
description_long: Python has long been established in software development departments of research and industry, not least because of the proliferation of libraries such as *SciPy* and *Matplotlib*. However, when processing large amounts of data, in particular in combination with GUI toolkits (*Qt*) or three-dimensional visualizations (*OpenGL*), it seems that Python as an interpretative programming language may be reaching its limits.

---

*Outline*

- Introduction (1 min)
    - motivation
- GR framework (2 mins)
    - layer structure
    - output devices and capabilities
- GR3 framework (1 min)
    - layer structure
    - output capabilities (3 mins)
        - high-resolution images
        - POV-Ray scenes
        - OpenGL drawables
        - HTML5 / WebGL
- Simple 2D / 3D examples (2 min)
- Interoperability (PyQt/PySide, 3 min)
- How to speed up Python scripts (4 mins)
    - Numpy
    - Numba (Pro) 
- Animated visualization examples (live demos, 6 mins)
    - physics simulations
    - surfaces / meshes
    - molecule viewer
    - MRI voxel data
- Outlook (1 min)

*Notes*

Links to similar talks, tutorials or presentations can be found [here][1]. Unfortunately, most of them are in German language.

The GR framework has already been presented in a talk at PyCon DE [2012][2] and [2013][3], during a [poster session][4] at PyCon US 2013, and at [PythonCamps 2013][5] in Cologne. The slides for the PyCon.DE 2013 talk can be found [here][6].

As part of a collaboration the GR framework has been integrated into [NICOS][7] (a network-based control system completely written in Python) as a replacement for PyQwt.

  [1]: http://gr-framework.org/
  [2]: https://2012.de.pycon.org/programm/schedule/sessions/54
  [3]: https://2013.de.pycon.org/schedule/sessions/45/
  [4]: https://us.pycon.org/2013/schedule/presentation/158/
  [5]: http://josefheinen.de/rasberry-pi.html
  [6]: http://iffwww.iff.kfa-juelich.de/pub/doc/PyCon_DE_2013
  [7]: http://cdn.frm2.tum.de/fileadmin/stuff/services/ITServices/nicos-2.0/dirhtml/

---
description_short: Python developers often get frustrated when managing visualization packages that cover the specific needs in scientific or engineering environments. The [*GR* framework](http://gr-framework.org/) could help. *GR* is a library for visualization applications ranging from publication-quality 2D graphs to the creation of complex 3D scenes and can easily be integrated into existing Python environments or distributions like *Anaconda*.
---
tags: []
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=-oSAMkqbWjs
---
copyright: http://creativecommons.org/licenses/by/3.0/
---
slug: scientific-visualization-with-gr
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i.ytimg.com/vi/-oSAMkqbWjs/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/-oSAMkqbWjs?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/-oSAMkqbWjs?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: 
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: 
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: needs editing, no last name 86