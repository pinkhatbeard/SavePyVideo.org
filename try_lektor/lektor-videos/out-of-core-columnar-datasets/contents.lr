_model: video
---
title: Out-of-Core Columnar Datasets
---
id: 2946
---
date_created: 2014-07-31T00:46:24.723
---
date_updated: 2014-07-31T01:03:52.616
---
date_recorded: 2014-07-25
---
event: EuroPython 2014
---
speakers: ['Francesc Alted']
---
description_long: It is a fact: we just entered in the Big Data era.  More sensors, more
computers, and being more evenly distributed throughout space and time
than ever, are forcing data analyists to navigate through oceans of
data before getting insights on what this data means.

Tables are a very handy and spreadly used data structure to store
datasets so as to perform data analysis (filters, groupings, sortings,
alignments...).  However, the actual table implementation, and
especially, whether data in tables is stored row-wise or column-wise,
whether the data is chunked or sequential, whether data is compressed or not,
among other factors, can make a lot of difference depending on the
analytic operations to be done.

My talk will provide an overview of different libraries/systems in the
Python ecosystem that are designed to cope with tabular data, and how
the different implementations perform for different operations.  The
libraries or systems discussed are designed to operate either with
on-disk data ([PyTables] [1], [relational databases] [2], [BLZ] [3],
[Blaze] [4]...) as well as in-memory data containers ([NumPy] [5],
[DyND] [6], [Pandas] [7], [BLZ] [3], [Blaze] [4]...).

A special emphasis will be put in the on-disk (also called
out-of-core) databases, which are the most commonly used ones for
handling extremely large tables.

The hope is that, after this lecture, the audience will get a better
insight and a more informed opinion on the different solutions for
handling tabular data in the Python world, and most especially, which
ones adapts better to their needs.

[1]: http://www.pytables.org
[2]: http://en.wikipedia.org/wiki/Relational_database
[3]: http://blz.pydata.org
[4]: http://blaze.pydata.org
[5]: http://www.numpy.org/
[6]: https://github.com/ContinuumIO/dynd-python
[7]: http://pandas.pydata.org/

---
description_short: Tables are a very handy data structure to store
datasets to perform data analysis (filters, groupings, sortings,
alignments...).

But it turns out that *how the tables are actually implemented* makes a large impact on how they perform.

Learn what you can expect from the current tabular offerings in the Python ecosystem.
---
tags: []
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=-lKV4zC1gss
---
copyright: http://creativecommons.org/licenses/by/3.0/
---
slug: out-of-core-columnar-datasets
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i.ytimg.com/vi/-lKV4zC1gss/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/-lKV4zC1gss?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/-lKV4zC1gss?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: 
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: 
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 