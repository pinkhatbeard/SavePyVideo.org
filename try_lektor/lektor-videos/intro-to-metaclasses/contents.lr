_model: video
---
title: Intro to Metaclasses
---
id: 2445
---
date_created: 2013-10-19T19:58:54
---
date_updated: 2014-04-08T20:28:26.074
---
date_recorded: 2013-09-16
---
event: Kiwi PyCon 2013
---
speakers: ['Craig de Stigter']
---
description_long: @ Kiwi PyCon 2013 - Saturday, 07 Sep 2013 - Track 2

**Audience level**

Intermediate

**Abstract**

Python's Metaclasses are an "advanced" technique, but once you get started they're surprisingly approachable.

Starting with the basics, we'll:

* figure out what a metaclass does
* point out some existing metaclasses you might already have used
* see where you might use one in your own code.

We'll compare use of a metaclass with some alternatives that don't use metaclasses, to better understand why you'd want to use them.

Then we'll take a look at a couple of use-cases where using a metaclass helps us achieve a much more friendly and intuitive API than we otherwise could.

**Slides**

https://speakerdeck.com/nzpug/craig-de-stigter-intro-to-metaclasses
---
description_short: Metaprogramming is a valuable technique for putting complexity where it belongs: behind a clean, friendly API. This talk goes over the basics of metaclasses and introspection in Python, and covers how and when you should use them to make your code more approachable.

---
tags: []
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=OCDbLcCB-uA
---
copyright: 
---
slug: intro-to-metaclasses
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/OCDbLcCB-uA/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/OCDbLcCB-uA?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/OCDbLcCB-uA?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: 
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: 
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 