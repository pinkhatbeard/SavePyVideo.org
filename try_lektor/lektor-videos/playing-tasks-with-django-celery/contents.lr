_model: video
---
title: Playing tasks with Django-Celery
---
id: 1039
---
date_created: 2012-08-30T21:02:14
---
date_updated: 2014-04-08T20:28:27.747
---
date_recorded: 2011-07-21
---
event: EuroPython 2011
---
speakers: ['Mauro Rocco']
---
description_long: Celery is an open source task queueing system based on distributed message
passing.

I will talk about the tools that Celery offers for task distribution and how
to monitor and manage the system using a Django web interface. This talk will
also focus on how we use Celery at Jamendo and our real solutions to some
common issues you may encounter when developing a back-office based on Celery.

The talk will cover the following topics:

  * A brief overview of Celery and the AMPQ protocol AMPQ protocol overview, Celery introduction: Celery, RabbitMQ code examples

  * The impact of Celery on the Jamendo work-flow; examples with real tasks. Here I will talk about the Jamendo back-office infrastructure and some of our common tasks. I will discuss the improvements made by introducing a new back-office system based on Celery. I will show some code snippets and go over some real scenarios.

  * Overview of the Django Celery admin interface and some Jamendo extensions. Let's talk about the Django-Celery interface that allows one to monitor or schedule tasks directly from the Django admin. I will explain which common additional features are necessary and how to add them.

  * Common "gotchas" we encountered while working with Celery and how we solved them.

  * Global task locks

  * Centralized logging: be able to read all the logs of all celery workers on different servers and filter them for real-time debugging


---
description_short: [EuroPython 2011] Mauro Rocco - 22 June 2011 in "Track Tagliatelle "


---
tags: ['celery', 'distributed', 'django', 'infrastructure', 'queueing', 'rabbitmq', 'real-time', 'web']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=HgN8rOstPjc
---
copyright: Standard YouTube License
---
slug: playing-tasks-with-django-celery
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i.ytimg.com/vi/HgN8rOstPjc/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/HgN8rOstPjc?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/HgN8rOstPjc?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 