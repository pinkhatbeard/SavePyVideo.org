_model: video
---
title: Epipy: Visualization of Emerging Zoonoses Through Temporal Networks
---
id: 2767
---
date_created: 2014-07-15T22:45:36.773
---
date_updated: 2014-07-15T23:01:32.155
---
date_recorded: 2014-07-14
---
event: SciPy 2014
---
speakers: ['Caitlin Rivers']
---
description_long: We present two new visualizations, [case tree plots](https://github.com/cmrivers/epipy/blob/master/figs/example_casetree.png)
and [checkerboard](https://github.com/cmrivers/epipy/blob/master/figs/test_checkerboard.png)
plots, for visualizing emerging zoonoses.

Zoonoses represent an estimated 58% of all human infectious diseases, and 73%
of emerging infectious diseases. Recent examples of zoonotic outbreaks include
H1N1, SARS and Middle East Respiratory Syndrome, which have caused thousands of
deaths combined. The current toolkit for visualizing data from these emerging
diseases is limited.

Case tree and checkerboard plots were developed to address that gap.
The visualizations are best suited for diseases like SARS for which there are a
limited number of cases, with data available on human to human transmission.
They a) allow for easy estimation of epidemiological parameters like basic
reproduction number b) indicate the frequency of introductory events, e.g.
spillovers in the case of zoonoses c) represent patterns of case attributes
like patient sex both by generation and over time.

Case tree plots depict the emergence and growth of clusters of disease over
time. Each case is represented by a colored node. Nodes that share an
epidemiological link are connected by an edge. The color of the node varies
based on the node attribute; it could represent patient sex, health status
(e.g. alive, dead), or any other categorical attribute. Node placement along
the x-axis corresponds with the date of illness onset for the case.

A second visualization, the checkerboard plot, was developed to complement case
tree plots. They can be used in conjunction with case tree plots, or in
situations where representing a hypothetical network structure is
inappropriate.

The plots are available in the open source package epipy, which is available on
[github](https://github.com/cmrivers/epipy). Detailed documentation and
examples are also [available](cmrivers.github.io/epipy). In addition to these
visualizations, epipy includes functions for common epidemiology calculations
like odds ratio and relative risk.

---
description_short: We introduce two new plots for visualizing infectious disease outbreaks. Case tree plots depict the emergence and growth of clusters of zoonotic disease over time. Checkerboard plots also represent temporal case clusters, but do not construct transmission trees. These plots visualize outbreak dynamics  and allow for analyses like case fatality risk stratified by generation.
---
tags: ['epipy', 'vis']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=JDtIDCVrxgk
---
copyright: http://www.youtube.com/t/terms
---
slug: epipy-visualization-of-emerging-zoonoses-through
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/JDtIDCVrxgk/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/JDtIDCVrxgk?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/JDtIDCVrxgk?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: 
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: 
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 