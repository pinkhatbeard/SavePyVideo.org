_model: video
---
title: Activity Detection from GPS Tracker Data
---
id: 2806
---
date_created: 2014-07-15T22:45:41.568
---
date_updated: 2014-07-15T23:29:14.024
---
date_recorded: 2014-07-13
---
event: SciPy 2014
---
speakers: ['Jan Vandrol']
---
description_long: Most modern GPS processing techniques started to utilise the time interval between points as a major indicator for finding POIs in a user's trajectories. We are taking step back in this process, and only account for the spatial distribution of measured points in order to extract POIs. Time is solely used as a secondary indicator and for ordering purposes.

Points are first cleaned of any highly inaccurate data and stored in a PostGIS environment. Using developed python module, we extract the point data and order them by time into one large trajectory. Then, for each point, we begin selecting its neighbours (both previous and following) until we reach one within a specified distance of 50m from the first point.The number of the selected points is added to the original point as a new attribute.Continuing with the process the newly calculated values create an imitation of signal, reflecting a point density.

Shift in strength of the signal signifies a change in a user's travel mode which can be used for segmentation of the trajectory into homogeneous parts. Identification of these parts is currently based on the decision tree, where individual aspects of the segment (like average speed, signal strength, time elapsed or centroid) are evaluated and the segment is categorized.

Current work seeks to incorporate neural networks into the processing framework. Since the signal pattern of the each mode of transportation (car, bus, walk, etc.) is independent from the user behaviour, a properly trained model should classify more accurately activities for a broad range of users.

---
description_short: Data gathered by personal GPS trackers are becoming a major source of information pertaining to human activity and behaviour. This presentation will include python work flows which aim to accurately and efficiently carry out the necessary computation required to process volunteered GPS trajectories into useful spatial information.
---
tags: []
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=8t__23WhJ1U
---
copyright: http://www.youtube.com/t/terms
---
slug: activity-detection-from-gps-tracker-data
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/8t__23WhJ1U/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/8t__23WhJ1U?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/8t__23WhJ1U?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: 
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: 
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 