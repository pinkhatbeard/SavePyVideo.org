_model: video
---
title: yt: volumetric data analysis
---
id: 2779
---
date_created: 2014-07-15T22:45:38.425
---
date_updated: 2014-07-15T23:27:36.774
---
date_recorded: 2014-07-14
---
event: SciPy 2014
---
speakers: ['Nathan Goldbaum']
---
description_long: 1. What is yt?

    1. "Lingua-franca for astrophysical simulations"
        1. [AGORA](https://sites.google.com/site/santacruzcomparisonproject/)
        2. Some selections from [the gallery](http://yt-project.org/gallery.html)
        3. 105 [citations to the yt method paper](http://adsabs.harvard.edu/cgi-bin/nph-ref_query?bibcode=2011ApJS..192....9T&amp;refs=CITATIONS&amp;db_key=AST)

    2. Massively parallel

        1. Examples of large-scale calculations and visualizations performed with yt

        2. Usage data on XSEDE visualization resources

    3. Volumetric data analysis beyond astrophysics

        1. [Neurodome](http://www.neurodome.org/), Whole-earth seismic wave data, Weather simulation data, Nuclear engineering, Radio astronomy

        2. ??? (insert your field here!)

2. What's new in yt-3.0?

    1. Rewrite of data selection, i/o, and field detection and creation

    2. Octree and particle support (i.e., discrete points)

    3. Unit conversions and dimensional analysis baked into the codebase

    4. Rethinking the API, 'rebranding' the project

    5. Advanced volume rendering

3. Growing the Community

    1. The gallery

    2. Workshops

    3. Contributor statistics.

4. The future

    1. New data styles

        1. Unstructured meshes

        2. Finite element analysis

        3. Spectral codes

    2. New domain-specific functionality (beyond astrophysics)

    3. Browser GUIs powered by IPython

---
description_short: yt started as a tool for visualizing data from astrophysical simulations, but it has evolved into a method for analyzing generic volumetric data.  In this talk we will present yt 3.0, which includes an increased focus on inquiry-driven analysis and visualization, a sympy-powered unit system, a revised user interface, and the ability to scale to petabyte datasets and tens of thousands of cores.
---
tags: []
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=sNkN7nyj4nE
---
copyright: http://www.youtube.com/t/terms
---
slug: yt-volumetric-data-analysis
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/sNkN7nyj4nE/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/sNkN7nyj4nE?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/sNkN7nyj4nE?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: 
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: 
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 