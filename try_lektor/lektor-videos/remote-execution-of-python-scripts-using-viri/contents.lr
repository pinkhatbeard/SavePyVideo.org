_model: video
---
title: Remote execution of Python scripts using Viri
---
id: 1063
---
date_created: 2012-08-30T21:02:20
---
date_updated: 2014-04-08T20:28:27.789
---
date_recorded: 2011-07-18
---
event: EuroPython 2011
---
speakers: ['Marc Garcia']
---
description_long: Viri is a system for automatic distribution and execution of Python code on
remote machines. This is especially useful when dealing with a large group of
hosts.

With Viri, Sysadmins can write their own scripts, and easily distribute and
execute them on any number of remote machines. Depending on the number of
computers to administrate, Viri can save thousands of hours, that Sysadmins
would spend transferring files, logging into remote hosts, and waiting for the
scripts to finish. Viri automates the whole process.

Viri can also be useful for remotely managing host settings. It should work
together with an application where the information about hosts would be
maintained. This information can include cron tasks, firewall rules, backup
settings,etc. After a simple Integration of this application with your Viri
infrastructure, you can change any settings in the application, and see how it
gets applied on the target host automatically.

The talk will cover next topics:

  * Introduction to Viri
  * Live demo on how to install Viri, write a Viri task, and execute it in a remote host
  * Security concerns using Viri

Check [Viri project page](http://www.viriproject.com)


---
description_short: [EuroPython 2011] Marc Garcia - 21 June 2011 in "Track Tagliatelle"


---
tags: ['backup', 'logging', 'scripts']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=SGz5RZEOMFE
---
copyright: Standard YouTube License
---
slug: remote-execution-of-python-scripts-using-viri
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i.ytimg.com/vi/SGz5RZEOMFE/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/SGz5RZEOMFE?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/SGz5RZEOMFE?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 