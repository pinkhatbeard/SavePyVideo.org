_model: video
---
title: Writing great documentation
---
id: 403
---
date_created: 2012-02-23T04:20:00
---
date_updated: 2014-04-08T20:28:28.066
---
date_recorded: 2011-03-11
---
event: PyCon US 2011
---
speakers: ['Jacob Kaplan-Moss']
---
description_long: Writing great documentation

Presented by Jacob Kaplan-Moss

Django’s among the best documented open source projects; I'm intensely proud
of that accomplishment. If any part of Django endures, I hope it’ll be a
“documentation culture” — an ethos that values great, well-written
documentation. To that end, this talk looks at the tools, tips, and techniques
I’ve learned over the years. I hope it helps you write great documentation,
too.

Abstract

This talk looks at tips, tools, and techniques you can use to produce great
technical documentation.

It's split roughly into two parts:

  * Part 1: technique. We'll look at the structural elements that make documentation useful: tutorials, high-level overviews, topical guides, reference material, FAQs, and more. We'll cover some tips on how to get documentation done, and community processes for handling documentation in teams (open or not). We'll also talk about what I'm calling "Documentation Driven Development" - a technique riffing off Test Driven Development that calls for writing documentation before code. 
  * Part 2: tools. Over the last couple of years a few fantastic tools have sprung up that ease the technical aspects of writing documentation. We'll talk about which tools are suitable for which uses, and look at a handful of cool tools including [Sphinx](http://sphinx.pocoo.org/), [Epydoc](http://epydoc.sourceforge.net/), and [http://fitzgen.github.com/pycco/](http://fitzgen.github.com/pycco/). 

This talk is mostly targeted towards those documenting libraries or frameworks
intended for use by other developers, but much of it probably applies to any
sort of technical documentation.


---
description_short: 
---
tags: ['docs', 'documentation', 'epydoc', 'faqs', 'pycco', 'pycon', 'pycon2011', 'sphinx']
---
duration: None
---
language: English
---
source_url: 
---
copyright: Creative Commons Attribution-NonCommercial-ShareAlike 3.0
---
slug: pycon-2011--writing-great-documentation
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://a.images.blip.tv/Pycon-PyCon2011WritingGreatDocumentation902.png
---
embed: 
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: http://05d2db1380b6504cc981-8cbed8cf7e3a131cd8f1c3e383d10041.r93.cf2.rackcdn.com/pycon-us-2011/403_writing-great-documentation.mp4
---
video_ogv_download_only: False
---
video_ogv_length: 158578172
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 