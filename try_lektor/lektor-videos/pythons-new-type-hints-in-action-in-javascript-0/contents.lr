_model: video
---
title: Python's New Type Hints in Action… In JavaScript
---
id: 3984
---
date_created: 2015-10-18T16:10:56.555
---
date_updated: 2015-10-18T16:10:56.556
---
date_recorded: 2015-08-04
---
event: PyCon AU 2015
---
speakers: ['Christopher Neugebauer']
---
description_long: Depending on who you ask, PEP 484's Type Hints are either the next big thing in Python, or the harbinger of doom upon our entire community. Which is it?

Allowing optional static typing in Python will bring with it some benefits that other languages have had for years: IDEs will be able to do code completion better; a whole class of boring tests will fall out automatically; and some bugs will be easier to catch. 

But this is also undeniably a huge change of direction: will it mean you have to substantially change your code style? Will Python's simple expressiveness suddenly become unattainable thanks to clumsy Java-style type declarations?

To show how PEP 484's Gradual Typing system works, we're going to look at TypeScript, a minimal implementation of Gradual Typing over JavaScript. We'll see how the type system works, and how it fits into the already thriving JavaScript developer community, where most people aren't using type hints at all.

We'll draw some parallels with how Python's implementation will work, and see what Python can learn from a language that has successfully made the jump to a type-hinted world.


---
description_short: 
---
tags: []
---
duration: None
---
language: English
---
source_url: https://www.youtube.com/watch?v=_PPQLeimyOM
---
copyright: creativeCommon
---
slug: pythons-new-type-hints-in-action-in-javascript-0
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: https://i.ytimg.com/vi/_PPQLeimyOM/hqdefault.jpg
---
embed: <iframe width="640" height="360" src="//www.youtube.com/embed/_PPQLeimyOM" frameborder="0" allowfullscreen></iframe>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: needs editing