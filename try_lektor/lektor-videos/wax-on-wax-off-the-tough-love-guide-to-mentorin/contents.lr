_model: video
---
title: Wax On, Wax Off: The Tough Love Guide to Mentoring
---
id: 4002
---
date_created: 2015-10-18T16:10:59.157
---
date_updated: 2015-10-18T16:10:59.157
---
date_recorded: 2015-08-16
---
event: PyCon AU 2015
---
speakers: ['Anita Kuno']
---
description_long: Everyone has more bugs than they can fix, more features they would like than time to write them. Wouldn't it be great to have some help? The problem is who knows enough about what you know to actually provide help the way you need?

Open Source has lots of fans and supporters, but many newcomers need the warmth of a person caring about their existence to actually turn them into a contributor.

Someone sees you working, is enamoured of what you do and wants to learn. You could break down the steps of what you are doing and get them to follow those steps, that is teaching.

You could recognize the deep inner understanding the is the foundation of your behaviour. You could decide to help someone build such a foundation for themselves. That is mentoring.

Mentoring is helping someone to fail and learn from it. Mentoring is about witnessing someone else's journey, whereever they go, whereever it takes them and saying I'm here, I'm with you, I'm watching. Mentoring is 99% listening.

Mentoring is much more time consuming than teaching. Teaching helps someone learn a skill, mentoring helps someone learn themselves.

Now don't get me wrong, teaching is great and we need teachers and to be taught plus many great teachers can also be mentors. Teaching and mentoring are different.

This talk will discuss mentoring.

With examples drawn from learning acupuncture, yoga, astrology, pottery, working on film sets, heavy equipment operating as well as code, Anita will endeavour to share what she has learned about mentoring. She hopes you will take away the ability to recognize the difference between teaching and mentoring. She also hopes that should you decide to mentor you have some resources to help you strengthen your mentoring skills.


---
description_short: 
---
tags: []
---
duration: None
---
language: English
---
source_url: https://www.youtube.com/watch?v=x1rIPW6IYVI
---
copyright: creativeCommon
---
slug: wax-on-wax-off-the-tough-love-guide-to-mentorin
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: https://i.ytimg.com/vi/x1rIPW6IYVI/hqdefault.jpg
---
embed: <iframe width="640" height="360" src="//www.youtube.com/embed/x1rIPW6IYVI" frameborder="0" allowfullscreen></iframe>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: needs editing