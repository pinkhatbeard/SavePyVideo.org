_model: video
---
title: Twisted Names: DNS Building Blocks for Python Programmers
---
id: 2945
---
date_created: 2014-07-31T00:46:24.600
---
date_updated: 2014-07-31T01:14:02.133
---
date_recorded: 2014-07-25
---
event: EuroPython 2014
---
speakers: ['Richard Wall']
---
description_long: # Description

My talk will consist of four main sections. Given the 30 minute time constraint,
I may shorten or drop the two introductory parts in favour of the narrative and
demonstration of interesting new APIs and code examples in the final two
parts. My experience of delivering a similar talk at PyconUK 2013 is that those
are the parts that will most interest the audience and prompt most audience
questions.

Here are my proposed sections with rough time allocations and descriptions:

## Introducing Twisted Names (0-5)

Twisted includes a comprehensive set of DNS components, collectively
known as Twisted Names.

-   <https://twistedmatrix.com/trac/wiki/TwistedNames>

I will begin the talk with a quick introduction to Twisted Names and its
capabilities, including one or two simple code examples.

## Introducing My Project (0-5)

With generous funding from The NLnet Foundation I am adding EDNS(0) and DNSSEC
client support in Twisted Names, including full DNSSEC verification and DANE
support.

In the talk I will quickly summarise the steps taken and lessons learned in
securing that funding, and hope to encourage the audience to seek funding to
support there own pet OSS projects.

## What's New in Twisted Names / Project Progress Report (10)

My project plan is divided into the following broad milestones.

1.  EDNS(0)

    1.  OPT record

    2.  Extended Message object with additional EDNS(0) items

    3.  EDNS Client

2.  RRSET handling

    1.  Canonical Form and Order of Resource Records

    2.  Receiving RRSETs

3.  DNSSEC

    1.  New DNSSEC Records and Lookup Methods

    2.  Security-aware Non-validating Client

    3.  Validating Client

4.  DANE

    1.  A twistd dns authoritative server capable of loading and serving TLSA
        records.

    2.  A Twisted web client Agent wrapper which performs TLSA lookup and
        verification of a server certificate.

    3.  A HostnameClientEndpoint which performs TLSA lookup and verification of a
        server certificate.

    4.  A command line tool for debugging TLSA records and for verifying a
        certificate file against a domain name.

    5.  A TLSA Record class for encoding and decoding TLSA bytes.

    6.  A TLSA lookup method which accepts port, protocol and hostname and constructs
        a suitable TLSA domain name.

In the talk I will quickly outline these goals, report on my progress so far,
and show running code examples to demonstrate the new APIs.

## Future Developments (5)

The aim of my project is to lay foundations that will eventually allow
end-to-end DNSSEC verification in all the core Twisted networking components,
including Twisted Conch (SSH), Mail (SMTP, POP3), Perspective Broker (RPC), Web
(HTTP, XML-RPC, SOAP), Words (XMPP, IRC).

Additionally I hope that this foundation work will encourage the development of
end-to-end DNSSEC verification in many of the Open Source and commercial
projects built on top of Twisted.

I will end the talk by outlining these exciting possibilities, and demonstrate
some code examples that illustrate these possibilities.

## Q & A (5-10)

I'm determined to leave at least five minutes at the end for audience
questions. At PyconUK 2013 I was frustrated because I ran out of time and ended
up answering questions outside the lecture theatre; questions which would have
been interesting to the whole audience.

---
description_short: In this talk I will report on my efforts to update the DNS components of Twisted
and discuss some of the things I've learned along the way. I'll demonstrate the
EDNS0, DNSSEC and DANE client support which I have been working on and show how
these new Twisted Names components can be glued together to build novel DNS
servers and clients.

Twisted is an event-driven networking engine written in Python and
licensed under the open source MIT license.  It is a platform for
developing internet applications.
---
tags: []
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=kuSXK4gNYqw
---
copyright: http://creativecommons.org/licenses/by/3.0/
---
slug: twisted-names-dns-building-blocks-for-python-pro
---
related_urls: [{'description': 'Notes and examples', 'url': 'https://github.com/wallrj/twisted-names-talk'}]
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i.ytimg.com/vi/kuSXK4gNYqw/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/kuSXK4gNYqw?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/kuSXK4gNYqw?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: 
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: 
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 