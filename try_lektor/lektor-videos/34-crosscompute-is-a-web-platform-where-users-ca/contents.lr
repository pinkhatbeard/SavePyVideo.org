_model: video
---
title: 34. CrossCompute is a web platform where users can run algorithms
---
id: 734
---
date_created: 2012-03-14T06:00:16
---
date_updated: 2014-04-08T20:28:27.337
---
date_recorded: 2012-03-11
---
event: PyCon US 2012
---
speakers: ['Roy Hyunjin Han']
---
description_long: CrossCompute is a site where you can run mathematical models and algorithms
submitted by researchers. If you are a researcher and you submit your
algorithm, you'll get paid each time it runs. We'll send you a URL where users
can run your model on their data as many times as they want. More users mean
more feedback that you can use to improve your model.

Here is an example: Suppose a mathematician in the US develops an operations
research model for placing schools in a community given the locations of
existing schools and households, and decides to charge ten cents each time it
runs. Then a business owner in Ghana runs the model over the internet to place
stores in his district, and saves money because he didn't have to hire a
consulting firm or buy a software package. Meanwhile, the mathematician in the
US uses the algorithm revenue to fund research to improve the model.

Now is an exciting time to get involved in data analytics for your city or
local government. Governments need to prioritize limited resources like
emergency response firefighters, police officers and city inspectors. I urge
you to contact your local Mayor's office to learn about the challenges they
are facing. Then, create a tool to make their life easier. You can send the
tool to support@crosscompute.com where it will join a growing collection of
analytics for the public good. You will get the credit for the algorithm, the
revenue and users from around the world.


---
description_short: Have you developed a mathematical model or computational / visualization
algorithm? Your algorithm could be useful worldwide to people who face a real-
world problem but do not want to write code. Professionals in industry,
scientists in other fields or researchers in developing countries can quickly
apply your work to their domain with a web-based cloud-computing platform like
CrossCompute.


---
tags: []
---
duration: None
---
language: English
---
source_url: https://www.youtube.com/watch?v=U1Y5Uxn2Rcw
---
copyright: 
---
slug: 34-crosscompute-is-a-web-platform-where-users-ca
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://img.youtube.com/vi/U1Y5Uxn2Rcw/hqdefault.jpg
---
embed: <object width="425" height="344"><param name="movie" value="http://www.youtube.com/v/U1Y5Uxn2Rcw?version=3&amp;f=videos&amp;app=youtube_gdata"><param name="allowFullScreen" value="true"><param name="allowscriptaccess" value="always"><embed src="http://www.youtube.com/v/U1Y5Uxn2Rcw?version=3&amp;f=videos&amp;app=youtube_gdata" allowscriptaccess="always" height="344" width="425" allowfullscreen="true" type="application/x-shockwave-flash"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 