_model: video
---
title: Python for High Performance Computing
---
id: 394
---
date_created: 2012-02-23T04:20:00
---
date_updated: 2014-04-08T20:28:28.017
---
date_recorded: 2011-03-11
---
event: PyCon US 2011
---
speakers: ['William Scullin']
---
description_long: Python for High Performance Computing

Presented by William Scullin

Python is becoming increasingly popular within the high performance computing
community. While it initially gained traction as a scripting language,
Python's role has continued to expand with Python applications for science
scaling to hundreds of thousands of cores and bindings to high performance
libraries becoming commonplace. This talk is meant as an overview of Python's
role in the HPC space.

Abstract

This talk is focused on raising awareness of Python in the high performance
computing space. Specific topics include:

  * building the Python interpreter for speed 
  * an overview of bindings to numerical libraries 
  * using GPUs and accelerators with Python 
  * scaling codes with MPI 
  * issues when scaling on very large systems 
  * an overview of successful science codes 
  * a live demonstration of Python running on 163,840 cores 


---
description_short: 
---
tags: ['gpu', 'highperformancecomputing', 'hpc', 'mpi', 'pycon', 'pycon2011']
---
duration: None
---
language: English
---
source_url: 
---
copyright: Creative Commons Attribution-NonCommercial-ShareAlike 3.0
---
slug: pycon-2011--python-for-high-performance-computing
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://a.images.blip.tv/Pycon-PyCon2011PythonForHighPerformanceComputing720.png
---
embed: 
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: http://05d2db1380b6504cc981-8cbed8cf7e3a131cd8f1c3e383d10041.r93.cf2.rackcdn.com/pycon-us-2011/394_python-for-high-performance-computing.mp4
---
video_ogv_download_only: False
---
video_ogv_length: 168164468
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 