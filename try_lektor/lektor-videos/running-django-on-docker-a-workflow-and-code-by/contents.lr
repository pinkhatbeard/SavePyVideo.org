_model: video
---
title: Running Django on Docker: a workflow and code
---
id: 3136
---
date_created: 2014-08-14T15:33:10.985
---
date_updated: 2014-08-14T19:10:28.665
---
date_recorded: 2014-08-09
---
event: PyCon AU 2014
---
speakers: ['Alexey Kotlyarov', 'Danielle Madeley']
---
description_long: Docker (http://docker.io) is the hot-new-thing out there for running your application in development and in production. Here at Infoxchange, we are extremely excited about how Docker allows a build-once, run-anywhere approach to moving applications from testing through to production. We are one of the first organisations to be using Docker in production on high-SLA applications.

In this talk we will present:

- a quick intro to Docker and how it solves our problems at Infoxchange;
- our interface for hosting applications on Docker ("pallet");
- how to build and develop a Python app on Docker;
- Infoxchange's Django settings module for the pallet interface;
- Forklift (https://github.com/infoxchange/docker-forklift), a tool we've developed for running and developing apps that use the pallet interface, in and out of a Docker container; and
- Extending forklift for your services.

If time permits we will try and cover the additional bonus topics:

- How to host your legacy mod_python application on Apache inside Docker with minimal changes; and
- Using Forklift to do automated full-stack integration testing of apps by spinning up containers of other apps (which we do in a container that is testing the built app that is running in a container that is our CI server... inception is possible).

The talk will be presented by Danielle Madeley and Alexey Kotlyarov.
---
description_short: 
---
tags: []
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=SgL8r--p97s
---
copyright: http://creativecommons.org/licenses/by/3.0/
---
slug: running-django-on-docker-a-workflow-and-code-by
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i.ytimg.com/vi/SgL8r--p97s/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/SgL8r--p97s?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/SgL8r--p97s?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: 
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: http://mirror.linux.org.au/pub/pycon-au/pyconau2014/19-out.ogv
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: needs editing