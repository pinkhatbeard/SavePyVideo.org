_model: video
---
title: Industrial Monitoring with the Wendelin Big Data platform
---
id: 3531
---
date_created: 2015-04-22T17:59:39.370
---
date_updated: 2015-04-22T17:59:39.370
---
date_recorded: 2015-04-14
---
event: PyData Paris 2015
---
speakers: ['Jean-Paul Smets', 'Sébastien Robin']
---
description_long: This talk presents the Wendelin Big Data “Full Stack” and introduces a first success story related to the collection of vibration data in wind turbines and the analysis of vibrations.

Wendelin (http://wendelin.io/) combines automated cluster deployment, distributed data persistency for NumPy arrays, parallel data processing, fluentd compliant data ingestion interface and JIO compliant javascript interface. It is an all­in­one open source solution that provides a 100% native python alternative to hybrid solutions based on Spark.

The talk is derived from the presentation made at MariaDB community event (https://mariadb.org/en/community-events/). The presentation of Wendelin Full Stack will be shorter than in Santa Clara in order to provide enough time to present the first implementation for Wind Turbines. We will show in particular which parts of data analysis are handled on server side with pydata libraries, which parts of data analysis are handled on browser side in javascript and how both can be integrated to minimize implementation costs.

The talk concludes on Wendelin platform roadmap.
---
description_short: 
---
tags: []
---
duration: None
---
language: English
---
source_url: https://www.youtube.com/watch?v=kyp8vHnpUXE
---
copyright: youtube
---
slug: industrial-monitoring-with-the-wendelin-big-data
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: https://i.ytimg.com/vi/kyp8vHnpUXE/hqdefault.jpg
---
embed: <iframe type='text/html' src='http://www.youtube.com/embed/kyp8vHnpUXE' width='640' height='360' frameborder='0' allowfullscreen='true'/>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: needs editing