_model: video
---
title: MiG - A Complete Grid Middleware (mostly) in Python
---
id: 1125
---
date_created: 2012-08-30T21:02:38
---
date_updated: 2014-04-08T20:28:27.849
---
date_recorded: 2011-07-13
---
event: EuroPython 2011
---
speakers: ['Jonas Bardino']
---
description_long: Grid computing was all the buzz in the beginning of the millennium and still
has serious attention in different forms although many of the original grand
promises were never delivered. The general level of ambitions have instead
slowly but steadily degraded to those of the latest buzz word, Cloud.

We as a project have proven that most of the original promises _can_ actually
be delivered and we have done so using Python almost solely as the
implementation language. The choice of Python provided us with a stable and
versatile base for quickly getting this far and it significantly eases
extending and maintaining our middleware in the future. MiG is currently about
50000 lines of source code but it still offers more features than competing
grid systems with millions of lines of code.

Apart from introducing the open source MiG middleware and summarizing how we
got here, this talk will outline some of the core technologies used to reach
that goal and underline why it can make a lot of sense to choose Python for
complex HPC projects like MiG, too. Talk keywords include Network Programming,
Open Source Python projects, Science and Math and Web-based Systems. There's
no special intended audience, but a certain level of Python knowledge and
experience may be an advantage. Please refer to
[http://code.google.com/p/migrid/](http://code.google.com/p/migrid/) for
further MiG information.


---
description_short: [EuroPython 2011] Jonas Bardino - 22 June 2011 in "Track Tagliatelle "


---
tags: ['forms', 'hpc', 'network', 'science']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=enNcrMTg2lM
---
copyright: Standard YouTube License
---
slug: mig-a-complete-grid-middleware-mostly-in-pyt-0
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i.ytimg.com/vi/enNcrMTg2lM/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/enNcrMTg2lM?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/enNcrMTg2lM?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 