_model: video
---
title: What is Google App Engine?
---
id: 1011
---
date_created: 2012-08-30T21:02:07
---
date_updated: 2014-04-08T20:28:27.728
---
date_recorded: 2011-07-24
---
event: EuroPython 2011
---
speakers: ['wesley chun']
---
description_long: Google App Engine is a unique hosting platform that lets you build
applications and run them in Google's data centers using the massive global
infrastructure built to run the Internet's most powerful company. App Engine
offers a development environment that uses familiar technologies (Java and
Python) and provides a powerful and robust set of APIs to users while
maintaining security and independence from other apps running in the cloud. It
is always free to get started so you can try it out with no risk, and if you
need additional computing resources, you can purchase additional computing
resources beyond the free quota limits. (If you enable billing and trust us
with your credit card, we will extend your free quotas even further; you won't
get charged until you exceed those _extended_ quotas.) Scale your application
to millions of users and pay only for what you use at competitive market
pricing.

In this session, we provide an update of the newest features found in the most
recent releases of the App Engine platform. We also share some suggestions for
best practices to existing App Engine developers.

Beginners to the App Engine platform will be interested in the introductory
workshop which may be offered (see description below).

Google App Engine workshop

In this tutorial, we'll give you a comprehensive introduction to the platform
in two/three components:

  * 1-hour Introduction to Cloud computing and Google App Engine seminar
  * 3-hour App Engine hands-on workshop/codelab

In the first hour, we review Cloud Computing as an industry and where Google
App Engine fits into the picture. Specifically, we discuss App Engine as a
PaaS solution because of the inherent challenges of building web and other
applications. We'll outline the architecture of App Engine, what it's major
components are, introduce its features and APIs, discuss the service and how
it works (including information on the free quotas), present some information
about current users and usage, including integration with Google Apps, and
finally, give an overview of its enterprise edition called Google App Engine
for Business.

After the approximately one-hour lecture, we'll show you how to create
applications that run on App Engine by building a simple but real web
application from the ground up via a hands-on coding laboratory. Although
based on the online tutorial, this codelab goes up and beyond what's in the
documentation: you will get a more detailed step-by-step instructions to
replicate that example as well as have the opportunity to extend your
application with some of the newer APIs that come with App Engine. The codelab
will cover the Users service, non-relational Datastore, and Memcache APIs.
Time-permitting, we'll also discuss some of the newest features found in
recent App Engine releases.


---
description_short: [EuroPython 2011] wesley chun - 23 June 2011 in "Track Tagliatelle"


---
tags: ['architecture', 'cloud', 'google', 'hosting', 'infrastructure', 'memcache', 'security', 'web']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=YiimQ3zyxRQ
---
copyright: Standard YouTube License
---
slug: what-is-google-app-engine
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i.ytimg.com/vi/YiimQ3zyxRQ/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/YiimQ3zyxRQ?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/YiimQ3zyxRQ?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 