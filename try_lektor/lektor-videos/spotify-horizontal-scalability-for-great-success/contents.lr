_model: video
---
title: Spotify: Horizontal scalability for great success
---
id: 1096
---
date_created: 2012-08-30T21:02:29
---
date_updated: 2014-04-08T20:28:27.895
---
date_recorded: 2011-07-13
---
event: EuroPython 2011
---
speakers: ['Nick Barkas']
---
description_long: If you run on CPython, it's not possible to get a single-process,
multithreaded Python program to use more than one CPU core at a time because
of the Global Interpreter Lock (GIL). A common way of dealing with this is to
run one instance of a Python program for each core a machine has and spread
the load amongst those processes. This forces developers to write simple,
stateless programs that naturally scale out to many many servers when needed,
while also not having to think about things like locking and thread
scheduling.

I'll discuss some tools and methods Spotify's backend uses for managing
multiple identical server processes as well as load balancing with DNS, proxy
servers, and using hashing to send repeated requests to the same process. I
will also talk about the difficulties that arise when you really need to share
data or state between processes, and how they can be dealt with.


---
description_short: [EuroPython 2011] Nick Barkas - 22 June 2011 in "Track Spaghetti"


---
tags: ['requests', 'scalability']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=BBAfIYpDMX4
---
copyright: Standard YouTube License
---
slug: spotify-horizontal-scalability-for-great-success
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i.ytimg.com/vi/BBAfIYpDMX4/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/BBAfIYpDMX4?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/BBAfIYpDMX4?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 