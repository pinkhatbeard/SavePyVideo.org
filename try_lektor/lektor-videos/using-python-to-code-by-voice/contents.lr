_model: video
---
title: Using Python to Code by Voice
---
id: 1735
---
date_created: 2013-03-13T18:55:18
---
date_updated: 2014-04-08T20:28:26.612
---
date_recorded: 2013-03-17
---
event: PyCon US 2013
---
speakers: ['Tavis Rudd']
---
description_long: Two years ago I developed a case of Emacs Pinkie (RSI) so severe my hands went numb and I could no longer type or work. Desperate, I tried voice recognition. At first programming with it was painfully slow but, as I couldn’t type, I persevered. After several months of vocab tweaking and duct-tape coding in Python and Emacs Lisp, I had a system that enabled me to code faster and more efficiently by voice than I ever had by hand.

In a fast-paced live demo, I will create a small system using Python, plus a few other languages for good measure, and deploy it without touching the keyboard. The demo gods will make a scheduled appearance. I hope to convince you that voice recognition is no longer a crutch for the disabled or limited to plain prose. It’s now a highly effective tool that could benefit all programmers. 
---
description_short: I dictate my code using a voice recognition system with Python embedded in it.  In a fast paced live demo, I will code a small system and deploy it without touching the keyboard. I hope to convince you that voice recognition is no longer a crutch for the disabled or limited to plain prose.
---
tags: ['talk']
---
duration: None
---
language: English
---
source_url: https://www.youtube.com/watch?v=8SkdfdXWYaI
---
copyright: CC
---
slug: using-python-to-code-by-voice
---
related_urls: [{'description': "Tavis's github account", 'url': 'https://github.com/tavisrudd'}]
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/8SkdfdXWYaI/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/8SkdfdXWYaI?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/8SkdfdXWYaI?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: True
---
video_mp4_length: None
---
video_mp4_url: http://s3.us.archive.org/nextdayvideo/psf/pycon2013/Using_Python_to_Code_by_Voice.mp4?Signature=8iV87JN7k7IweE10nJanN%2FAUdVY%3D&Expires=1363825863&AWSAccessKeyId=FEWGReWX3QbNk0h3
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: 
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 