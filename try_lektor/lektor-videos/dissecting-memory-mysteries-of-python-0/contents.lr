_model: video
---
title: Dissecting memory mysteries of Python
---
id: 2359
---
date_created: 2013-10-19T19:48:45
---
date_updated: 2014-04-08T20:28:26.093
---
date_recorded: 2013-09-12
---
event: Kiwi PyCon 2013
---
speakers: ['Chetan Giridhar', 'Vishal Kanaujia']
---
description_long: @ Kiwi PyCon 2013 - Saturday, 07 Sep 2013 - Track 2

**Audience level**

Intermediate

**Abstract**

Python is a dynamically typed language. Applications leave task of object memory management to Python VM. Python automatically manages memory using reference counting and garbage collection. But, Python memory manager may bloat the VM size, and sometimes it may consume complete main memory. It causes applications to deliver low performance and encounter unexpected memory errors.

This talk dissects the internals of CPython memory manager, its limitations and negative impact on application behavior. We demonstrate the problem of memory leaks by learning Python heap pattern, object graphs and memory profiling. Next, we suggest solutions to reduce memory footprints of applications, tools to diagnose and fix memory leaks and lesson learned as best development practices.

**Slides**

https://speakerdeck.com/nzpug/vishal-kanaujia-dissecting-memory-mysteries-of-python
---
description_short: Memory leak has been perennial problem for Python applications. This causes application to behave erroneously with Memory error and very slow operation. What is wrong with the application? How to find out the cause and fix it? This is the motivation for this talk.

---
tags: []
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=7glw_5YFU-Q
---
copyright: 
---
slug: dissecting-memory-mysteries-of-python-0
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/7glw_5YFU-Q/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/7glw_5YFU-Q?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/7glw_5YFU-Q?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: 
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: 
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 