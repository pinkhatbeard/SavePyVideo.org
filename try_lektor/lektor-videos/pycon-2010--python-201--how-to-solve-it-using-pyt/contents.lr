_model: video
---
title: Python 201: How to Solve It Using Python
---
id: 253
---
date_created: 2012-02-23T04:20:00
---
date_updated: 2014-04-08T20:28:28.180
---
date_recorded: 2010-02-19
---
event: PyCon US 2010
---
speakers: ['Senthil Kumaran']
---
description_long: Python 201: How to Solve it using Python

Presented by Senthil Kumaran

One good way to develop applications is to divide the problem into smaller
manageable chunks and the build those. Once we identify and define those
smaller piece of tasks, our problem becomes incredibly easy.

But we are still faced with the question of what is the best way to do that
smaller task in Python? This tutorial will answer those questions.

In this tutorial, we will identify the commonly occurring tasks that would fit
into to bigger problems and learn how do we solve it using Python.

The aim of the tutorial is help you develop the correct intuition when
developing applications using Python.

Intended Audience

Beginning to intermediate Python Programmers. Familiarity with Python's syntax
and usage is assumed.

This tutorial will use Python 3.1 examples (with changes from Python 2.6
explained, when it is required).

The source code and the handout will contain the snippets written in both
Python 2.6 and Python 3.1.

Class Outline

  * Outline of the Tutorial and a Brief Overview of Standard Library. 
  * Lets start with Strings. 
  * Files - We deal with them often. 
  * Date time related tasks. 
  * Dealing with Database stuff. 
  * Process Handling. 
  * Processing XMLs. 
  * Web Programming 
  * Programming tasks. 
  * Unit Testing 
  * How to Convert Python 2 to Python 3. 

Requirements

Attendees are welcome to bring their laptops with Python installed ( version
3.1 and version 2.6 both installed).

Some snippets will be using Twisted Matrix Framework, those who would like to
try those recipes, would like to have Twisted Package installed for their
Python Distribution.

[VIDEO HAS ISSUES: Some missing sections of video due to technical issues
(approx 3 mins in)]


---
description_short: 
---
tags: ['pycon', 'pycon2010', 'python-basics', 'testing', 'tutorial', 'xml']
---
duration: None
---
language: English
---
source_url: 
---
copyright: Creative Commons Attribution-NonCommercial-ShareAlike 3.0
---
slug: pycon-2010--python-201--how-to-solve-it-using-pyt
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://a.images.blip.tv/Pycon-PyCon2010Python201HowToSolveItUsingPython440-675.jpg
---
embed: 
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: http://05d2db1380b6504cc981-8cbed8cf7e3a131cd8f1c3e383d10041.r93.cf2.rackcdn.com/pycon-us-2010/253_python-201-how-to-solve-it-using-python.m4v
---
video_ogv_download_only: False
---
video_ogv_length: 1023524470
---
video_ogv_url: http://05d2db1380b6504cc981-8cbed8cf7e3a131cd8f1c3e383d10041.r93.cf2.rackcdn.com/pycon-us-2010/253_python-201-how-to-solve-it-using-python.ogv
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 