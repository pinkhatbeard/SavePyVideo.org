_model: video
---
title: Rasterio: Geospatial Raster Data Access for Programmers and Future Programmers
---
id: 2771
---
date_created: 2014-07-15T22:45:37.217
---
date_updated: 2014-07-15T23:11:37.393
---
date_recorded: 2014-07-14
---
event: SciPy 2014
---
speakers: ['Sean Gillies']
---
description_long: Rasterio is a GDAL and Numpy-based Python library guided by lessons learned over a decade of using GDAL and Python to solve geospatial problems. Among these lessons: the importance of productivity, enjoyability, and serendipity.

I will discuss the motivation for writing Rasterio and explain how and why it diverges from other GIS software and embraces Python types, protocols, and idioms.  I will also explain why Rasterio adheres to some GIS paradigms and bends or breaks others.

Finally, I will show examples of using Rasterio to read, manipulate, and write georeferenced raster data. Some examples will be familiar to users of older Python GIS software and will illustrate how Rasterio lets you get more done with less code and fewer bugs. I will also demonstrate fun and useful features of Rasterio not found in other geospatial libraries.

---
description_short: Learn to read, manipulate, and write georeferenced imagery and other kinds of geospatial raster data using a productive and fun GDAL and Numpy-based library named Rasterio. It's a new open source project from the satellite team at Mapbox and is informed by a decade of experience using Python and GDAL.
---
tags: ['gis']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=yI9_ozSIKlk
---
copyright: http://www.youtube.com/t/terms
---
slug: rasterio-geospatial-raster-data-access-for-progr
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/yI9_ozSIKlk/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/yI9_ozSIKlk?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/yI9_ozSIKlk?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: 
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: 
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 