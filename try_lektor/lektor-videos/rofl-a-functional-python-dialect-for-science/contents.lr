_model: video
---
title: ROFL: a functional Python dialect for science
---
id: 1191
---
date_created: 2012-08-31T16:34:22
---
date_updated: 2014-04-08T20:28:27.158
---
date_recorded: 2012-07-18
---
event: SciPy 2012
---
speakers: ['Jonathan Riehl']
---
description_long: Current parallel programming models leave a lot to be desired and fail to
maintain pace with improvements in hardware architecture. For many scientific
research groups these models only widen the gap between equations and scalable
parallel code. The Resilient Optimizing Flow Language (ROFL) is a data-flow
language designed with the purpose of solving the problems of both domain
abstraction and efficient parallelism. Using a functional, declarative variant
of the Python language, ROFL takes scientific equations and optimizes for both
scalar and parallel execution.

ROFL is closely tied to Python and the SciPy libraries. ROFL uses Python
expression syntax, is implemented in Python, and emits optimized Python code.
ROFL's implementation in Python allows ROFL to be embedded in Python. Using
Python as a target language makes ROFL extensible and portable. By removing
imperative loop constructs and focusing on integration with the NumPy and
SciPy libraries, ROFL both supports and encourages data parallelism.

In this presentation, we introduce the ROFL language, and demonstrate by
example how ROFL enables scientists to focus more on the equations they are
solving, and less on task and data parallelism.


---
description_short: 
---
tags: ['hpc']
---
duration: None
---
language: English
---
source_url: https://www.youtube.com/watch?v=OF3CYqj4H9U
---
copyright: CC BY-SA
---
slug: rofl-a-functional-python-dialect-for-science
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i4.ytimg.com/vi/OF3CYqj4H9U/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/OF3CYqj4H9U?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/OF3CYqj4H9U?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: http://s3.us.archive.org/nextdayvideo/enthought/scipy_2012/ROFL_a_functional_Python_dialect_for_science.mp4?Signature=s%2FDUCoB4OybZmEOzK2fg9hTBccw%3D&Expires=1346445141&AWSAccessKeyId=FEWGReWX3QbNk0h3
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 