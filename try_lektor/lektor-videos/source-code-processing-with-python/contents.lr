_model: video
---
title: Source code processing with Python
---
id: 1099
---
date_created: 2012-08-30T21:02:30
---
date_updated: 2014-04-08T20:28:27.892
---
date_recorded: 2011-07-13
---
event: EuroPython 2011
---
speakers: ['Kay Schluehr']
---
description_long: Folklore says that having a problem and trying to solve it with regular
expressions gives you two problems. However not applying regular expressions
to advanced textual search'n replace doesn't solve your problem either. One
step above you have large portions of recursively structured text aka "source
code" and using context free grammars and tools supporting them gives you two
problems but not using them also doesn't solve your original problem. Maybe
you get uneasy at that point because what I say implies parsers and computing
science and what not and you still wake up in the night believing that you
have to learn automata theory but you are lucky it was just a nightmare.
Otherwise you are laughing about the little diatribe against regexps and use
them without much deliberation, verifying your SQL input, mining source code
and do all the other things they are not made for.

In my talk I'm addressing daily use of grammars outside of the scope of
compiler implementation or natural language processing. My talk covers:

  * Search & Replace using grammars

  * CodeTemplates for source code transformation

  * Generative grammars for expression generation

I'm touching this from the lightweight, "pythonic" angle and you might wonder
why not everyone uses those techniques already for decades in their daily
work. I can't answer this, I wonder about this too.


---
description_short: [EuroPython 2011] Kay Schluehr - 24 June 2011 in "Track Lasagne"


---
tags: ['processing', 'science']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=tjF5jLD31kQ
---
copyright: Standard YouTube License
---
slug: source-code-processing-with-python
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i.ytimg.com/vi/tjF5jLD31kQ/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/tjF5jLD31kQ?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/tjF5jLD31kQ?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 