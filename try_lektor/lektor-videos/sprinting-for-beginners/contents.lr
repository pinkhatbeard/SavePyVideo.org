_model: video
---
title: Sprinting For Beginners
---
id: 3952
---
date_created: 2015-10-18T16:10:52.289
---
date_updated: 2015-10-18T16:10:52.289
---
date_recorded: 2015-08-04
---
event: PyCon AU 2015
---
speakers: ['Tennessee Leeuwenburg']
---
description_long: What do sprinters eat before a race?
Nothing, they fast.

Sprinting at PyCon AU is not like that at all. Instead, it's a great chance to do something good for the community and get to know some fellow devs along the way. The sprints are an semi-formal two-day event following the close of the main track of the conference where people get together and code on whatever takes their fancy. Most of the time, it's a a chance to work on an open source package you're already involved with in some way, or to meet face-to-face with people you otherwise know only by email.

However, there are also those brave souls who are joining in for the first time. It can be particularly intimidating if you don't know the others there or how to really get started. This presentation will talk about the sprint format, how to get in with a sprint group, and how to be productive in such a short time (especially if you're new to the concept).

We'll cover off basics like getting your environment set up, checking out the code, and identifying appropriate projects and tasks to work on. Standard workflows and tips on how and when to approach people for assistance can also help to make things flow more smoothly.

It will also include some tips for sprint leaders about how to make your project easy and enjoyable for others to hack on. There is a lot than can be done without a lot of effort, such as pre-classifying bugs or identifying appropriate tasks for newcomers, identifying "go-to" people in your project, and spending time on the day talking to people and getting them comfortable working on your code. Clearly documenting simple workflows (like how to submit a patch, what code standards are used, and how to validate results) can help tremendously.

With any luck, this presentation can help both sprint leaders and attendees with a smoother experience which gets the difficulties out the way and leaves maximum opportunity for coding and enjoyment.

Tennessee has gone through the meatgrinder of sprinting, from sitting down with a group of strangers trying to help them fix trivial bugs, to developing significant extensions, to leading sprints. With the scars to show and lessons learned, he is in a reasonable position to help suggest some simple things that can make sprinting easier and more enjoyable for the newcomer.


---
description_short: 
---
tags: []
---
duration: None
---
language: English
---
source_url: https://www.youtube.com/watch?v=6NUBO1DDQAY
---
copyright: creativeCommon
---
slug: sprinting-for-beginners
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: https://i.ytimg.com/vi/6NUBO1DDQAY/hqdefault.jpg
---
embed: <iframe width="640" height="360" src="//www.youtube.com/embed/6NUBO1DDQAY" frameborder="0" allowfullscreen></iframe>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: needs editing