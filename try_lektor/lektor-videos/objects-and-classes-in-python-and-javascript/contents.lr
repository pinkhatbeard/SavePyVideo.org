_model: video
---
title: Objects and classes in Python (and JavaScript)
---
id: 1041
---
date_created: 2012-08-30T21:02:14
---
date_updated: 2014-04-08T20:28:27.745
---
date_recorded: 2011-07-21
---
event: EuroPython 2011
---
speakers: ['Jonathan Fine']
---
description_long: Python has a well-thought out system for classes. Beginners can use it without
understanding it all. Experts can use it to produce code that is both elegant
and powerful (such as models and class-based views in Django). Python classes
can be used for many purposes.

This tutorial arises from the author's experience in using classes to solve
problems, such as:

  * Add custom methods and attributes to objects that are, at root, just an integer
  * Construct dictionaries whose values are functions
  * Construct classes on-the-fly
  * Implement JavaScript object semantics in Python (advanced topic)

The outline syllabus is:

  * Review of the class statement in Python (and decorators)
  * Subclassing built-in types such as int and tuple
  * How to define classes without using a class statement
  * How to define a dispatch dictionary using a class statement
  * Metaclasses as syntactic sugar for class construction
  * Metaclasses to provide new class semantics
  * Review of JavaScript object semantics
  * Using Python classes to implement JavaScript object semantics

For Guido on the history of Python classes see:

  * [http://python-history.blogspot.com/2010/06/new-style-classes.html](http://python-history.blogspot.com/2010/06/new-style-classes.html)
  * [http://python-history.blogspot.com/2010/06/inside-story-on-new-style-classes.html](http://python-history.blogspot.com/2010/06/inside-story-on-new-style-classes.html)
  * [http://python-history.blogspot.com/2010/06/method-resolution-order.html](http://python-history.blogspot.com/2010/06/method-resolution-order.html)

This tutorial is for Intermediate or Advanced Python programmers. (Beginners
will find it very hard going.) The aim of the course is to explain exactly
what happens when a class in constructed, and to learn ways of using this
knowledge.


---
description_short: [EuroPython 2011] Jonathan Fine - 23 June 2011 in "Training Pizza Napoli "


---
tags: ['dictionaries', 'javascript', 'tutorial']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=e0tK-Kawx3E
---
copyright: Standard YouTube License
---
slug: objects-and-classes-in-python-and-javascript
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i.ytimg.com/vi/e0tK-Kawx3E/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/e0tK-Kawx3E?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/e0tK-Kawx3E?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 