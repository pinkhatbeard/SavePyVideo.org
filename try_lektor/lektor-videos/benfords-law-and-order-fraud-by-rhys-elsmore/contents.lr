_model: video
---
title: (Benford's) Law and Order (Fraud) by Rhys Elsmore
---
id: 3107
---
date_created: 2014-08-14T15:33:07.490
---
date_updated: 2014-08-14T15:39:15.171
---
date_recorded: 2014-08-11
---
event: PyCon AU 2014
---
speakers: ['Rhys Elsmore']
---
description_long: "On the internet, fraudulent and abusive behavior is considered especially heinous. At Heroku, the dedicated detectives who investigate these vicious felonies are members of an elite squad armed with large amounts of data and spare CPU cycles. These are their stories."

Bad behavior can wreak havoc on your web application. It might be mass-signups, fraudulent orders, spammy posts, right up to automated bots designed to work around restrictions you have set in place; this can cost you time, resources, and lots of money. All is not lost though. Despite the ongoing efforts of abusers, their activity still leaves fingerprints and clues, which you can use to your advantage.

This talk is a 101 introduction to some of the methods which you can use to separate good from bad users using a combination of data mining, statistics, and some some basic machine learning. Basically, I want to get you thinking like an internet detective.

Some of the topics I will be covering include:

- Collecting and preparing data sources.
- Effective methods for classifying existing users.
- Feature extracting; what works and what doesn't.
- Analyzing user-provided data to profile your users, and weed out the bad operators.
- Determining a user's intentions by looking at their access patterns.
- Making use of 'outliers' to find suspicious users and transactions.
- Stopping bad users before they can wreak havoc.

As this is a 101 topic I will provide some basic examples, as well as links to more in-depth resources for further reading. I would recommend this talk to developers of web applications, especially those with a large number of users, the ability to process credit cards, or with a 'free' offering. Attendees should have a basic understanding of topics such as SQL, Pandas, and some basic understanding of mathematics and statistics, although this is not essential as I will be providing links to further reading.
---
description_short: 
---
tags: []
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=_VO8QxIkjqY
---
copyright: http://www.youtube.com/t/terms
---
slug: benfords-law-and-order-fraud-by-rhys-elsmore
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i.ytimg.com/vi/_VO8QxIkjqY/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/_VO8QxIkjqY?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/_VO8QxIkjqY?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: 
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: http://mirror.linux.org.au/pub/pycon-au/pyconau2014/56-out.ogv
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: needs editing