_model: video
---
title: Python and Robots: Teaching Programming in High School
---
id: 413
---
date_created: 2012-02-23T04:20:00
---
date_updated: 2014-04-08T20:28:28.014
---
date_recorded: 2011-03-11
---
event: PyCon US 2011
---
speakers: ['Vern Ceder']
---
description_long: Python and Robots: Teaching Programming in High School

Presented by Vern Ceder

Combining Python with inexpensive robots is a very effective way of teaching
programming at the middle and high school levels. Since Python is easy to
understand a constructivist approach is possible - students learn by creating
and running simple programs, observing the results, and then modifying their
code to fix bugs and add functionality.

Abstract

The approach suggested in this talk is partly based upon that developed at the
Institute for Personal Robots in Education
([http://wiki.roboteducation.org](http://wiki.roboteducation.org/)) by staff
from Georgia Tech and Bryn Mawr, combined with my own experiences teaching
programming with Python as described in my talk "Goodbye, Hello World:
Rethinking Teaching with Python", PyCon 2007, and my subsequent talks at NECC.

Because students are able to see what their code is doing and because Python
is easy to understand, students can explore simple programming concepts,
learning features as they need them. This approach increases both student
engagement and retention. It also seems that this approach is more appealing
to girls than a more traditional programming class.

I'll illustrate my talk with samples of code created by students and video of
the students/robots in action.

Outline

Introduction - school background, course structure, origin of approach

Hardware and computer setup used

Initial exercises and first projects

  * Control of robot 
  * Program as sequence of commands 
  * Basic programming concepts - looping, branching, functions 

Advanced projects

    * obstacle detection 
    * image processing 
    * simple AI approaches 

Pitfalls and strategies for using robots

Questions and Suggestions


---
description_short: 
---
tags: ['highschool', 'pycon', 'pycon2011', 'robots', 'teaching']
---
duration: None
---
language: English
---
source_url: 
---
copyright: Creative Commons Attribution-NonCommercial-ShareAlike 3.0
---
slug: pycon-2011--python-and-robots--teaching-programmi
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://a.images.blip.tv/Pycon-PyCon2011PythonAndRobotsTeachingProgrammingInHighSchool378.png
---
embed: 
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: http://05d2db1380b6504cc981-8cbed8cf7e3a131cd8f1c3e383d10041.r93.cf2.rackcdn.com/pycon-us-2011/413_python-and-robots-teaching-programming-in-high-school.mp4
---
video_ogv_download_only: False
---
video_ogv_length: 258289243
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 