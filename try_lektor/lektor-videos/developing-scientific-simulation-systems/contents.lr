_model: video
---
title: Developing Scientific Simulation Systems
---
id: 3330
---
date_created: 2014-11-05T22:41:04.060
---
date_updated: 2014-11-05T22:41:04.060
---
date_recorded: 2014-10-22
---
event: EuroScipy 2014
---
speakers: ['Mike Müller']
---
description_long: Scientific simulation models often need to incorporate many aspects into a
working software system. This talk gives an overview of
[PITLAKQ](http://www.pitlakq.com/), a complex, coupled model for water
quality predictions of pit lakes. The focus is on experiences gained that
might be useful for similar projects from other domains. This includes
wrapping of legacy code, creating flexible but simple to use model input
approaches and simplifying automation by users.  Description

The model PITLAKQ helps to predict the water quality of pit lakes. These
lakes can form in voids created by mining and may pose considerable
environmental challenges. PITLAKQ couples several existing numerical models
and adds lots of new functionality to account for the complex interactions
of hydrodynamics, geochemical processes and anthropogenic influences.

Obviously, there is lots of scientific details. The talk only gives a very
short overview of the functionality of the software. The main focus is on
general principles that can be applied to other domains of scientific
modeling. This includes:

* wrapping legacy Fortran code
* coupling models from different domains
* creating a simple, yet flexible input system for model users
* simplifying automation of simulation runs

I think the lessons I learned over the last 15 years I've been working on
this project can be useful for other scientists who need to solve simulation
and modeling problems in other fields.

---
description_short: 
---
tags: []
---
duration: None
---
language: English
---
source_url: https://www.youtube.com/watch?v=Fo2aZGDgFCM
---
copyright: youtube
---
slug: developing-scientific-simulation-systems
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: https://i.ytimg.com/vi/Fo2aZGDgFCM/hqdefault.jpg
---
embed: <iframe type='text/html' src='http://www.youtube.com/embed/Fo2aZGDgFCM' width='640' height='360' frameborder='0' allowfullscreen='true'/>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: 
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: 
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 