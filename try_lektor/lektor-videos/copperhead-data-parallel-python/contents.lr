_model: video
---
title: Copperhead: Data Parallel Python
---
id: 1197
---
date_created: 2012-08-31T16:34:35
---
date_updated: 2014-04-08T20:28:27.140
---
date_recorded: 2012-07-18
---
event: SciPy 2012
---
speakers: ['Bryan Catanzaro']
---
description_long: Copperhead is a data parallel language embedded in Python, which aims to
provide both a productive programming environment as well as excellent
computational efficiency on heterogeneous parallel hardware. Copperhead
programs are written in a small, restricted subset of Python, using standard
constructs like map and reduce, along with traditional data parallel
primitives like scan and sort. Copperhead programs are written in standard
Python modules and interoperate with existing Python numerical and
visualization libraries such as NumPy, SciPy, and Matplotlib. The Copperhead
runtime compiles Copperhead programs to target either CUDA-enabled GPUs or
multicore CPUs using OpenMP or Threading Building Blocks. On several example
applications from Computer Vision and Machine Learning, Copperhead programs
achieve between 45-100% of the performance of hand-coded CUDA code, running on
NVIDIA GPUs. In this talk, we will discuss the subset of Python that forms the
Copperhead language, the open source Copperhead runtime and compiler, and
selected example programs.


---
description_short: 
---
tags: ['hpc']
---
duration: None
---
language: English
---
source_url: http://youtube.com/watch?v=FpDVyAwz2qM
---
copyright: CC BY-SA
---
slug: copperhead-data-parallel-python
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i3.ytimg.com/vi/FpDVyAwz2qM/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/FpDVyAwz2qM?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/FpDVyAwz2qM?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: http://s3.us.archive.org/nextdayvideo/enthought/scipy_2012/Copperhead_Data_Parallel_Python.mp4?Signature=laRgCy3mlIsaXoYCTKhlhu6Orck%3D&Expires=1346380548&AWSAccessKeyId=FEWGReWX3QbNk0h3
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 