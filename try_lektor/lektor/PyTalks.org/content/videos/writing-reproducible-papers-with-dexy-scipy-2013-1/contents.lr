_model: video
---
title: Writing Reproducible Papers with Dexy; SciPy 2013 Presentation
---
id: 2117
---
date_created: 2013-07-04T10:08:56
---
date_updated: 2014-04-08T20:28:26.456
---
date_recorded: 2013-07-01
---
event: SciPy 2013
---
speakers: []
---
description_long: 
---
description_short: Authors: Nelson, Ana

Track: Reproducible Science

Scientists are frequently admonished to create reproducible papers, but one reason so few do is the lack of good tools. Imagine you want to create a reproducible paper that any other researcher in your field can easily grab, look at, and run in order to verify your results. What would you use? A tool intended for generating API documentation? A literate programming tool? Maybe some hand coded scripts binding together a rickety project that nobody but you can run? The truth is, the tools available to scientists to create clean reproducible papers are too limited, not general purpose enough, and not portable enough for sharing with other scientists.

This talk will be a demonstration of generating a paper using Dexy, a tool that has been written from scratch for making it as painless as possible to write truly reproducible technical documents. The whole project workflow will be automated including gathering and cleaning input data, running analysis scripts, producing plots, and compiling output documents to PDF, HTML and ePub output formats. The example project will include software and scripts in Python as well as other programming languages. We will automate running the code, applying syntax highlighting, generating plots and other files as side effects from running the code, and incorporating all these elements into the various documents we wish to create.
---
tags: ['Tech']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=u6_qtDJ6ciA
---
copyright: http://www.youtube.com/t/terms
---
slug: writing-reproducible-papers-with-dexy-scipy-2013-1
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/u6_qtDJ6ciA/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/u6_qtDJ6ciA?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/u6_qtDJ6ciA?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: needs editing