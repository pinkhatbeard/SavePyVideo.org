_model: video
---
title: Minimalism in software development, or why you should do less
---
id: 1243
---
date_created: 2012-09-06T22:33:03
---
date_updated: 2014-04-08T20:28:27.209
---
date_recorded: 2012-07-06
---
event: EuroPython 2012
---
speakers: ['Fredrik Haard']
---
description_long: Lean and agile promises - when implemented right - to reduce the pain of
process and management overhead. As agile proponents say (the better of them
at least), this does not actually solve all our problems, it merely lets us
see them clearer. And one thing that we can see clear once management overhead
goes down, is that we are perfectly able to create our own, technical,
overhead. Developer culture and a tendency of solving problems ‘the usual way’
can - and does - create complicated solutions to simple problems, and
regularly leaves us in configuration hell. This is a talk about frameworks and
API:s, programming languages and data storage, and why we - developers - spend
a large amount of time not creating value. Even if we have the most perfect of
agile implementations with zero process overhead (yeah, right!), we still
waste time in development, and its all our own fault. I will discuss what we
spend our time on, why any framework should be approached with trepidation,
and how we can do better than keep repeating the mistakes of yesteryear.


---
description_short: [EuroPython 2012] Fredrik Haard - 5 JULY 2012 in "Track Lasagne"


---
tags: []
---
duration: None
---
language: None
---
source_url: http://www.youtube.com/watch?v=kEqwe7EGuQU
---
copyright: Standard YouTube License
---
slug: minimalism-in-software-development-or-why-you-sh
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i.ytimg.com/vi/kEqwe7EGuQU/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/kEqwe7EGuQU?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/kEqwe7EGuQU?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 