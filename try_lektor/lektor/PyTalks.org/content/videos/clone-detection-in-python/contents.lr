_model: video
---
title: Clone detection in python
---
id: 1308
---
date_created: 2012-09-06T22:33:20
---
date_updated: 2014-04-08T20:28:27.236
---
date_recorded: 2012-07-05
---
event: EuroPython 2012
---
speakers: ['V Maggio']
---
description_long: The clone detection is a longstanding and very active research area in the
field of Software Maintenance aimed at identifying duplications in source
code. The presence of clones may affect maintenance activities. For example,
errors in the “original” fragment of code have to be fixed in every clone. To
make things worse, code clones are usually not documented and so their
location in the source code is not known. In case of small-size software
systems the clone detection may be manually performed, but on large software
systems it can be accomplished only by means of automatic techniques. In this
talk an approach that exploits structural (i.e., AST) and lexical information
of the code (e.g., name of methods, variables) for the identification of
clones will be presented. The main innovation of such approach is represented
by the adoption of a Machine Learning technique based on (Tree) Kernel
functions. Some insights on mathematical properties of these Kernel-based
method along with its corresponding (efficient) Python implementation (Numpy,
Scipy) will be presented. Afterwards the talk will be focused on the
explanation of some detection results gathered on well-known Python systems
(Eric, Plone, networkx, Zope), compared with other non-Python ones (Eclipse-
Jdtcore, JHotDraw). The aim of this part will be to analyze what are the
Python features that could possibly avoid (or allow) duplications w.r.t. other
OO languages. Some snippets for analyzing the Python code “by itself” will be
also presented, emphasizing the powerful Python built-in reflection
capabilities, extremely useful in this specific code analysis task. Basic
maths skill and basic knowledge of the Python language are the only suggested
prerequisites for the talk.


---
description_short: [EuroPython 2012] V Maggio - 4 JULY 2012 in "Track Spaghetti"


---
tags: []
---
duration: None
---
language: None
---
source_url: http://www.youtube.com/watch?v=xmsK1geCDq4
---
copyright: Standard YouTube License
---
slug: clone-detection-in-python
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i.ytimg.com/vi/xmsK1geCDq4/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/xmsK1geCDq4?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/xmsK1geCDq4?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 