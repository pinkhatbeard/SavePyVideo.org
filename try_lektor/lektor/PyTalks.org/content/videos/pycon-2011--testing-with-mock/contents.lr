_model: video
---
title: Testing with mock
---
id: 392
---
date_created: 2012-02-23T04:20:00
---
date_updated: 2014-04-08T20:28:28.045
---
date_recorded: 2011-03-11
---
event: PyCon US 2011
---
speakers: ['Michael Foord']
---
description_long: Testing with mock

Presented by Michael Foord

mock is a Python testing library. It has the goal of making mocking in tests
brain dead simple! mock provides the Mock class and the patch decorator for
safely patching out the objects you are mocking in your tests. This talk will
cover standard mocking patterns. We'll also look at some of the newer features
in the latest release, including support for mocking magic methods.

Abstract

mock provides a core Mock class that removes the need to create a host of
trivial stubs throughout your test suite. After performing an action, you can
make assertions about which methods / attributes were used and arguments they
were called with. You can also specify return values and set specific
attributes in the normal way.

  * [http://www.voidspace.org.uk/python/mock/](http://www.voidspace.org.uk/python/mock/)
  * [http://pypi.python.org/pypi/mock/](http://pypi.python.org/pypi/mock/)

The mock module also provides a patch() decorator that handles safely patching
out the things you are mocking during your test.

We'll cover standard mocking patterns, and how mock makes them easy. We'll
also be looking at some of the newer features in the latest release, including
the magic method support that can be used (for example) for mocking out
objects used as context managers.

mock is designed for "unit test style" testing, but is used with Python
testing libraries like nose and py.test.

There will be some emphasis on how *not* to use mocking in testing, and why
'over mocking' is bad (and makes for brittle tests).


---
description_short: 
---
tags: ['mock', 'pycon', 'pycon2011', 'testing']
---
duration: None
---
language: English
---
source_url: 
---
copyright: Creative Commons Attribution-NonCommercial-ShareAlike 3.0
---
slug: pycon-2011--testing-with-mock
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://a.images.blip.tv/Pycon-PyCon2011TestingWithMock498.png
---
embed: 
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: http://05d2db1380b6504cc981-8cbed8cf7e3a131cd8f1c3e383d10041.r93.cf2.rackcdn.com/pycon-us-2011/392_testing-with-mock.mp4
---
video_ogv_download_only: False
---
video_ogv_length: 171633013
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 