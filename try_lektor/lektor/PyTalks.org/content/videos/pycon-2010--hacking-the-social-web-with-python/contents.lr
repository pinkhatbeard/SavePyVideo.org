_model: video
---
title: Hacking the Social Web with Python
---
id: 286
---
date_created: 2012-02-23T04:20:00
---
date_updated: 2014-04-08T20:28:28.118
---
date_recorded: 2010-02-19
---
event: PyCon US 2010
---
speakers: ['David Recordon', 'Mike Malone']
---
description_long: Open Stack: Hacking the Social Web

Presented by David Recordon/Mike Malone

In this tutorial we'll explain what the Open Stack is and how you can use
tools like OAuth, OpenID, and Portable Contacts to integrate your app with the
rest of the web. We'll cover each component in depth, and walk through example
Python implementations for a variety of scenarios.

Intended Audience

  * Beginning to intermediate Python programmers interested in Web Development 
  * Python programmers who provide or consumer web service APIs 

Class Outline

  * Intro: what is the "Open Stack" 
  * Working with HTTP in Python 
  * Working with responses: JSON, XML, Atom, RSS, HTML 
  * Decentralized identity with OpenID 
    * Implementing a Provider in Python 
    * Implementing a Relying Party in Python 
  * Delegated authorization with OAuth 
    * Implementing a Provider in Python 
    * Implementing a Consumer in Python 
  * Exchanging contacts with Portable Contacts 
  * Aggregating activity with Activity Streams 
  * Discovering how to communicate: LRDD + XRD 
  * Making markup structured: microformats 
  * The future of the Open Stack 
  * Learn more & get involved: resources and communities 
  * Q&A;

Requirements

Some experience doing web development or working with web service APIs


---
description_short: 
---
tags: ['atom', 'http', 'json', 'oauth', 'openid', 'pycon', 'pycon2010', 'rss', 'tutorial', 'xml']
---
duration: None
---
language: English
---
source_url: 
---
copyright: Creative Commons Attribution-NonCommercial-ShareAlike 3.0
---
slug: pycon-2010--hacking-the-social-web-with-python
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://a.images.blip.tv/Pycon-PyCon2010HackingTheSocialWebWithPython694-349.jpg
---
embed: 
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: http://05d2db1380b6504cc981-8cbed8cf7e3a131cd8f1c3e383d10041.r93.cf2.rackcdn.com/pycon-us-2010/286_hacking-the-social-web-with-python.m4v
---
video_ogv_download_only: False
---
video_ogv_length: 923087859
---
video_ogv_url: http://05d2db1380b6504cc981-8cbed8cf7e3a131cd8f1c3e383d10041.r93.cf2.rackcdn.com/pycon-us-2010/286_hacking-the-social-web-with-python.ogv
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 