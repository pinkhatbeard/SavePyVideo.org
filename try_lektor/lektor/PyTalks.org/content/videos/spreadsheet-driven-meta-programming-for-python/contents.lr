_model: video
---
title: Spreadsheet-Driven Meta-Programming for Python
---
id: 3959
---
date_created: 2015-10-18T16:10:53.291
---
date_updated: 2015-10-18T16:10:53.291
---
date_recorded: 2015-08-04
---
event: PyCon AU 2015
---
speakers: ['Javier Candeira']
---
description_long: We've all had those very detail-oriented clients who are able to specify their needs very neatly, often by means of exhaustive spreadsheets. Wouldn't it be nice if we could turn these spreadsheets straight into Python code?

In this tutorial we'll use two sets of meta-programming techniques to derive Python code out of semi-formal descriptions: first, to build Django models requiring very detailed validation supplied by the client in a spreadsheet; second, to build a Python API for a hardware device, the description of which is maintained by the engineers in a spreadsheet.

For the Pycon attendance, this tutorial has both business value and technical value:

Business value because spreadsheet-driven metaprogramming allows developers to:

- respond to changes very fast by re-rendering their Python code from the modified spreadsheet descriptions.
- avoid a certain class of human errors unavoidable when transcribing the requirements by hand.
- save time which can be employed in other areas of the project, or with other clients.
- clarify ambiguous specifications by making explicit all programmer's assumptions into the code generator.

Technical value because we'll be talking about:

- api design,
- Django modeling and validation,
- consuming spreadsheets via csv dictreaders and elbow grease,
- writing partially-applied functions via custom classes with a __call__ method,
- implementing fluent apis with table-driven __getattr__ methods,
- implementing iterators with __iter__ and __next__,
- papering over some differences between Python 2 and Python 3 syntax,
- and much, much more.

We'll be doing all of it in a hands-on manner, without getting distracted by abstract discussions of Python syntax. There will be some discussions of Python syntax, but they won't be abstract. We'll always have a goal in sight.

Level:

Intermediate. You'll learn some advanced techniques, but all you need to really know in order to follow this tutorial is how lists, dictionaries, iteration and reading files work in Python. 

Requirements:

- Attendees are expected to know basic Python (see above, under "Level").
- A laptop and some knowledge of git will be useful for those wanting to follow along, but is not required. You can get a lot out of this tutorial just by watching and paying attention.



---
description_short: 
---
tags: []
---
duration: None
---
language: English
---
source_url: https://www.youtube.com/watch?v=DmLHqOvbG9M
---
copyright: creativeCommon
---
slug: spreadsheet-driven-meta-programming-for-python
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: https://i.ytimg.com/vi/DmLHqOvbG9M/hqdefault.jpg
---
embed: <iframe width="640" height="360" src="//www.youtube.com/embed/DmLHqOvbG9M" frameborder="0" allowfullscreen></iframe>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: needs editing