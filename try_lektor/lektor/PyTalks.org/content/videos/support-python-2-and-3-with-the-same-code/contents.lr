_model: video
---
title: Support Python 2 and 3 with the same code
---
id: 2974
---
date_created: 2014-07-31T00:46:27.832
---
date_updated: 2014-07-31T23:05:34.218
---
date_recorded: 2014-07-24
---
event: EuroPython 2014
---
speakers: ['Stefan Schwarzer']
---
description_long: Your library supports only Python 2, - but your users keep nagging you about Python 3 support?

As Python 3 gets adopted more and more, users ask for Python 3 support in existing libraries for Python 2. This talk mentions some approaches for giving users a Python 3 version, but will quickly focus on using the very same code for a Python 2 and a Python 3 version.

This is much easier if you require Python 2.6 and up, and yet a bit easier if you require Python 3.3 as the minimum Python 3 version.

The talk discusses main problems when supporting Python 3 (some are easily solved):

* `print` is a function.

* More Python APIs return iterators that used to return lists.

* There's now a clear distinction between bytes and unicode (text) strings.

* Files are opened as text by default, requiring an encoding to apply on reading and writing.


The talk also explains some best practices:

* Start with a good automatic test coverage.

* Deal with many automatic conversions with a one-time 2to3 run.

* Think about how your library should handle bytes and unicode strings. (Rule of thumb: Decode bytes as early as possible; encode unicode text as late as possible.)

* Should you break compatibility with your existing Python 2 API? (Yes, if there's no other way to design a sane API for Python 2 and 3. If you do it, raise the first part of the version number.)

* Try to keep code that's different for Python 2 and 3 minimal. Put code that needs to be different for Python 2 and 3 into a `compat` module. Or use third-party libraries like `six` or `future`.


Finally, the talk will mention some helpful resources on the web.
---
description_short: Your library supports only Python 2, - but your users keep nagging you about Python 3 support?

As Python 3 gets adopted more and more, users ask for Python 3 support in existing libraries for Python 2. Although there are several approaches, this talk focuses on using the very same code for a Python 2 and a Python 3 version.

The talk discusses the main problems when supporting Python 3 and best practices to apply for compatibility with Python 2 and 3.
---
tags: []
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=elAV6aZDMvg
---
copyright: http://creativecommons.org/licenses/by/3.0/
---
slug: support-python-2-and-3-with-the-same-code
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i.ytimg.com/vi/elAV6aZDMvg/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/elAV6aZDMvg?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/elAV6aZDMvg?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: 
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: 
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 