_model: video
---
title: The political implications of having fun (while programming open source)
---
id: 1092
---
date_created: 2012-08-30T21:02:28
---
date_updated: 2014-04-08T20:28:27.903
---
date_recorded: 2011-07-13
---
event: EuroPython 2011
---
speakers: ['Benno Luthiger']
---
description_long: What are the implications for the society if people enjoy their work, because
this work is perceived as rewarding by itself? It is this question that
occupies me since I have finished my Ph.D. research about the motivation of
open source programmers. In my research, I have been able to show that fun is
an important driver for open source programmers. Moreover, fun plays an
important role not only for hobbyists, but for developers who are paid for
their work too (open or closed source).

These findings contrast with the traditional efforts of leftist parties and
labor unions. For those political organizations, labor is perceived as
compulsion and exploitation and, therefore, people have to be liberated from
labor. The resulting political praxis is to send people into retirement as
soon as possible and, on the other hand, to make the life for unemployed
people carefree.

Is the software developers‘ world a little Shangri-La, apart from the real
world, delineated by the leftist parties, or is the open source model
generalizable? What happens with the values generated through the labor if the
work as such is rewarding? The open source mode of production is part of our
modern reality and in my presentation I would like to consider this big
reality from the perspective of open source.


---
description_short: [EuroPython 2011] Benno Luthiger - 23 June 2011 in "Track Spaghetti"


---
tags: []
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=B5V7fWST3H0
---
copyright: Standard YouTube License
---
slug: the-political-implications-of-having-fun-while-0
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i.ytimg.com/vi/B5V7fWST3H0/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/B5V7fWST3H0?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/B5V7fWST3H0?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 