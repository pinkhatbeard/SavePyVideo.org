_model: video
---
title: DevOps Risk Mitigation: Test Driven Infrastructure
---
id: 3000
---
date_created: 2014-07-31T00:46:30.966
---
date_updated: 2014-07-31T23:20:48.497
---
date_recorded: 2014-07-23
---
event: EuroPython 2014
---
speakers: ['Schlomo Schapiro']
---
description_long: Common wisdom has it that the test effort should be related to the risk of a change. However, the reality is different: Developers build elaborate automated test chains to test every single commit of their application. Admins regularly “test” changes on the live platform in production. But which change carries a higher risk of taking the live platform down?

What about the software that runs at the “lower levels” of your platform, e.g. systems automation, provisioning, proxy configuration, mail server configuration, database systems etc. An outage of any of those systems can have a financial impact that is as severe as a bug in the “main” software!
One of the biggest learnings that any Ops person can learn from a Dev person is Test Driven Development. Easy to say - difficult to apply is my personal experience with the TDD challenge.

This talk throws some light on recent developments at ImmobilienScout24 that help us to develop the core of our infrastructure services with a test driven approach:

* How to do unit tests, integration tests and systems tests for infrastructure services?
* How to automatically verify Proxy, DNS, Postfix configurations before deploying them on live servers?
* How to test “dangerous” services like our PXE boot environment or the automated SAN mounting scripts?
* How to add a little bit of test coverage to everything we do.
* Test Driven: First write a failing test and then the code that fixes it.

The tools that we use are Bash, Python, Unit Test frameworks and Teamcity for build and test automation.

See http://blog.schlomo.schapiro.org/2013/12/test-driven-infrastructure.html for more about this topic.

---
description_short: The (perceived) risk of the DevOps is that too many people get the right to "break" the platform.

Test Driven Infrastructure is about adapting proven ideas from our developer colleagues to the development and operations of Infrastructure services like virtualization, OS provisioning, postfix configuration, httpd configuration, ssh tuning, SAN LUN mounting and others.

This talk shows how ImmobilienScout24 utilizes more and more test driven development in IT operations to increase quality and to mitigate the risk of opening up the infrastructure developmen to all developers.
---
tags: []
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=L6TtXrLmdKA
---
copyright: http://creativecommons.org/licenses/by/3.0/
---
slug: devops-risk-mitigation-test-driven-infrastructur
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i.ytimg.com/vi/L6TtXrLmdKA/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/L6TtXrLmdKA?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/L6TtXrLmdKA?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: 
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: 
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 