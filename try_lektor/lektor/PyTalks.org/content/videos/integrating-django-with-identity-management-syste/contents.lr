_model: video
---
title: Integrating Django with Identity Management Systems
---
id: 4042
---
date_created: 2015-11-23T19:30:05.756
---
date_updated: 2015-11-23T19:30:05.756
---
date_recorded: 2015-08-04
---
event: PyCon AU 2015
---
speakers: ['Fraser Tweedale']
---
description_long: Most Django developers are familiar with authentication and
authorisation on the open web, but the requirements and technologies
used inside companies and large organisations are different:

- Identities and groups are probably stored in an external identity
  management system's directory rather than in an application's
  database tables.

- Authorisation decisions will be based on group membership and
  policies that are defined outside the application.

- Users may be expected or required to use a *single sign-on*
  technology such as Kerberos or SAML to authenticate to applications.

This talk will familiarise the audience with these technologies
and demonstrate how Django applications can be integrated
with an identity management system to meet business
requirements while providing a positive user experience.  Particular
technologies covered will include:

- FreeIPA: an open-source identity management solution, for defining
  users, groups and authorisation policies

- mod_auth_gssapi / mod_auth_kerb: Apache modules for Kerberos
  authentication

- mod_lookup_identity: Apache module to retrieve user information
  from a directory

The talk will conclude with discussion about upcoming Kerberos
features, techniques for dealing with multiple authentication
methods, and progress in making identity management integration
easier for Django developers.

People developing or deploying Django applications in business
environments or for large open source projects with centralised
identity management will get the most out of this talk.
---
description_short: 
---
tags: []
---
duration: None
---
language: English
---
source_url: https://www.youtube.com/watch?v=HhcotmeioT8
---
copyright: creativeCommon
---
slug: integrating-django-with-identity-management-syste
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: https://i.ytimg.com/vi/HhcotmeioT8/hqdefault.jpg
---
embed: <iframe width="640" height="360" src="//www.youtube.com/embed/HhcotmeioT8" frameborder="0" allowfullscreen></iframe>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: needs editing