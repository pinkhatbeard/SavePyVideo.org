_model: video
---
title: Processing biggish data on commodity hardware: simple Python patterns; SciPy 2013 Presentation
---
id: 2121
---
date_created: 2013-07-04T10:08:57
---
date_updated: 2014-04-08T20:28:26.425
---
date_recorded: 2013-07-01
---
event: SciPy 2013
---
speakers: []
---
description_long: 
---
description_short: Authors: Author: Gael Varoquaux Institution: INRIA, Parietal team

Track: Machine Learning

While big data spans many terabytes and requires distributed computing, most mere mortals deal with gigabytes. In this talk I will discuss our experience in applying efficiently machine learning to hundreds of gigabytes on commodity hardware. In particular, I will discuss patterns implemented in two Python libraries, joblib and scikit-learn, dissecting why they help addressing big data and how to implement them efficiently with simple tools.

In particular, I will cover:

On the fly data reduction
On-line algorithms and out-of-core computing
Parallel computing patterns: performance outside of a framework
Caching of common operations, with efficient hashing of arbitrary
Python objects and a robust datastore relying on Posix disk semantics
The talk will illustrate the high-level concepts introduced with detailed technical discussions on Python implementations, based both on examples using scikit-learn and joblib and on an analysis of how these libraries work. The goal here is less to sell the libraries themselves than to share the insights gained in using and developing them.
---
tags: ['Tech']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=-W-UZTDZ2r0
---
copyright: http://www.youtube.com/t/terms
---
slug: processing-biggish-data-on-commodity-hardware-si-1
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/-W-UZTDZ2r0/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/-W-UZTDZ2r0?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/-W-UZTDZ2r0?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: needs editing