_model: video
---
title: Fang: Pythonic dependency injection
---
id: 4004
---
date_created: 2015-10-18T16:10:59.443
---
date_updated: 2015-10-18T16:10:59.444
---
date_recorded: 2015-08-04
---
event: PyCon AU 2015
---
speakers: ['Nathan Craike']
---
description_long: Dependency injection (DI) is a technique most often used in "big OO" languages like Java and C#. It's usually dismissed by Python developers as only needed because of these other languages' deficiencies: static typing, restrictive object systems, lack of reflection, etc.

However, there are other dynamically-typed languages which do use forms of dependency injection. From looking at these systems, we can see there are some advantages to dependency injection _even if_ you're not using a traditional "big OO" language with static types and interfaces. Dependency-injected code can be more modular, much easier to test, easier to analyse, and easier to modify or extend to meet new requirements.

Despite this, there aren't many dependency-injection systems for Python. The few that do exist aren't "Pythonic", or implement DI in ways that compromise a lot of its benefits.

Fang is a library which adds dependency injection to Python in a way which provides real benefits for code clarity, ease of testing, and maintainability, while still fitting naturally into Python's idioms. Fang describes dependencies concisely but explicitly, and doesn't restrict the developer to a particular paradigm on how dependencies are validated or met.

This talk will give a brief overview of how DI is used across a few languages (both traditional OO and dynamic languages) and highlight what elements of DI can be beneficial in Python. The talk will then demo how Fang can be used to add DI to Python code, and show some of the benefits to code clarity, unit testing and code analysis.


---
description_short: 
---
tags: []
---
duration: None
---
language: English
---
source_url: https://www.youtube.com/watch?v=zqRd941NXlI
---
copyright: creativeCommon
---
slug: fang-pythonic-dependency-injection
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: https://i.ytimg.com/vi/zqRd941NXlI/hqdefault.jpg
---
embed: <iframe width="640" height="360" src="//www.youtube.com/embed/zqRd941NXlI" frameborder="0" allowfullscreen></iframe>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: needs editing