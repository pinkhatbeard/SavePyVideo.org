_model: video
---
title: Anatomy of Matplotlib - Part 2
---
id: 2756
---
date_created: 2014-07-10T22:35:23.090
---
date_updated: 2014-07-10T22:35:23.090
---
date_recorded: 2014-07-09
---
event: SciPy 2014
---
speakers: ['Benjamin Root']
---
description_long: Introduction
============

Purpose of matplotlib
---------------------

Online Documentation
--------------------
### [matplotlib.org](http://matplotlib.org "")
### Mailing Lists and StackOverflow
### Github Repository
### Bug Reports & Feature Requests

What is this "backend" thing I keep hearing about?
==================================================
### Interactive versus non-interactive
### Agg
### Tk, Qt, GTK, MacOSX, Wx, Cairo

Plotting Functions
==================
### Graphs (plot, scatter, bar, stem, etc.)
### Images (imshow, pcolor, pcolormesh, contour[f], etc.)
### Lesser Knowns: (pie, acorr, hexbin, streamplot, etc.)

What goes in a Figure?
======================
### Axes
### Axis
### ticks (and ticklines and ticklabels) (both major & minor)
### axis labels
### axes title
### figure suptitle
### axis spines
### colorbars (and the oddities thereof)
### axis scale
### axis gridlines
### legend

Manipulating the "Look-and-Feel"
================================

### Introducing matplotlibrc
### Properties
- color (and edgecolor, linecolor, facecolor, etc...)
- linewidth and edgewidth and markeredgewidth (and the oddity that happens in errorbar())
- linestyle
- fonts
- zorder
- visible

What are toolkits?
==================
### axes_grid1
### mplot3d
### basemap

---
description_short: This tutorial will be the introduction to matplotlib. Users will learn the types of plots and experiment with them. Then the fundamental concepts and terminologies of matplotlib are introduced. Next, we will learn how to change the "look and feel" of their plots. Finally, users will be introduced to other toolkits that extends matplotlib.
---
tags: ['matplotlib']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=YXdaLmzYEKU
---
copyright: http://www.youtube.com/t/terms
---
slug: anatomy-of-matplotlib-part-2
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/YXdaLmzYEKU/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/YXdaLmzYEKU?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/YXdaLmzYEKU?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 