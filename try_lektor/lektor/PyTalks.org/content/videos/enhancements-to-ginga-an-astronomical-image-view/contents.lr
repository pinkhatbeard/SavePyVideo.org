_model: video
---
title: Enhancements to Ginga: an Astronomical Image Viewer and Toolkit
---
id: 2777
---
date_created: 2014-07-15T22:45:37.832
---
date_updated: 2014-07-15T22:59:53.756
---
date_recorded: 2014-07-14
---
event: SciPy 2014
---
speakers: ['Eric Jeschke']
---
description_long: Ginga is an open-source astronomical image viewer and toolkit written in python and [hosted on Github](https://github.com/ejeschke/ginga).  It uses and inter-operates with several key scientific python packages: numpy, scipy, astropy and matplotlib.

In this talk/poster we describe and illustrate recent enhancements to the package since the introductory talk at SciPy 2013, including:


* modular/pluggable interfaces for world coordinate systems, image file I/O and star and image catalogs
* support for rendering into matplotlib figures
* support for image mosaicing
* support for image overlays
* customizable user-interface bindings
* improved documentation
* self contained Mac OS X packages

During the talk we will demonstrate the mosaicing plugin that is being used with several instruments at Subaru Telescope in Hawaii, including the new Hyper Suprime-Cam wide-field camera with 116 separate 4Kx2K CCDs.

The talk/poster may be of interest to anyone developing code in python needing to display scientific image (CCD or CMOS) data and astronomers interested in python-based quick look and analysis tools.

---
description_short: We describe recent developments in the Ginga package, a open-source astronomical image viewer and toolkit written in python and hosted on Github.  The package was introduced to the scientific python community at SciPy 2013 and has received a number of enhancements since then based on user feedback.  The talk includes an image mosaicing demo of a wide-field camera exposure with 116 4Kx2K CCDs.
---
tags: ['astronomy']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=wa_0SuVckGQ
---
copyright: http://www.youtube.com/t/terms
---
slug: enhancements-to-ginga-an-astronomical-image-view
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/wa_0SuVckGQ/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/wa_0SuVckGQ?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/wa_0SuVckGQ?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: 
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: 
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 