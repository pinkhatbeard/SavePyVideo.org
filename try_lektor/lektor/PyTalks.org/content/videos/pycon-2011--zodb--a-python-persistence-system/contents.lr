_model: video
---
title: ZODB: A Python Persistence System
---
id: 430
---
date_created: 2012-02-23T04:20:00
---
date_updated: 2014-04-08T20:28:28.067
---
date_recorded: 2011-03-11
---
event: PyCon US 2011
---
speakers: ['Chris McDonough']
---
description_long: ZODB: A Python Persistence System

Presented by Chris McDonough

[ZODB](http://zodb.org/) is a transactional persistence system written
entirely in Python. This talk will serve as an introduction to using the ZODB
in a Python application.

Abstract

This talk will provide a high-level overview of ZODB useful to a novice or
intermediate Python programmer. The talk will cover the following topics:

  * What Is ZODB? 
  * Brief history 
  * ZODB vs. relational databases 
  * ZODB vs. NoSQL databases 
  * ZODB vs. pickle 
  * Using ZODB 
  * Creating a Persistent Object 
  * Storing a Persistent Object 
  * Retrieving a Persistent Object 
  * Modifying a Persistent Object 
  * Saving Changes 
  * Folders 
  * Aspects 
  * Pluggable storages 
  * Scaling across multiple clients 
  * Caching 
  * Indexing and Searching 
  * repoze.catalog 

At the end of the talk, an attendee should have a basic understanding of how
to create an application which depends on ZODB persistence.


---
description_short: 
---
tags: ['pycon', 'pycon2011', 'zodb']
---
duration: None
---
language: English
---
source_url: 
---
copyright: Creative Commons Attribution-NonCommercial-ShareAlike 3.0
---
slug: pycon-2011--zodb--a-python-persistence-system
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://a.images.blip.tv/Pycon-PyCon2011ZODBAPythonPersistenceSystem382.png
---
embed: 
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: http://05d2db1380b6504cc981-8cbed8cf7e3a131cd8f1c3e383d10041.r93.cf2.rackcdn.com/pycon-us-2011/430_zodb-a-python-persistence-system.mp4
---
video_ogv_download_only: False
---
video_ogv_length: 175860855
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 