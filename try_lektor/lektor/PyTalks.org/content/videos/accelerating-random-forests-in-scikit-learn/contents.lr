_model: video
---
title: Accelerating Random Forests in Scikit Learn
---
id: 3334
---
date_created: 2014-11-05T22:41:04.483
---
date_updated: 2014-11-05T22:42:28.662
---
date_recorded: 2014-10-22
---
event: EuroScipy 2014
---
speakers: ['Gilles Louppe']
---
description_long: Random Forests are without contest one of the most robust, accurate and versatile tools for solving machine learning tasks. Implementing this algorithm properly and efficiently remains however a challenging task involving issues that are easily overlooked if not considered with care. In this talk, we present the Random Forests implementation developed within the Scikit-Learn machine learning library. In particular, we describe the iterative team efforts that led us to gradually improve our codebase and eventually make Scikit-Learn's Random Forests one of the most efficient implementations in the scientific ecosystem, across all libraries and programming languages. Algorithmic and technical optimizations that have made this possible include:

* An efficient formulation of the decision tree algorithm, tailored for Random Forests;
* Cythonization of the tree induction algorithm;
* CPU cache optimizations, through low-level organization of data into contiguous memory blocks;
* Efficient multi-threading through GIL-free routines;
* A dedicated sorting procedure, taking into account the properties of data;
* Shared pre-computations whenever critical.

Overall, we believe that lessons learned from this case study extend to a broad range of scientific applications and may be of interest to anybody doing data analysis in Python.
---
description_short: 
---
tags: []
---
duration: None
---
language: English
---
source_url: https://www.youtube.com/watch?v=TqF0rKzvjm4
---
copyright: youtube
---
slug: accelerating-random-forests-in-scikit-learn
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: https://i.ytimg.com/vi/TqF0rKzvjm4/hqdefault.jpg
---
embed: <iframe type='text/html' src='http://www.youtube.com/embed/TqF0rKzvjm4' width='640' height='360' frameborder='0' allowfullscreen='true'/>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: 
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: 
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 