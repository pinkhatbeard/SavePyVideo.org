_model: video
---
title: Sqlkit: empowering database access
---
id: 1014
---
date_created: 2012-08-30T21:02:08
---
date_updated: 2014-04-08T20:28:27.723
---
date_recorded: 2011-07-24
---
event: EuroPython 2011
---
speakers: ['Alessandro Dentella']
---
description_long: It's very easy to access databases with Python, and there are many ORMs
allowing a high level of abstraction. Still, it's a long road from there to
interactive handling of data, even for very simple tasks.

Sqlkit tries to act as a bridge, offering some mega-widgets that allow you to
build GUI applications or directly access data using the 'sqledit'
application, shipped with sqlkit; sqlkit is built with PyGTK and SQLAlchemy.

This presentation covers some of the most powerful features of sqlkit, and
shows how easy it is to build small programs for personal use, as well as rich
and complex applications. It primarily focuses on two points: how easy it is
to create data forms (eventually with one2many or many2many, thanks to a
original definition of layouts), and the template system based on OpenOffice
which allows easy report creation.

This presentation shows how sqlkit can be used both as application development
framework and as a tool to help us while developing other applications (e.g.:
Django, OpenERP,… ).

To deliver a gradual presentation, we'll use the configuration system of
sqledit, starting from a minimal configuration (a single URL) we'll add
elements to that until we'll have a full application.


---
description_short: [EuroPython 2011] Alessandro Dentella - 23 June 2011 in "Track Spaghetti"


---
tags: ['database', 'forms', 'framework', 'gui', 'openoffice', 'python,']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=UwV2Hdb8XQQ
---
copyright: Standard YouTube License
---
slug: sqlkit-empowering-database-access
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i.ytimg.com/vi/UwV2Hdb8XQQ/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/UwV2Hdb8XQQ?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/UwV2Hdb8XQQ?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 