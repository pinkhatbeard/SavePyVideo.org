_model: video
---
title: Integrating Python and C++ with Boost Python part 3
---
id: 2750
---
date_created: 2014-07-10T22:35:19.418
---
date_updated: 2014-07-10T22:35:19.418
---
date_recorded: 2014-07-09
---
event: SciPy 2014
---
speakers: ['Austin Bingham']
---
description_long: Python and C++ are both popular languages that each bring a lot to the table. The languages also complement one another well: Python is high-level, dynamic, and easy to use while C++ is at-the-metal, static, and (in)famously tricky. There are times when there are real advantages to combining these disparate natures, and Python’s C API provides a strong interface for doing just that. Boost.Python is a C++ library that builds upon and improves Python’s C API to give users a simpler, more intuitive, and safer means to integrate Python and C++.

In this tutorial we’ll look at how to use Boost.Python to effectively bridge the Python/C++ boundary. We’ll start by briefly looking at the fundamentals of the Python C API since that defines the “ground rules”; this includes things like reference counting, the basic object model, and so forth. We’ll then quickly look at the Boost.Python API and show how it provides the same functionality as the underlying C API, but does so in a way that doesn’t obscure the real semantics of the Python language.

After this introduction, the rest of the tutorial will involve writing code to explore various elements of Boost.Python. We’ll focus on techniques for extending Python with C++, that is, writing Python modules in C++. Boost.Python can be used for embedding (i.e. invoking Python code from C++), but that involves a different set of techniques, and in practice most scientific Python developers are more interested in developing extensions.

The syllabus for the four-hour tutorial will be like this:

1. Introduction: C-API and Boost.Python basics

    Note that this can be reduced or eliminated of participants are already comfortable with the topics.

2. Hello World: Exposing a basic function

    In this section we’ll get a minimal Boost.Python module working. This will not only introduce students to the infrastructure of Boost.Python, but it will also give us a chance to make sure that everyone’s build environment is working.

3. Exposing functions

    In this section we’ll look at the details of exposing C++ functions to Python. The topics we’ll cover will include overloading (including Boost.Python’s auto-overload feature), default argument values, and a brief look at call policies.

4. Exposing classes

    Here we’ll look at how to expose C++ classes to Python. Topics will include the basic `class_<T>` template, member functions, data members, properties, inheritance, and virtual functions.

5. `boost::python::object`

    The `boost::python::object` class is Boost.Python’s primary interface to Python’s `PyObject` structure. Understanding how to work with this class is a key building-block for developing Python modules with Boost.Python. We’ll explore its API and features, including areas like attribute access, reference counting, and converting between Python and C++ objects.

6. Derived object types

    Boost.Python provides a number of `boost::python::object` subclasses for important Python classes like `list`, `dict`, and `tuple`. In this section we’ll look at these subclasses and how to use them in Boost.Python modules.

7. Enums

    Boost.Python provides `enum_<T>` for exposing C++ enums to Python. Python doesn’t have a notion of enums *per se*, but in this section we’ll explore how this template makes it straightforward to use C++ enums in Python in a simple and intuitive way.

8. Type conversion

    In this section we’ll look at Boost.Python’s support for doing automatic type-conversion across the Python/C++ boundary. We’ll see how you can register type-converters with Boost.Python which will be invoked whenever Boost.Python needs to convert a Python object to a C++ object or vice versa.

This is a fairly ambitious set of topics, and it’s possible that we won’t be able to cover them all. The topics are roughly in most-often-used to least-often-used order, however, so students will be sure to be exposed to the most important and relevant elements of the course.

Likewise, the four-hour format of the course means that we won’t be able to go into great depth on many topics. The main goal of the course, then, is to give students enough orientation and hands-on experience with Boost.Python that they can continue to learn on their own. Inter-language integration - especially between languages as dissimilar as C++ and Python - can be quite complex, but this tutorial will give students the grounding they need to successfully apply Boost.Python to their problems.  

---
description_short: Python and C++ can be powerful complements to one another. C++ is great for performance-critical calculations, while Python is great for everything else. In this tutorial we’ll look at how to integrate Python and C++ using the Boost.Python library. You’ll learn techniques for easily developing hybrid systems that use the right language for the right task, resulting in better software.

---
tags: ['boost', 'c++']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=kR5tREIotlI
---
copyright: http://www.youtube.com/t/terms
---
slug: integrating-python-and-c-with-boost-python-part-1
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/kR5tREIotlI/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/kR5tREIotlI?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/kR5tREIotlI?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 