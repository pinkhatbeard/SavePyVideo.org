_model: video
---
title: IPython Protocol, Kernals and new features
---
id: 3338
---
date_created: 2014-11-05T22:41:05.087
---
date_updated: 2014-11-05T22:41:05.087
---
date_recorded: 2014-10-22
---
event: EuroScipy 2014
---
speakers: ['Matthias Bussionnier', 'Thomas Kluyver']
---
description_long: A key idea behind IPython is decoupling code execution from user interfaces. IPython relies on a documented protocol, which can be implemented by different frontends and different kernels. By implementing it, frontends and kernels gain the ability to communicate regardless of which language they're written in. The IPython project maintains three different frontends, while there are multiple third party frontends and kernels already in use.

We will show some important features that such a protocol permits, by demonstrating some of our alternative frontends, as well as kernels that people have written in languages such as Julia and R. Our presentation will also feature interactive widgets, a new feature in IPython 2.0, and preview the upcoming features that will allow a single notebook server to start different types of kernel.

This will demonstrate that the IPython Notebook is the perfect polyglot tool for scientific computation workflows.
---
description_short: 
---
tags: []
---
duration: None
---
language: English
---
source_url: https://www.youtube.com/watch?v=brgMEWT1pYc
---
copyright: youtube
---
slug: ipython-protocol-kernals-and-new-features
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: https://i.ytimg.com/vi/brgMEWT1pYc/hqdefault.jpg
---
embed: <iframe type='text/html' src='http://www.youtube.com/embed/brgMEWT1pYc' width='640' height='360' frameborder='0' allowfullscreen='true'/>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: 
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: 
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 