_model: video
---
title: Software Carpentry arrives Down Under!
---
id: 2230
---
date_created: 2013-07-13T01:04:20
---
date_updated: 2014-04-08T20:28:26.291
---
date_recorded: 2013-07-06
---
event: PyCon AU 2013
---
speakers: ['Damien Irving']
---
description_long: 
---
description_short: Scientists are required to do more and more programming these days,  however they are almost always self-taught. They spend a large percentage of their time wrestling with software, reinvent a lot of wheels, and still don’t know if their results are reliable. For proficient software developers, particularly those employed to assist these scientists, this computational illiteracy is easy to identify, but much harder to remedy. What do you teach a scientist, given their time constraints, background knowledge and work requirements? This presentation will give an answer to that question, by describing what gets taught at a Software Carpentry boot camp.

Software Carpentry is a volunteer organisation that has been teaching basic software development to scientists for over a decade. Supported by the Sloan Foundation and Mozilla, Software Carpentry has refined its teaching methods over time and has recently settled on a model centred around the delivery of intensive short courses known as “boot camps”. Institutions such as Oxford University, MIT, University of California Berkeley and the International Centre for Theoretical Physics have all hosted boot camps in the past 12 months.     

Dr. Greg Wilson, the founder of Software Carpentry, flew out to Australia in February 2013 to host a series of two day boot camps with the Genes to Geoscience Research Centre in Sydney and the Australian Meteorological and Oceanographic Society (AMOS) and ARC Centre for Excellence in Climate System Science in Melbourne. These two events were the first ever boot camps held outside of North America and Europe, were booked out in a matter of days, and received rave reviews from participants. Given this success, there are already plans afoot to host a pair of boot camps for the bioinformatics community in Brisbane and Adelaide later this year, plus another for the weather/climate community in Hobart. 

This presentation will describe what gets taught at a Software Carpentry boot camp (hint: there's lots of Python), how it's taught and why it's so effective (hint: we take an evidence-based approach to teaching). All Software Carpentry teaching materials can be freely re-used under open access licenses, so at the conclusion of the talk you can take anything you've learned and apply it to your own work and/or teaching. Better still, you'll find out how to join the Australian Software Carpentry team, so that you can organise and/or teach a boot camp for your own discipline.        
---
tags: []
---
duration: 30
---
language: English
---
source_url: https://www.youtube.com/watch?v=-AGgugxn4RM
---
copyright: CC-BY-SA
---
slug: software-carpentry-arrives-down-under
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/-AGgugxn4RM/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/-AGgugxn4RM?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/-AGgugxn4RM?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: True
---
video_mp4_length: None
---
video_mp4_url: http://s3.us.archive.org/ndvpyconau2013/Software_Carpentry_arrives_Dow.mp4
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 