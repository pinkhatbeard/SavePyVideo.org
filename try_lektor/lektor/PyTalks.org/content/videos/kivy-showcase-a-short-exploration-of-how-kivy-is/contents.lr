_model: video
---
title: Kivy Showcase: a short exploration of how Kivy is changing the world
---
id: 3936
---
date_created: 2015-09-30T02:46:10.644
---
date_updated: 2015-10-04T10:23:42.918
---
date_recorded: 2015-10-02
---
event: PyCon ZA 2015
---
speakers: ['Richard Larkin']
---
description_long: In this talk, we will look at some real-world success stories using Kivy. From exhibition-size interactive displays to race car telemetry systems to robotics, Kivy is being used to deliver many compelling and innovative applications. We'll use these applications to discuss some of the features and abilities of the framework that make it such a good choice for a wide range of uses.

We'll briefly cover some exciting projects using Kivy, namely:

* Project Liatris: a new, open source project using Kivy touch devices to control robots.

* RaceCapture Pro: A race car telemetry system that uses Kivy interactive displays both in-car and in post-session analytics interaction.

* Brain Trainer Plus: A mental dexterity trainer deployed to old age homes and medical treatment facilities that has been shown effective in treating dementia and other mental disorders, as well as bringing a greatly enhanced quality-of-life to the aged and mentally ill.
  
* CAMI Educational products: The rich feature set and effortless animation abilities of Kivy make developing compelling, responsive desktop and mobile interactions easy.
    
* The Icarus touch and Touch live projects: creating radically new and exciting interactive musical instruments.
    
All of these applications are being delivered using Kivy, a free, open source, full stack, multi-touch, cross-platform Python/Cython framework that runs on a watch. I kid you not.
---
description_short: 
---
tags: ['Room 215']
---
duration: 3239
---
language: English
---
source_url: http://youtu.be/kXLQ_7GGMnM
---
copyright: 
---
slug: kivy-showcase-a-short-exploration-of-how-kivy-is
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: https://i.ytimg.com/vi/kXLQ_7GGMnM/hqdefault.jpg
---
embed: 
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 