_model: video
---
title: Dynamics with SymPy Mechanics; SciPy 2013 Presentation
---
id: 2006
---
date_created: 2013-07-04T10:08:37
---
date_updated: 2014-04-08T20:28:26.339
---
date_recorded: 2013-07-02
---
event: SciPy 2013
---
speakers: []
---
description_long: 
---
description_short: Authors: Moore, Jason, University of California at Davis

Track: General

The SymPy Mechanics package was created to automate the derivation of the equations of motion for rigid body dynamics problems. It has been developed primarily through several Google Summer of Code grants over three years and is capable of deriving Newton's Second Law for non-trivial multi-body systems using a variety of methods: from Newton-Euler, to Lagrange, to Kane. The software provides essential classes based around the concepts of a three dimensional vector in a reference frame which ease the setup and bookkeeping of the tedious kinematics including both kinematic and motion constraints. There are also classes for the automated formulation of the equations of motion based on the bodies and forces in a system. It also includes automated linearization of the resulting non-linear models. The software can be used to solve basic physics problems or very complicated many-body and many-constraint systems all with symbolic results. I will go over the basic software design, demonstrate its use through the API along with several classic physics problems and some not-so-trivial three dimensional multi-body problems.
---
tags: ['Tech']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=Jtt9hexk93o
---
copyright: http://www.youtube.com/t/terms
---
slug: dynamics-with-sympy-mechanics-scipy-2013-present
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/Jtt9hexk93o/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/Jtt9hexk93o?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/Jtt9hexk93o?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: needs editing