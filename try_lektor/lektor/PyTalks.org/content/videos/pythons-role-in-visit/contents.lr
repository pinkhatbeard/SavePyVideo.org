_model: video
---
title: Python's role in VisIt
---
id: 1228
---
date_created: 2012-08-31T16:35:42
---
date_updated: 2014-04-08T20:28:27.119
---
date_recorded: 2012-07-19
---
event: SciPy 2012
---
speakers: ['Cyrus Harrison', 'Harinarayan Krishnan']
---
description_long: VisIt is an open source, turnkey application for scientific data analysis and
visualization that runs on a wide variety of platforms from desktops to
petascale class supercomputers. This talk will provide an overview of Python’s
role in VisIt with a focus on use cases of scripted rendering, data analysis,
and custom application development.

Python is the foundation of VisIt’s primary scripting interface, which is
available from both a standard python interpreter and a custom command line
client. The interface provides access to all features available through
VisIt’s GUI. It also includes support for macro recording of GUI actions to
python snippets and full control of windowless batch processing.

While Python has always played an important scripting role in VisIt, two
recent development efforts have greatly expanded VisIt’s python capabilities:

  1. We recently enhanced VisIt by embedding python interpreters into our data flow network pipelines. This provides fine grained access, allowing users to write custom algorithms in python that manipulate mesh data via VTK’s python wrappers and leverage packages such as numpy and scipy. Current support includes the ability to create derived mesh quantities and execute data summarization operations.
  2. We now support custom GUI development using Qt via PySide. This allows users to embed VisIt’s visualization windows into their own python applications. This provides a path to extend VisIt’s existing GUI and for rapid development of streamlined GUIs for specific use cases.

The ultimate goal of this work is to evolve Python into a true peer to our
core C++ plugin infrastructure.

This work performed under the auspices of the U.S. Department of Energy by
Lawrence Livermore National Laboratory under Contract DE-AC52-07NA27344 (LLNL-
ABS-552316).


---
description_short: 
---
tags: ['visualization']
---
duration: None
---
language: English
---
source_url: http://youtube.com/watch?v=0e4TTcswqLg
---
copyright: CC BY-SA
---
slug: pythons-role-in-visit
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/0e4TTcswqLg/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/0e4TTcswqLg?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/0e4TTcswqLg?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: http://s3.us.archive.org/nextdayvideo/enthought/scipy_2012/Pythonu2019s_role_in_VisIt.mp4?Signature=syBSoJjkdBb1x58V850wn21fP04%3D&Expires=1346382780&AWSAccessKeyId=FEWGReWX3QbNk0h3
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 