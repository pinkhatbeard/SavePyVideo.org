_model: video
---
title: Simulating and visualising commuter flow through the London Underground
---
id: 3532
---
date_created: 2015-04-22T17:59:39.457
---
date_updated: 2015-04-22T17:59:39.457
---
date_recorded: 2015-04-14
---
event: PyData Paris 2015
---
speakers: ['Camilla Montonen']
---
description_long: Every year the London Tube transports about 1.3 billion commuters. With multiple intersecting lines, the daily functioning of the London Tube network poses many interesting questions: What effects do severe delays on the Piccadilly Line have on passengers taking the Northern Line? What is the shortest path from station x to station y? How does overcrowding on one station affect its neighbouring stations? These are but a few examples of the many questions one can start to investigate using two Python libraries: graph­tool for manipulating and analysing graphs and bokeh for visualizing and streaming “live” data from simulations. In this talk, I will briefly show how one can transform structures such as the London Underground map into programmatic objects that can be analysed and manipulated using Python. Furthermore, I will show examples of simulations based on the publicly available data from Transport for London and illustrate how simulations can be used to glean insight on the large scale behaviour of a complex system. Last but not least, I hope to illustrate how the bokeh server can be leveraged to create “real­time” visualizations of simulations.
---
description_short: 
---
tags: []
---
duration: None
---
language: English
---
source_url: https://www.youtube.com/watch?v=lSbpt67GBUo
---
copyright: youtube
---
slug: simulating-and-visualising-commuter-flow-through
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: https://i.ytimg.com/vi/lSbpt67GBUo/hqdefault.jpg
---
embed: <iframe type='text/html' src='http://www.youtube.com/embed/lSbpt67GBUo' width='640' height='360' frameborder='0' allowfullscreen='true'/>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: needs editing