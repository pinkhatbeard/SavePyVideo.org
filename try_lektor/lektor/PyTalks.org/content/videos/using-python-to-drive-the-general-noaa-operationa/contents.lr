_model: video
---
title: Using Python to drive the General NOAA Operational Modeling Environment; SciPy 2013 Presentation
---
id: 1987
---
date_created: 2013-07-04T10:08:35
---
date_updated: 2014-04-08T20:28:26.376
---
date_recorded: 2013-07-02
---
event: SciPy 2013
---
speakers: []
---
description_long: 
---
description_short: Authors: Barker, Christopher H. NOAA Emergency Response Division.

Track: Meteorology, Climatology, Atmospheric and Oceanic Science

The General NOAA Operational Modeling Environment (GNOME) is a general purpose modeling tool originally designed for operational oil spill modeling. It was developed by NOAA's Emergency Response Division primarily to provide oil spill transport forecasts to the Federal On Scene Coordinator. In the years since its original development, the model has been extended to support other drifting objects, and has been used for modeling a wide variety cases, including: Marine Debris, larval transport, chemicals in water, etc. It played a key role in the Deepwater Horizon oil spill in 2010, and is being used to forecast the drift of debris from the Japanese Tsunami in 2011. In addition, the model is distributed freely to the general public, and is widely used in education and oil spill response planning.

The first version of the program has proven to be powerful, flexible, and easy to use. However, the program is written in C++, with the computational components and a desktop graphical interface code tightly integrated. As we move forward with development, we require a system that allows a new web-based user interface, easier extension of the model, easier scripting for automation, use of the core algorithms in other models, and easier testing. To achieve these goals, we are re-writing the model as a system of components, tied together with Python. Each component can be written in Python, or any language Python can call (primarily C++), and tested either individually or as part of the system with Python. We have written the new model driver in Python, and are wrapping the existing C++ components using Cython. In this paper, the model architecture is presented, with a discussion of the strengths and pitfalls of the approach.
---
tags: ['Tech']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=3pJlgQRn1jY
---
copyright: http://www.youtube.com/t/terms
---
slug: using-python-to-drive-the-general-noaa-operationa
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/3pJlgQRn1jY/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/3pJlgQRn1jY?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/3pJlgQRn1jY?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: needs editing