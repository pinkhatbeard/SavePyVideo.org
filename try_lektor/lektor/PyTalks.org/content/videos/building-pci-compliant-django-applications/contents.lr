_model: video
---
title: Building PCI compliant Django applications
---
id: 1411
---
date_created: 2012-10-08T17:39:53
---
date_updated: 2014-04-08T20:28:26.972
---
date_recorded: 2012-09-06
---
event: DjangoCon 2012
---
speakers: ['Ken Cochrane']
---
description_long: PCI DSS is a set of twelve different security standards that are required for
any organization that handles credit or debit card transactions. These
standards are created by the Payment Card Industry Security Standards Council
and they require all organizations to validate that they are compliant every
year.

Understanding these rules and how they effect you is sometimes a daunting
task. The goals of this talk will be the following:

  * Explain PCI DSS, and quickly go over the rules that will effect your application.
  * Show how to securely handle credit card transactions in your Django application.
  * Storing credit card information isn't ideal, but if you need to, I'll explain the different ways to securely store the data.
  * Go over the different ways to limit your PCI DSS liability (BrianTree, Akamai Edge Tokenization, Auth.net CIM)
  * Explore the different tools you will need in order validate your PCI compliance (Web application scans, IDS, Network Scan, firewalls)
  * How to be PCI Complaint in the cloud

I'll close out the talk with some of the details on how I haver personally
satisfied PCI DSS Requirements on my projects in the past. I'll cover some of
the tools and services that I used, and why I decided to use them.

At the conclusion of the talk you should have a better understanding of PCI
DSS, and what you need to do, in order for your Django application to be
certified as compliant.


---
description_short: If you currently accept credit cards with your Django application today, or
you plan on accepting them in the future, then you will need to worry about
PCI DSS. Learn what you need to do to make sure that your application is PCI
DSS compliant, and if it is not, what you need to do to bring it into
compliance.


---
tags: ['django', 'pci-complicance']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=9ZIPNWqjIEI
---
copyright: Creative Commons Attribution license (reuse allowed
---
slug: building-pci-compliant-django-applications
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i.ytimg.com/vi/9ZIPNWqjIEI/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/9ZIPNWqjIEI?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/9ZIPNWqjIEI?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: needs editing