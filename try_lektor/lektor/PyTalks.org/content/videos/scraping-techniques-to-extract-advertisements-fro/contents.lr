_model: video
---
title: Scraping Techniques to Extract Advertisements from Web Pages
---
id: 1016
---
date_created: 2012-08-30T21:02:08
---
date_updated: 2014-04-08T20:28:27.720
---
date_recorded: 2011-07-24
---
event: EuroPython 2011
---
speakers: ['Mirko Urru', 'Stefano Cotta Ramusino']
---
description_long: Online Advertising is an emerging research field, at the intersection of
Information Retrieval, Machine Learning, Optimization, and Microeconomics. Its
main goal is to choose the right ads to present to a user engaged in a given
task, such as Sponsored Search Advertising or Contextual Advertising. The
former puts ads on the page returned from a Web search engine following a
query. The latter puts ads within the content of a generic, third party, Web
page. The ads themselves are selected and served by automated systems based on
the content displayed to the user.

Web scraping is the set of techniques used to automatically get some
information from a website instead of manually copying it. In particular,
we're interested in studying and adopting scraping techniques for: i.
accessing tags as object members ii. finding out tags whose name, contents or
attributes match selection criteria iii. accessing tag attributes by using a
dictionary-like syntax.

In this talk, we focus on the adoption of scraping techniques in the
contextual advertising field. In particular, we present a system aimed at
finding the most relevant ads for a generic web page p. Starting from p, the
system selects a set of its inlinks (i.e., the pages that link p) and extracts
the ads contained into them. Selection is performed querying the Google search
engine, whereas extraction is made by using suitable scraping techniques.


---
description_short: [EuroPython 2011] Mirko Urru,Stefano Cotta Ramusino - 24 June 2011 in "Track
Tagliatelle "


---
tags: ['google', 'scraping', 'search', 'web']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=IL4o0HSGOsU
---
copyright: Standard YouTube License
---
slug: scraping-techniques-to-extract-advertisements-fro
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i.ytimg.com/vi/IL4o0HSGOsU/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/IL4o0HSGOsU?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/IL4o0HSGOsU?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 