_model: video
---
title: 3D sensors and Python: A space odyssey
---
id: 2993
---
date_created: 2014-07-31T00:46:30.151
---
date_updated: 2014-07-31T23:14:17.103
---
date_recorded: 2014-07-23
---
event: EuroPython 2014
---
speakers: ['Celia Cintas']
---
description_long: This talk will start with a brief introduction to 3D Sensors and OpenNI. Then we’ll surf into PyOpenNI,  features such as the skeleton, hand and gesture tracking, RGB and depth video. Every topic will be presented with practical demos. The talk will end with a demo integrating WebGL (THREE.JS), 3D sensors, Flask and ZMQ to produce a simple fully open source based NUI game.

Some simple demos of PyOpenNI and PyGame can be found at [1](http://www.youtube.com/watch?v=wI2ktioiPY8) and [2](http://youtu.be/3e8jibGUQ2Q)

Attendees will not only learn about game related technologies but also about innovative ways of doing domotics, cinema & art, Interactive visualization, scientific research, educations, etc.

3D Sensors will be available for testing during the event - you can get yours for about 80 to 140 Euros (depending on the brand). Slides and demo code will be available at Github.

Talk structure:

* Introduction: hardware and OpenNI goodies and a tale of PCL (5’)
* Hands On PyOpenNI
    * Normal and Depth camera - basics concepts and small demo (5’)
    * Skeleton - basics concepts and small demo. (5’)
	* Hand & gesture - basics concepts and small demo. (5’)
* Final Demo
	* What we’re going to use? Flask, ZMQ, THREE.JS, PyOpenNI. (6’)
* Q&A. (4’)
---
description_short: This talk will show how to build a simple open source based NUI (Natural User Interface)  game with 3D Sensors, incorporating PyOpenNI with PyGame and WebGL.
OpenNI allows you operate several 3D sensors, enabling hardware independent game development (supported 3D sensors are Microsoft Kinect, PrimeSense Carmine or Asus XTion). It also runs on Linux, Mac OS X and Windows.
---
tags: []
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=3FubECqg688
---
copyright: http://creativecommons.org/licenses/by/3.0/
---
slug: 3d-sensors-and-python-a-space-odyssey
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i.ytimg.com/vi/3FubECqg688/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/3FubECqg688?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/3FubECqg688?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: 
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: 
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 