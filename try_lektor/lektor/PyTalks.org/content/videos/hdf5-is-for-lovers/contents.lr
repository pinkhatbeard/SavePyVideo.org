_model: video
---
title: HDF5 is for lovers
---
id: 1196
---
date_created: 2012-08-31T16:34:33
---
date_updated: 2014-04-08T20:28:27.175
---
date_recorded: 2012-07-16
---
event: SciPy 2012
---
speakers: ['Anthony Scopatz']
---
description_long: HDF5 is a hierarchical, binary database format that has become a _de facto_
standard for scientific computing. While the specification may be used in a
relatively simple way (persistence of static arrays) it also supports several
high-level features that prove invaluable. These include chunking, ragged
data, extensible data, parallel I/O, compression, complex selection, and in-
core calculations. Moreover, HDF5 bindings exist for almost every language -
including two Python libraries (PyTables and h5py).

This tutorial will discuss tools, strategies, and hacks for really squeezing
every ounce of performance out of HDF5 in new or existing projects. It will
also go over fundamental limitations in the specification and provide creative
and subtle strategies for getting around them. Overall, this tutorial will
show how HDF5 plays nicely with all parts of an application making the code
and data both faster and smaller. With such powerful features at the
developer's disposal, what is not to love?!

This tutorial is targeted at a more advanced audience which has a prior
knowledge of Python and NumPy. Knowledge of C or C++ and basic HDF5 is
recommended but not required.


---
description_short: 
---
tags: []
---
duration: None
---
language: English
---
source_url: http://youtube.com/watch?v=Nzx0HAd3FiI
---
copyright: CC BY-SA
---
slug: hdf5-is-for-lovers
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i3.ytimg.com/vi/Nzx0HAd3FiI/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/Nzx0HAd3FiI?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/Nzx0HAd3FiI?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: http://s3.us.archive.org/nextdayvideo/enthought/scipy_2012/HDF5_is_for_lovers.mp4?Signature=cM8rA2NfAmu5op3Ha%2FNG7gQUYFc%3D&Expires=1346380446&AWSAccessKeyId=FEWGReWX3QbNk0h3
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 