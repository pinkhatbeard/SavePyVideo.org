_model: video
---
title: Campaign for IT literacy through FOSS and Spoken Tutorials
---
id: 2787
---
date_created: 2014-07-15T22:45:39.584
---
date_updated: 2014-07-16T12:09:40.696
---
date_recorded: 2014-07-13
---
event: SciPy 2014
---
speakers: ['Kannan Moudgalya']
---
description_long: The FOSSEE team has been promoting the use of FOSS in educational
institutions in India. It focuses on the following FOSS systems currently:
Scilab, Python, Oscad, OpenFOAM, COIN-OR and OpenFormal
([http://fossee.in](http://fossee.in)).  On each of these systems, the
following three standardised help are provided:

1. Support to conduct spoken tutorial based workshops, explained below
2. Creation of Textbook Companion (TBC) 
3. Support to Lab Migration.

A TBC is a collection of code for solved examples of standard textbooks.  We
have completed a large number of TBCs on Scilab and made them available for
online use at [Scilab](http://cloud.scilab.in/) and offline use at
[Completed Books](http://www.scilab.in/Completed_Books).  Similarly, one may
access the Python TBC at [Python TBC](http://tbc-python.fossee.in/).  These
TBCs are created by students and teachers from many colleges and each
creator is paid an honorarium through a project funded by the Government of
India.

[Spoken Tutorial](http://spoken-tutorial.org) is a screencast of ten minute
duration on a FOSS topic, created for self learning.  Using Spoken
Tutorials, we conduct two hour long workshops on FOSS topics through
volunteers, who need not be experts.  We conduct online tests and provide
certificates for all who pass the tests.  All of these are done completely
free of cost, thanks to the financial support from the Government of India.
Using this method, we have trained more than 200K students in the last two
years in India ([statistics](http://www.spoken-tutorial.org/statistics)).

The students love this method, see some testimonials at
[http://www.spoken-tutorial.org/testimonials](http://www.spoken-tutorial.org/testimonials).  It is being increasingly
accepted by colleges and universities, officially.  We expect to
conduct 5K to 10K workshops in 2014, training 200K to 500K students.
As we dub the spoken part into all 22 languages of India, the FOSS
topics are accessible also to students who are not fluent in English,
thereby helping us reach out to many students.  Spoken Tutorial, which
started as a documentation project for FOSS systems has transformed
into a massive training programme.  Our methods are scalable and are
available to the FOSS enthusiasts in the rest of the world.

---
description_short: Textbook Companion (TBC) has code for solved problems of textbooks,
coordinated by FOSSEE (http://fossee.in).
We explain a collaborative method
that helped create 500 Scilab TBCs (http://cloud.scilab.in) and over 100
Python TBCs (http://tbc-python.fossee.in/). We also explain a self learning
method that trained 250,000 students on FOSS systems using spoken
tutorials (http://spoken-tutorial.org).

---
tags: []
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=rCSoGLjqyyE
---
copyright: http://www.youtube.com/t/terms
---
slug: campaign-for-it-literacy-through-foss-and-spoken
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/rCSoGLjqyyE/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/rCSoGLjqyyE?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/rCSoGLjqyyE?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: 
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: 
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 