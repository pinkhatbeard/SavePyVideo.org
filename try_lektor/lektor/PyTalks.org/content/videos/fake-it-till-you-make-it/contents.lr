_model: video
---
title: Fake it till you make it
---
id: 2439
---
date_created: 2013-10-19T19:58:52
---
date_updated: 2014-04-08T20:28:26.079
---
date_recorded: 2013-09-14
---
event: Kiwi PyCon 2013
---
speakers: ['Ravi Chandra']
---
description_long: @ Kiwi PyCon 2013 - Saturday, 07 Sep 2013 - Track 1

**Audience level**

Intermediate

**Abstract**

In practise most of us have found testing to be much harder to achieve in the "real world". That is often because the application architecture is modular with some level of service abstraction, and the ensuing layers of code.

The premise of this talk is: do not test someone else's code. They are a good developer. They would have tested it so it must work.

Taking this perspective allows us to develop fast and compact unit tests for our application functionality while ignoring bigger picture integration issues (these can be addresses using other types of tests!). In our experience this unburdens developers so they can really practice test-driven development much more diligently.

Implementation of this approach is simplified thanks to the mock library. Mock provides a core MagicMock class that obviates the need for custom stubs throughout the code. Moreover, its supports patching specific methods, properties, and more. Mocking is used to patch calls to external code within our unit tests, since we don't need to test them, in a non-non-invasive way to our application.

**Slides**

https://speakerdeck.com/nzpug/ravi-chandra-fake-it-till-you-make-it
---
description_short: A practical introduction to unit testing decoupled, service-oriented, Python applications using with Mock library. A three-tiered (server, client, and view) Flask web application is used as a motivating example across the talk. Mock helps to isolate functionality specific to a layer to facilitate fast, compact, unit testing while avoiding writing specific mock classes or using fixtures.
---
tags: []
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=HbJZu2qcC-s
---
copyright: 
---
slug: fake-it-till-you-make-it
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/HbJZu2qcC-s/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/HbJZu2qcC-s?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/HbJZu2qcC-s?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: 
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: 
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 