_model: video
---
title: PyOhio 2010: Implementation of a Numerical Simulation in Python
---
id: 515
---
date_created: 2012-02-23T04:20:00
---
date_updated: 2014-04-08T20:28:25.831
---
date_recorded: None
---
event: PyOhio 2010
---
speakers: ['Jeffrey Armstrong']
---
description_long: Implementation of a Numerical Simulation in Python

Presented by Jeffrey B. Armstrong

The Python programming language is well suited for numerical computation under
a variety of circumstances. Python offers advantages over competing free and
commercial technologies, including price, functionality, and maintainability.
Specifically, the combination of mature numerical libraries and liberal
licensing allow complex simulations to be coded with ease and to be made
available to nearly all interested parties. !NumPy/!SciPy, database access,
networking, and optimization techniques are examined in detail with respect to
numerical computation. A practical example involving an aerothermal commercial
turbofan aircraft engine simulation showcases these advantages. An aircraft
engine is broken down into discrete stages, including compressors, turbines,
and other flow-related components. Commonalities between components, such as
rotation and the presence of inlet and exit conditions, map cleanly to the
object-oriented nature of Python. Based on simulation needs and hardware
availability, Python allows for the parallel computation of simulations
without the expense and complexity of commercial parallelization packages.


---
description_short: 
---
tags: ['database', 'networking', 'numpy', 'optimization', 'pyohio', 'pyohio2010', 'scipy', 'simulation']
---
duration: None
---
language: English
---
source_url: 
---
copyright: Creative Commons Attribution-NonCommercial-ShareAlike 3.0
---
slug: pyohio-2010--implementation-of-a-numerical-simula
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://a.images.blip.tv/Pyohio-ImplementationOfANumericalSimulationInPython272.png
---
embed: 
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: http://05d2db1380b6504cc981-8cbed8cf7e3a131cd8f1c3e383d10041.r93.cf2.rackcdn.com/pyohio-2010/515_pyohio-2010-implementation-of-a-numerical-simulation-in-python.flv
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: 
---
video_ogv_download_only: False
---
video_ogv_length: 105404408
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 