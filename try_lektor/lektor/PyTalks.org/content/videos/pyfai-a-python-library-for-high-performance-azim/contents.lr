_model: video
---
title: PyFAI: a Python library for high performance azimuthal integration on GPU
---
id: 3332
---
date_created: 2014-11-05T22:41:04.279
---
date_updated: 2014-11-05T22:47:28.886
---
date_recorded: 2014-10-22
---
event: EuroScipy 2014
---
speakers: ['Giannis Ashiotis', 'Jerome Kieffer']
---
description_long: In the field of X-Ray diffraction, 2D area detectors like ccd or pixel detectors have become popular in the last 15 years for diffraction experiments thanks to the large solid-angle coverage and to their better signal linearity. These detectors have a large sensitive area of millions of pixels with high spatial resolution and are getting fast: one kilo-Hertz is expected this year. The software package pyFAI we present here has been designed to reduce X-ray 2D-diffraction images into 1D curves (azimuthal integration) usable by other software for in-depth analysis such as Rietveld refinement, ... Other averaging patterns like 2D integration, image distortion, ... are also available. PyFAI is a library featuring a clean pythonic interface and aiming at being integrated into other software. But it also needs to cope with the data deluge coming from the detector...

In this contribution, we would like to highlight the performance reached by this library. To get today’s computers best performances, one needs to have parallelized code and azimuthal integration is not directly parallelizable. After a scatter-to-gather transformation of the algorithm, it got parallel (Cython implementation). Other optimizations have been used to get the best performances out of GPU (compensated summation, partial parallel reductions, ...). 
---
description_short: 
---
tags: []
---
duration: None
---
language: English
---
source_url: https://www.youtube.com/watch?v=QSlo_Nyzeig
---
copyright: youtube
---
slug: pyfai-a-python-library-for-high-performance-azim
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: https://i.ytimg.com/vi/QSlo_Nyzeig/hqdefault.jpg
---
embed: <iframe type='text/html' src='http://www.youtube.com/embed/QSlo_Nyzeig' width='640' height='360' frameborder='0' allowfullscreen='true'/>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: 
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: 
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 