_model: video
---
title: SciPy 2013 Keynote: The New Scientific Publishers
---
id: 2041
---
date_created: 2013-07-04T10:08:42
---
date_updated: 2014-04-08T20:28:26.439
---
date_recorded: 2013-07-01
---
event: SciPy 2013
---
speakers: []
---
description_long: 
---
description_short: Presenter: William Schroeder, Kitware

Track: Keynotes

Scientific societies such as the Royal Society were formed in the 17th century with the goals of sharing information and ensuring reproducibility. Very quickly scientific letters and publications were assembled into collected transactions and eventually journals. For hundreds of years publishers served admirably as disseminators of scientific knowledge. Publications, and the associated peer review process, became central to the scientific process, greatly impacting how science is practiced, knowledge disseminated, and careers made. However, as software becomes increasingly important to the practice of science, and data becomes larger and more complex, the conventional scientific journal is no longer an adequate vehicle to communicate scientific findings and ensure reproducibility. So who are the new scientific publishers filling these needs, and what roles will they play in the future of science?

In this presentation we'll discuss the central mandate of reproducibility, and the role of Open Science, in particular Open Access, Open Source and Open Data, and how emerging communities and organizations are filling the needs of the scientific community. We'll also discuss the challenges of curating the avalanche of scientific knowledge, whether it be software, data or publications, and how these communities and organizations can work together to support science progress, and ensure continued technological innovation.
---
tags: ['Tech']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=7FIjpuwE0BQ
---
copyright: http://www.youtube.com/t/terms
---
slug: scipy-2013-keynote-the-new-scientific-publishers-1
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/7FIjpuwE0BQ/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/7FIjpuwE0BQ?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/7FIjpuwE0BQ?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: needs editing