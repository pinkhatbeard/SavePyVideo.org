_model: video
---
title: PLY and PyParsing (#93)
---
id: 265
---
date_created: 2012-02-23T04:20:00
---
date_updated: 2014-04-08T20:28:28.167
---
date_recorded: 2010-02-19
---
event: PyCon US 2010
---
speakers: ['Andrew Dalke']
---
description_long: PLY and PyParsing

  
Presented by Andrew Dalke

  
Got a text parsing problem? In most cases using string manipulation and
regular expressions will solve it for you. But when the input is complex, try
reaching for a parsing system to help out.

  
The two most popular in Python are PLY and PyParsing. PLY follows the lex/yacc
tradition with a domain specific language to describe the tokens and grammar.
It was built with both error diagnostics and performance in mind. PyParsing is
a recursive descent parser which expresses the format as a Python data
structure. It make no distinction between lexer and grammar and has a uniform
callback system which makes certain types of data extraction very easy.

  
In my talk I'll show the basics of how to use both systems for several
different format parsing tasks, of different complexity. This will let you see
how to use the parsers and understand more of the tradeoffs between
complexity, readability, error handling, and performance.


---
description_short: 
---
tags: ['ply', 'pycon', 'pycon2010', 'pyparsing']
---
duration: None
---
language: English
---
source_url: 
---
copyright: Creative Commons Attribution-NonCommercial-ShareAlike 3.0
---
slug: pycon-2010--ply-and-pyparsing---93
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://a.images.blip.tv/Pycon-PyCon2010PLYAndPyParsing93735-488.jpg
---
embed: 
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: http://05d2db1380b6504cc981-8cbed8cf7e3a131cd8f1c3e383d10041.r93.cf2.rackcdn.com/pycon-us-2010/265_ply-and-pyparsing-93.m4v
---
video_ogv_download_only: False
---
video_ogv_length: 163502409
---
video_ogv_url: http://05d2db1380b6504cc981-8cbed8cf7e3a131cd8f1c3e383d10041.r93.cf2.rackcdn.com/pycon-us-2010/265_ply-and-pyparsing-93.ogv
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 