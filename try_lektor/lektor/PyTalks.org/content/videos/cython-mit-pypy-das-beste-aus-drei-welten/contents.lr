_model: video
---
title: Cython mit PyPy - das Beste aus drei Welten
---
id: 1431
---
date_created: 2012-11-01T13:43:58
---
date_updated: 2014-04-08T20:28:26.930
---
date_recorded: 2012-10-30
---
event: PyCon DE 2012
---
speakers: ['Stefan Behnel']
---
description_long: 
---
description_short: Die Programmiersprache Cython ([http://cython.org](http://cython.org) "Web-
Seite des Cython-Projekts") ist das de-facto Standard-Tool um CPython um
nativen Code zu erweitern. Die Python-ähnliche Sprache macht schon seit
einigen Jahren das Schreiben von schnellen Erweiterungsmodulen für CPython und
das Anbinden von externen nativen Bibliotheken (C/C++/Fortran/...) so einfach
wie Python selbst und gleichzeitig so effizient wie C. Davon profitiert
besonders das weite Feld des High-Performance-Computing in Python
(NumPy/SciPy/Sage/...), aber auch zahllose andere Bereiche, die eine
effiziente Verarbeitung großer Datenmengen und die Anbindung von nativen
Bibliotheken an die Programmiersprache Python benötigen.

Die neueste Version des Cython-Compilers bietet erstmals auch Unterstützung
für PyPy an, eine alternative Python-Implementierung, die vor allem durch
ihren schnellen JIT-Compiler von sich reden macht. Dadurch kann einmal mit
Cython geschriebener und in C übersetzter Code sowohl in CPython als auch in
PyPy verwendet werden. So wird es beispielsweise möglich, in einer einzigen
Code-Basis effiziente Anbindungen externer Bibliotheken für beide
Laufzeitumgebungen zu entwickeln.

Dieser Vortrag von einem der Cython Core-Entwickler erklärt, welche
Fallstricke dabei noch auf Benutzerseite zu erwarten sind und gibt
Hilfestellungen beim Schreiben von Cython-Code, der sowohl in CPython als auch
in PyPy funktioniert.


---
tags: []
---
duration: None
---
language: German
---
source_url: https://www.youtube.com/watch?v=IETT76NN6Z8
---
copyright: 
---
slug: cython-mit-pypy-das-beste-aus-drei-welten
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i2.ytimg.com/vi/IETT76NN6Z8/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/IETT76NN6Z8?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/IETT76NN6Z8?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: http://s3.us.archive.org/nextdayvideo/pyconde/pyconde2012/Vortrag_Cython_mit_PyPy_das_Be.mp4?Signature=JeqVDywROU3i4DxAbmNJDPZ%2FvVM%3D&Expires=1351795204&AWSAccessKeyId=FEWGReWX3QbNk0h3
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 