_model: video
---
title: Anatomy of Matplotlib, SciPy2013 Tutorial, Part 2 of 3
---
id: 2170
---
date_created: 2013-07-04T10:09:06
---
date_updated: 2014-04-08T20:28:26.459
---
date_recorded: 2013-06-27
---
event: SciPy 2013
---
speakers: []
---
description_long: 
---
description_short: Presenter: Benjamin Root

Description

This tutorial will be the introduction to matplotlib, intended for users who want to become familiar with python's predominate scientific plotting package. First, the plotting functions that are available will be introduced so users will know what kinds of graphs can be done. We will then cover the fundamental concepts and terminologies, starting from the figure object down to the artists. In an organized and logical fashion, the components of a matplotlib figure are introduced, such as the axes, axis, tickers, and labels. We will explain what an Artist is for, as well as explain the purpose behind Collections. Finally, we will take an overview of the major toolkits available to use, particularly AxesGrid, mplot3d and basemap.

Outline

Outline:

Introduction

Purpose of matplotlib
Online Documentation
Examples Page
Gallery Page
FAQs
API documentation
Mailing Lists
Github Repository
Bug Reports & Feature Requests
What is this "backend" thing I keep hearing about?

Plotting Functions

Graphs (plot, scatter, bar, stem, etc.)
Images (imshow, pcolor, pcolormesh, contour[f], etc.)
Lesser Knowns: (pie, acorr, hexbin, etc.)
Brand New: streamplot()
What goes in a Figure?

Axes
Axis
ticks (and ticklines and ticklabels) (both major & minor)
axis labels
axes title
figure suptitle
axis spines
colorbars (and the oddities thereof)
axis scale
axis gridlines
legend
(Throughout the aforementioned section, I will be guiding audience members through the creation and manipulation of each of these components to produce a fully customized graph)

Introducing matplotlibrc

Hands-On: Have users try making some changes to the settings and see how a resulting figure changes
What is an Artist?

Hands-On: Have audience members create some and see if they can get them displayed
What is a Collection?

Hands-On: Have audience members create some, manipulate the properties and display them
Properties:

color (and edgecolor, linecolor, facecolor, etc...)
linewidth and edgewidth and markeredgewidth (and the oddity that happens in errorbar())
linestyle
zorder
visible
What are toolkits?

axes_grid1
mplot3d
basemap
Required Packages

NumPy

Matplotlib (version 1.2.1 or later is preferred, but earlier version should still be sufficient for most of the tutorial)

ipython v0.13

Documentation

https://dl.dropbox.com/u/7325604/AnatomyOfMatplotlib.ipynb
---
tags: ['Tech']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=c553uCRmEVc
---
copyright: http://www.youtube.com/t/terms
---
slug: anatomy-of-matplotlib-scipy2013-tutorial-part-2-1
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/c553uCRmEVc/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/c553uCRmEVc?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/c553uCRmEVc?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: needs editing