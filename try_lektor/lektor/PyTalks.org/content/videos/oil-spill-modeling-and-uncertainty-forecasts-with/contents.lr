_model: video
---
title: Oil spill modeling and uncertainty forecasts with Python; SciPy 2013 Presentation
---
id: 1984
---
date_created: 2013-07-04T10:08:34
---
date_updated: 2014-04-08T20:28:26.356
---
date_recorded: 2013-07-02
---
event: SciPy 2013
---
speakers: []
---
description_long: 
---
description_short: Authors: Hou,Xianlong, University of Texas at Austin; Hodges,Ben, University of Texas at Austin

Track: Meteorology, Climatology, Atmospheric and Oceanic Science

A new method is presented to provide automatic sequencing of multiple hydrodynamic models and automated analysis of model forecast uncertainty. The Hydrodynamic and Oil Spill model Python (hyospy) wrapper was developed to run a hydrodynamic model, link with the oil spill, and visualize results. Hyospy completes the following steps automatically: (1) downloads wind and tide data (nowcast, forecast and historical); (2) converts data to hydrodynamic model input; (3) initializes a sequence of hydrodynamic models starting at pre-defined intervals on multi-processor workstation, and (4) provides visualization on Google Earth. Each model starts from the latest observed data, so that the multiple models provide a range of forecast hydrodynamics with different initial and boundary conditions reflecting different forecast horizons. As a simple testbed for integration and visualization strategies, a Runge-Kutta 4th order (RK4) particle transport method is used for spill transport. The model forecast uncertainty is estimated by the difference between forecasts in the sequenced model runs. The hyospy integrative system shows that challenges in operational oil spill modeling can be met by leveraging existing models and web-visualization methods to provide tools for emergency managers.
---
tags: ['Tech']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=lorRZWM-dSc
---
copyright: http://www.youtube.com/t/terms
---
slug: oil-spill-modeling-and-uncertainty-forecasts-with
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/lorRZWM-dSc/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/lorRZWM-dSc?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/lorRZWM-dSc?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: needs editing