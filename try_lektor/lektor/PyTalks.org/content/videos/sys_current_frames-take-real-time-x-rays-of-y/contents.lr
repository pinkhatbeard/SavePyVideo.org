_model: video
---
title: sys._current_frames(): take real-time X-rays of your software for fun and performance
---
id: 1257
---
date_created: 2012-09-06T22:33:07
---
date_updated: 2014-04-08T20:28:27.224
---
date_recorded: 2012-07-06
---
event: EuroPython 2012
---
speakers: ['Leonardo Almeida']
---
description_long: Profiling is hard. Trying to understand what is making your system slow can be
very frustrating. Specially when it happens only when your clients are
looking, but not you. Python comes with elaborate profiling tools, but
understanding the output of profile/cProfile can be a daunting task, specially
on complex frameworks, and these modules might be impractical in a production
environment where their performance toll can make the system unusable. But
Python is a very reflexive language, allowing extensive investigation of its
own state during runtime, and one of its lesser known tools is the
sys._current_frames() function. It can be used to take an X-ray of what all
the threads in your running Python program are doing. It can also be used for
a kind of “statistical” profiling, with little impact on your running system.
In this talk we’ll investigate how this function can be used to tell what your
program is doing, exactly at the moment when it is misbehaving. We’ll learn
how looking at a series of tracebacks, instead of a bunch of calling
statistics, can help zooming quickly into code hot spots. We’ll also show some
case studies of real world server-side slowdowns that were solved by the use
of this technique which were caused by complex interactions of different
components, including a case where a serious performance issue was diagnosed
in a production deployment of ERP5 in a corporate client of Nexedi’s.


---
description_short: [EuroPython 2012] Leonardo Almeida - 5 JULY 2012 in "Track Spaghetti"


---
tags: []
---
duration: None
---
language: None
---
source_url: http://www.youtube.com/watch?v=MFqP03EfT4I
---
copyright: Standard YouTube License
---
slug: sys_current_frames-take-real-time-x-rays-of-y
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i.ytimg.com/vi/MFqP03EfT4I/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/MFqP03EfT4I?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/MFqP03EfT4I?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 