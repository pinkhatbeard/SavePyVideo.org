_model: video
---
title: Hidden Treasures in the Standard Library
---
id: 370
---
date_created: 2012-02-23T04:20:00
---
date_updated: 2014-04-08T20:28:27.982
---
date_recorded: 2011-03-11
---
event: PyCon US 2011
---
speakers: ['Doug Hellmann']
---
description_long: Hidden Treasures in the Standard Library

Presented by Doug Hellmann

This presentation covers features of the standard library not widely known or
used. Each feature is presented with a short demonstration program and
explanation.

Abstract

The standard library contains many hidden gems that are not widely used,
either because they are not publicized enough or because they are deep in a
module that programmers haven't had cause to study or use. This presentation
covers 8-10 selected topics of this nature in about 25 minutes (leaving time
for a couple of questions). Demonstration code is included for every item.

Possible tips include, in no particular order:

  * Using hmac to verify pickled data before unpacking it. 
  * Using uuid4 to generate session tokens. 
  * Regular expression look-ahead/behind matches. 
  * pdb startup files 
  * Reading files with mmap 
  * Using csv dialects 
  * The robotparser module 
  * The rlcompleter module 
  * Using locale to format numbers and currency 
  * The cgitb module 
  * pkgutil.getdata 
  * contextlib.contextmanager 
  * The cmd module 
  * The fileinput module 


---
description_short: 
---
tags: ['cgitb', 'cmd', 'contextlib.contextmanager', 'csv', 'fileinput', 'hmac', 'locale', 'pdb', 'pkgutil.getdata', 'pycon', 'pycon2011', 'rlcompleter', 'robotparser', 'uuid4']
---
duration: None
---
language: English
---
source_url: 
---
copyright: Creative Commons Attribution-NonCommercial-ShareAlike 3.0
---
slug: pycon-2011--hidden-treasures-in-the-standard-libr
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://a.images.blip.tv/Pycon-PyCon2011HiddenTreasuresInTheStandardLibrary179-925.jpg
---
embed: 
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: http://05d2db1380b6504cc981-8cbed8cf7e3a131cd8f1c3e383d10041.r93.cf2.rackcdn.com/pycon-us-2011/370_hidden-treasures-in-the-standard-library.mp4
---
video_ogv_download_only: False
---
video_ogv_length: 132722607
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 