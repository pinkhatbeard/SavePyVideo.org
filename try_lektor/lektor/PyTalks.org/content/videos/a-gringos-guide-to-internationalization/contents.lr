_model: video
---
title: A Gringo's Guide to Internationalization
---
id: 1379
---
date_created: 2012-10-08T17:39:46
---
date_updated: 2014-04-08T20:28:26.997
---
date_recorded: 2012-09-04
---
event: DjangoCon 2012
---
speakers: ['Jacob Burch']
---
description_long: There's often a bubble in the English-speaking programming world that the
entire internet runs on English. Our Top-level domains are shortened english
words. Our programming languages--Python in particular--try to emulate English
words and grammar.

Firmly in place in this world for many years, my first attempt to translate a
pre-existing site came with a high sticker shock of nuanced difficulty. This
talk covers all the lessons I learned in taking a fully-fleshed web app with
no translation (and a programmer with no translation experience) to being
completely translated.

# Talk Overview

  1. Introduction to Translation
    1. Overview of gettext
    2. Brief history of a translation
      1. Add gettext calls
      2. `django-admin.py makemessages`
      3. Ship po file to translation service
      4. Receive file back
      5. Compile
      6. Fin
    3. gettext vs gettext_lazy
  2. Batteries Included
    1. gettext Python calls
    2. Middleware
    3. template tags
      1. trans vs. blocktrans
    4. Anti-patterns of the battery included use.
      1. Heavy whitespace in blocktrans
      2. Monster-sized translations
  3. The best laid plans of mice and men...
    1. Problems that get in the way of A history of a translation.
      1. Realize half the site is still untranslated
      2. Edit out the translations you already had translated to avoid re-curring costs
      3. Have the product adjust so two words change
      4. Realize common text that's in the database
      5. Fin?
    2. Patterns and tools to help
      1. `poxx.py` and the idea of a 'canonical' po file.
      2. `dbgettext`
      3. Wholly-translated template files (translate_include, trans_render)
  4. Advanced Translation Technique
    1. While undocumented, translation backend is pretty darn flexible and the source code is a treasure trove to make 'impossible' use cases not too bad.
    2. Overview of how the translation service works behind the scenes
      1. `_translation` thread local
      2. Merged translations (root->app->default)
      3. Sessions vs. Accept-Language
      4. Middleware and Context processors
    3. Example of advanced translation code: Multi-tenancy translation.
      1. Translating some sites, not translating others
      2. Translating Site A one way, Site B another
      3. Using an English translation for duct-tape and profit.
  5. Why Django's i18n sucks.
    1. In the vein of the "Django Sucks" Key note. It doesn't really, but it could use some changes--from minor to major.
    2. Why .po Files are archaic and limited.
    3. What we can do to improve for 1.5 and beyond.


---
description_short: Coming from a speaker who escaped a notion to "why would we need to
translate?" this talk aims to show what actually goes into translating a
website or app using Django's Internationalization tools. Covered will be an
overview of batteries included, best practices and anti-patterns in using
them, and some third party tools to help make your life escaping the myth of a
Lingua Franca easier.


---
tags: ['django', 'internationalization']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=j2ZHZWfx60Y
---
copyright: Creative Commons Attribution license (reuse allowed
---
slug: a-gringos-guide-to-internationalization
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i.ytimg.com/vi/j2ZHZWfx60Y/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/j2ZHZWfx60Y?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/j2ZHZWfx60Y?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 