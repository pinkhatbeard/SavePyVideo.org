_model: video
---
title: Cryptography for Django Applications
---
id: 1381
---
date_created: 2012-10-08T17:39:48
---
date_updated: 2014-04-08T20:28:26.984
---
date_recorded: 2012-09-05
---
event: DjangoCon 2012
---
speakers: ['Erik Labianca']
---
description_long: # Introduction

The web is a hostile place, and isn't showing any signs of becoming less so.
In order to mitigate this, many developers turn to cryptography.
Unfortunately, cryptography can be complicated, and is easily circumvented if
not properly handled. This presentation will provide an introduction to
cryptographic tools available to Python/Django applications, appropriate use
cases for each, proper usage, and operational concerns necessary to operate in
a certified environment. Finally, we will also demonstrate a reusable
application that wraps this all up, providing secure key-management
capabilities to a running Django environment via the Django admin.

# Why Encrypt?

# Rules of Encryption

  * Don't do it if you don't need it.
  * Don't write your own.
  * Understand what you're doing if you do.

# When to encrypt?

## Understand what you're protecting

  * Data
  * User records
  * Code
  * Systems

## Understand your attack vectors

  * Data (backups, revision control)
  * Systems
  * Application
  * Transport
  * Client

## Understand the types of encryption you might use:

  * Hashing

### Passwords are a special case. Use a key derivation function

  * PBKDF2 – Upgrade to Django 1.4!

### Algorithms

  * MD5 - fine as a checksum. not fine as a cryptographic hash.
  * SHA1 - fine as a checksum. becoming less fine as a cryptographic hash every day
  * SHA2 - so far so good. use as many bits as you can handle.

## Symmetric Encryption

  * Fast
  * Reversible

### Algorithms

  * Caesar Cipher (for fun puzzles)
  * DES (don't use)
  * AES (certified)
  * Blowfish

## Asymmetric Encryption

  * Slow
  * One-way

### Algorithms

  * RSA
  * DSA

#### Uses

##### Signing

###### Web of Trust

  * PGP

###### PKI

##### Encryption

  * PGP
  * SSL
  * TLS

# Doing it right

## Use known-good algorithms

  * AES-256
  * SHA2
  * RSA
  * DSA

## Use known-good implementations

  * Open Source is good

## Extra Credit

  * FIPS 140 certified implementations
  * FIPS 140 / NIST configurations

## Transport (always use HTTPS)

  * Use good algorithms AES-256

## At Rest (insecure servers or backups)

  * Understand the ramifications of key management

# Examples

## Hashing

  * Use a key-derivation function

### Don't be linked-in

  * Salt your hashes (with a secret). 
  * Salt and pepper your hashes if possible (with a known unique value)

## SSL

  * Forced connections
  * Making the application aware
  * Hardened cipher selection

### Robust PKI

  * Client authentication
  * SSL Test Page

## Asymmetric Encryption

### Key Management

  * Using GPG Agent
  * GPG Manager App

### PGP Files

## Symmetric Encryption

### Key Management

  * Use Asymmetric Encryption

### Use a unique Initialization Vector if possible

  * LoopBack Devices


---
description_short: A review of encryption in the context of a web application storing sensitive
information. Topics covered include choosing whether to use crypto, selection
of tools, proper usage (including examples), and operational considerations
with respect to security assessment.


---
tags: ['cryptography', 'django']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=pWCAADXd-DI
---
copyright: Creative Commons Attribution license (reuse allowed
---
slug: cryptography-for-django-applications
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i.ytimg.com/vi/pWCAADXd-DI/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/pWCAADXd-DI?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/pWCAADXd-DI?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 