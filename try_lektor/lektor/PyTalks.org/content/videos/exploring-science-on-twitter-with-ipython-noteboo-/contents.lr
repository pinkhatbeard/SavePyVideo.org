_model: video
---
title: Exploring Science on Twitter with IPython Notebook and Python Pandas
---
id: 2443
---
date_created: 2013-10-19T19:58:53
---
date_updated: 2014-04-08T20:28:26.072
---
date_recorded: 2013-09-16
---
event: Kiwi PyCon 2013
---
speakers: ['Brenda Moon']
---
description_long: @ Kiwi PyCon 2013 - Sunday, 08 Sep 2013 - Track 1

**Audience level**

Intermediate

**Abstract**

I'm studying Science Communication and looking at how people use the word science on Twitter as a way of understanding what they might mean when they say 'science'. My initial dataset is 13.5 million tweets I collected during 2011. Since the start of 2012 I've been using Python for my data analysis.

I'll demonstrate how I've been using IPython notebook [1] with the Pandas [2] data analysis library to work with large data tables and time series data. The IPython notebook is a great tool for research, allowing notes about research to be kept interleaved with the python code. The ability to quickly see results and plot them using Matplotlib [3] encourages interactive exploration of the data.

[1] http://ipython.org/notebook.html
[2] http://pandas.pydata.org/
[3] http://matplotlib.org/

**Slides**

https://speakerdeck.com/nzpug/brenda-moon-exploring-science-on-twitter-with-ipython-notebook-and-python-pandas
---
description_short: Exploring discussions of 'science' on Twitter by analysing 13.5 million tweets from 2011. I'll demonstrate how I've been using IPython notebook with the Pandas data analysis library to work with large data tables and time series data. You will see the strength of IPython Notebook for research in allowing you to keep notes interleaved with python code.

---
tags: []
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=FzCk747yjCU
---
copyright: 
---
slug: exploring-science-on-twitter-with-ipython-noteboo-
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/FzCk747yjCU/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/FzCk747yjCU?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/FzCk747yjCU?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: 
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: 
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 