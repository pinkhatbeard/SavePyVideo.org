_model: video
---
title: Under the Microscope: Evaluating Existing Django Code When Onboarding a New Client
---
id: 1413
---
date_created: 2012-10-08T17:39:54
---
date_updated: 2014-04-08T20:28:26.980
---
date_recorded: 2012-09-06
---
event: DjangoCon 2012
---
speakers: ['Brian Moloney', 'Joe Jasinski']
---
description_long: Taking over someone else’s code is an exercise fraught with peril. However,
with the growing popularity of Django, more and more organizations are seeking
companies and individuals to take over their Django website and support their
existing Django codebase. This talk will describe the standardized process
Imaginary Landscape has developed to evaluate existing code as part of their
new client onboarding process.

Covered topics:

  * Introduction/Overview

  * Top reasons why clients are looking to change vendor

  * Initial assessment

>     * What questions to ask before looking at the code.

  * Where to start when looking at code? 

>     * Traversing the code tree to get a feel for how the code is structured

>     * Trying to determine how the previous developer thinks

  * Detailed code review including checklist

>     * Things to look for when evaluating code: Version control, "Standard"
site layout, Settings file, hardcoding, Virtualenv, south, etc.

  * Examples (names have been changed to protect the imperfect)

>     * The good: the kinds of coding and configuration techniques that seem
to be consistent among well-thought-out projects.

>     * The bad: examples of code and configuration that make onboarding and
maintenance difficult.

  * Final thoughts

>     * What you can do as a developer taking over a project.

>     * What you can do as a developer hoping to make great code that others
may someday see.

>     * Exercise your right to say no, it’s your reputation on the line

  * Q&A


---
description_short: As a Web development firm that specializes in Django, we receive many
inquiries from organizations looking for assistance with their existing Django
websites. This session will describe our process for evaluating existing
codebases and deployment structures. The goal is to provide a framework for
evaluating other people's code and understand the scrutiny your code may
someday endure.


---
tags: ['django']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=-hma3mlcrD8
---
copyright: Creative Commons Attribution license (reuse allowed
---
slug: under-the-microscope-evaluating-existing-django
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i.ytimg.com/vi/-hma3mlcrD8/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/-hma3mlcrD8?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/-hma3mlcrD8?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: needs editing