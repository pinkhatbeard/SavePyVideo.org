_model: video
---
title: Reproducible Documents with PythonTeX; SciPy 2013 Presentation
---
id: 2002
---
date_created: 2013-07-04T10:08:37
---
date_updated: 2014-04-08T20:28:26.363
---
date_recorded: 2013-07-02
---
event: SciPy 2013
---
speakers: []
---
description_long: 
---
description_short: Authors: Poore, Geoffrey, Union University

Track: Reproducible Science

Writing a scientific document can be slow and error-prone. When a figure or calculation needs to be modified, the code that created it must be located, edited, and re-executed. When data changes or analysis is tweaked, everything that depends on it must be updated. PythonTeX is a LaTeX package that addresses these issues by allowing Python code to be included within LaTeX documents. Python code may be entered adjacent to the figure or calculation it produces. Built-in utilities may be used to track dependencies.

PythonTeX maximizes performance and efficiency. All code output is cached, so that documents can be compiled without executing code. Code is only re-executed when user-specified criteria are met, such as exit status or modified dependencies. In many cases, dependencies can be detected and tracked automatically. Slow code may be isolated in user-defined sessions, which automatically run in parallel. Errors and warnings are synchronized with the document so that they have meaningful line numbers.

Since PythonTeX documents mix LaTeX and Python code, they are less portable than plain LaTeX documents. PythonTeX includes a conversion utility that creates a new copy of a document in which all Python code is replaced by its output. The result is suitable for journal submission or conversion to other formats such as HTML.

While PythonTeX is primarily intended for Python, its design is largely language-independent. Users may easily add support for additional languages.
---
tags: ['Tech']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=G-UDHc2UVOg
---
copyright: http://www.youtube.com/t/terms
---
slug: reproducible-documents-with-pythontex-scipy-2013
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/G-UDHc2UVOg/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/G-UDHc2UVOg?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/G-UDHc2UVOg?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: needs editing