_model: video
---
title: Scrape the Web: Strategies for programming websites that don't expect it
---
id: 256
---
date_created: 2012-02-23T04:20:00
---
date_updated: 2014-04-08T20:28:28.196
---
date_recorded: 2010-02-19
---
event: PyCon US 2010
---
speakers: ['Asheesh Laroia']
---
description_long: Scrape the Web: Strategies for programming websites that don't expect it

  
Presented by Asheesh Laroia

  
Do you find yourself faced with websites that have data you need to extract?
Would your life be simpler if you could programmatically input data into web
applications, even those tuned to resist interaction by bots?

  
Year by year, the web is becoming a stronger force. Learn how to get the best
of it.

  
We'll discuss the basics of web scraping, and then dive into the details of
different methods and where they are most applicable. You'll leave with an
understanding of when to apply different tools, and learn about automating a
full web browser, a "heavy hammer" that I picked up at a project for the
Electronic Frontier Foundation.

  
Atendees should bring a laptop, if possible, to try the examples we discuss
and optionally take notes. Code samples will be made available after class
with no restrictions. Intended Audience

  
Intermediate (or better) Python programmers, probably without extensive web
testing experience

  
Class Outline

  * My motto: "The website is the API." 
  * Choosing a parser: BeautifulSoup, lxml, HTMLParse, and html5lib. 
  * Extracting information, even in the face of bad HTML: Regular expressions, BeautifulSoup, SAX, and XPath. 
  * Automatic template reverse-engineering tools. 
  * Submitting to forms. 
  * Playing with XML-RPC 
  * DO NOT BECOME AN EVIL COMMENT SPAMMER. 
  * Countermeasures, and circumventing them: 
    * IP address limits 
    * Hidden form fields 
    * User-agent detection 
    * JavaScript 
    * CAPTCHAs 
  * Plenty of full source code to working examples: 
    * Submitting to forms for text-to-speech. 
    * Downloading music from web stores. 
    * Automating Firefox with Selenium RC to navigate a pure-JavaScript service. 
  * Q&A; and workshopping 
  * Use your power for good, not evil. 


---
description_short: We'll discuss the basics of web scraping, and then dive into the details of
different methods and where they are most applicable. You'll leave with an
understanding of when to apply different tools, and learn about automating a
full web browser, a "heavy hammer" that I picked up at a project for the
Electronic Frontier Foundation.


---
tags: ['pycon', 'pycon2010', 'scraping', 'web']
---
duration: None
---
language: English
---
source_url: 
---
copyright: Creative Commons Attribution-NonCommercial-ShareAlike 3.0
---
slug: pycon-2010--scrape-the-web--strategies-for-progra
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://a.images.blip.tv/Pycon-PyCon2010ScrapeTheWebStrategiesForProgrammingWebsitesTha613-141.jpg
---
embed: 
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: http://05d2db1380b6504cc981-8cbed8cf7e3a131cd8f1c3e383d10041.r93.cf2.rackcdn.com/pycon-us-2010/256_scrape-the-web-strategies-for-programming-websites-that-don-t-expect-it.m4v
---
video_ogv_download_only: False
---
video_ogv_length: 851529869
---
video_ogv_url: http://05d2db1380b6504cc981-8cbed8cf7e3a131cd8f1c3e383d10041.r93.cf2.rackcdn.com/pycon-us-2010/256_scrape-the-web-strategies-for-programming-websites-that-don-t-expect-it.ogv
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: 