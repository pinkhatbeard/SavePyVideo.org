_model: video
---
title: Unlocking Data Trapped in Audio and Video Files
---
id: 3165
---
date_created: 2014-09-26T00:28:00.625
---
date_updated: 2014-10-12T22:50:16.711
---
date_recorded: 2014-10-04
---
event: PyTexas 2014
---
speakers: ['Paul Murphy']
---
description_long: As more and more apps record audio and video files we need to start thinking about what to do with those files.  Playing them back isn't enough.  Media files are full of data that developers can start exploiting thanks to an emergent category of signal and natural language processing APIs.

There are only 3 options for processing the words embedded in these files:

1. Transcribe them yourself, manually.
2. Find a transcript made by someone else.
3. Use a library that extracts the words for you.   

As the developer of a python library that automates the extraction and processing of words in media files, I'll demonstrate how easy it is to make audio and video libraries fully searchable, create a word cloud of keywords from a recorded phone call, and extract topics from news broadcast.  

I'll show coding examples as well as products using this API.
---
description_short: 
---
tags: []
---
duration: 1500
---
language: English
---
source_url: http://youtu.be/KI0m0tFEAnY
---
copyright: CC-BY
---
slug: unlocking-data-trapped-in-audio-and-video-files
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i.ytimg.com/vi/KI0m0tFEAnY/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/KI0m0tFEAnY?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/KI0m0tFEAnY?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: http://64966d3674e0a64d8f4a-47c94b14ef8e1f83d5390cdb0629c6ed.r53.cf2.rackcdn.com/pytexas-2014/3165_Unlocking_Data_Trapped_in_Audio_and_Video_Files.webm
---
whiteboard: 