_model: video
---
title: SunPy - Python for Solar Physicists; SciPy 2013 Presentation
---
id: 2043
---
date_created: 2013-07-04T10:08:42
---
date_updated: 2014-04-08T20:28:26.448
---
date_recorded: 2013-07-01
---
event: SciPy 2013
---
speakers: []
---
description_long: 
---
description_short: Authors: Mumford, Stuart, University of Sheffield / SunPy

Track: Astronomy and Astrophysics

Modern solar physicists have, at their disposal, an abundance of space and ground based instruments providing a large amount of data to analyse the complex Sun every day. The NASA Solar Dynamics Observatory satellite, for example, collects around 1.2 TB of data every 24 hours which requires extensive reconstruction before it is ready for scientific use. Currently most data processing and analysis for all solar data is done using IDL and the 'SolarSoft' library. SunPy is a project designed to provide a free, open and easy-to-use Python alternative to IDL and SolarSoft.

SunPy provides unified, coordinate-aware data objects for many common solar data types and integrates into these plotting and analysis tools. Providing this base will give the global solar physics community the opportunity to use Python for future data processing and analysis routines. The astronomy and astrophysics community, through the implementation and adoption of AstroPy and pyRAF, have already demonstrated that Python is well suited for the analysis and processing of space science data.

In this presentation, we give key examples of SunPy's structure and scope, as well as the major improvements that have taken place to provide a stable base for future expansion. We discuss recent improvements to file I/O and visualisation, as well as improvements to the structure and interface of the map objects.

We discuss the many challenges which SunPy faces if it is to achieve its goal of becoming a key package for solar physics. The SunPy developers hope to increase the the visibility and uptake of SunPy, and encourage people to contribute to the project, while maintaining a high quality code base, which is facilitated by the use of a social version control system (git and GitHub).
---
tags: ['Tech']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=bXPPTCkaVu8
---
copyright: http://www.youtube.com/t/terms
---
slug: sunpy-python-for-solar-physicists-scipy-2013-p
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/bXPPTCkaVu8/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/bXPPTCkaVu8?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/bXPPTCkaVu8?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: needs editing