_model: video
---
title: Real time Crunching of Petabytes of Geospatial Data with Google Earth Engine
---
id: 2772
---
date_created: 2014-07-15T22:45:37.322
---
date_updated: 2014-07-15T23:12:43.769
---
date_recorded: 2014-07-14
---
event: SciPy 2014
---
speakers: ['Randy Sargent']
---
description_long: Background
==========

* What is [Earth Engine](https://earthengine.google.org) at a high level?
* Why did the Earth Engine (EE) project start? [To monitor global deforestation.](http://blog.google.org/2010/12/introducing-google-earth-engine.html)
* What architecture design decisions were made, and why?
    - Just-in-time computation model
    - Lazy evaluation for real-time feedback

The Earth Engine Python API
===========================

* PyPI package: [earthengine-api](https://pypi.python.org/pypi/earthengine-api)
* OAuth authentication
* Using IPython Notebooks for algorithm development
    - Special display methods for interactive maps

Philosophical goals and how they are manifested
===============================================

* Organize the world's (geospatial) information and make it universally accessible and useful
* Facilitate open transparent science
* Speed up science by reducing the effort required to test hypotheses
* Enable collaborative algorithm development

Selected Results
================

* Consumer-grade visualizations
    - Time-lapse global scale interactive video - [blog post](http://googleblog.blogspot.com/2013/05/a-picture-of-earth-through-time.html), [interactive viewer (centered on Austin)](https://earthengine.google.org/#timelapse/v=30.27632,-97.74597,10.812,latLng&t=1.67)
* Science-grade Data Products
    - High-Resolution Global Maps of 21st-Century Forest Cover Change - [Science journal publication](http://www.sciencemag.org/content/342/6160/850), [blog post](http://googleresearch.blogspot.com/2013/11/the-first-detailed-maps-of-global.html), [interactive viewer](http://earthenginepartners.appspot.com/science-2013-global-forest)

The Future
==========

* Global-scale analysis challenges
* An invitation for developers

---
description_short: Google Earth Engine is a platform designed to enable petabyte-scale scientific analysis and visualization of geospatial datasets.  Earth Engine provides a consolidated environment including a massive data catalog co-located with thousands of computers for analysis. This talk will discuss products that Earth Engine has produced, and how to access Earth Engine via its Python API.
---
tags: []
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=8LZGBL4U3F4
---
copyright: http://www.youtube.com/t/terms
---
slug: real-time-crunching-of-petabytes-of-geospatial-da
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/8LZGBL4U3F4/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/8LZGBL4U3F4?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/8LZGBL4U3F4?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: 
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: 
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 