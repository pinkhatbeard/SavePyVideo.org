_model: video
---
title: Version Control and Unit Testing for Scientific Software, SciPy2013 Tutorial, Part 3 of 3
---
id: 1932
---
date_created: 2013-07-01T19:52:11
---
date_updated: 2014-04-08T20:28:26.513
---
date_recorded: 2013-06-27
---
event: SciPy 2013
---
speakers: []
---
description_long: 
---
description_short: Presenters: Matt Davis, Katy Huff

Description

Writing software can be a frustrating process but developers have come up with ways to make it less stressful and error prone. Version control saves the history of your project and makes it easier for multiple people to participate in development. Unit testing and testing frameworks help ensure the correctness of your code and help you find errors by quickly executing and testing your entire code base. These tools can save you time and stress and are valuable to anyone writing software of any description.

This collaborative, hands-on tutorial will cover version control with Git plus writing and running unit tests in Python (and IPython!) using the nose testing framework. Attendees should be comfortable with the basics of Python and the command line but no experience with scientific Python is necessary.

Outline

The tutorial will be split into two two-hour lessons. The first lesson will cover git/GitHub and the second lesson will cover unit testing. Throughout the entirety of the tutorial students will be working in pairs. Our teaching style is to have frequent, short exercises for students. Ideally instructors aren't talking for more than a few minutes before they stop and students do something on their own machines. Instructors then give an explanation and example, then move on to the next item.

Lesson 1: git/GitHub

Students will work in pairs and in each pair one student will make a GitHub repo, then give the other student commit access on that repo. Students will take turns making modifications to learn various bits of git functionality.

Lesson 2: Unit Testing

Continuing to work in pairs, students will use test driven development to construct a small scientific program in the IPython Notebook and then take their work to .py files to see how nose works from the command line.

Required Packages

In addition to standard Python this tutorial will require the nose testing framework, the IPython Notebook, and the command line interface to Git. Installing Git varies by platform. Windows users should install Git Bash (aka msysgit), Mac users should install the Mac OS X Command Line Tools, and Linux users should use the packaging system for their particular Linux distribution. The Anaconda CE Python installer includes nose and the IPython Notebook.

Documentation

Material will be adapted from existing Software Carpentry lessons, especially: https://github.com/swcarpentry/boot-camps/tree/master/version-control/git/git-and-github https://github.com/swcarpentry/boot-camps/tree/master/python/sw_engineering

Other links: http://software-carpentry.org/ - The software carpentry organization with links to many lessons and past boot camps. https://github.com/swcarpentry/boot-camps - Standard Software Carpentry boot camp curriculum. https://github.com/thehackerwithin/PyTrieste/wiki - Early Software Carpentry curriculum from a two week boot camp at the International Center for Theoretical Physics. https://code.google.com/p/hacker-within/w/list - Lesson notes for a three hour lightning lesson at the American Nuclear Society Conference 2011. http://software-carpentry.org/blog/2012/02/trieste-italy-workshop-week-1.html - A blog post about the first week of the two week ICTP boot camp. http://software-carpentry.org/blog/2012/04/lessons-learned-at-the-university-of-chicago.html - A blog post about a two day boot camp at the University of Chicago. http://software-carpentry.org/blog/2011/11/knowledge-of-the-second-kind.html - A blog post about what the hacker within did before it was absorbed into Software Carpentry (it's no longer really its own entity).
---
tags: ['Tech']
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=j36U6DTKVDY
---
copyright: http://www.youtube.com/t/terms
---
slug: version-control-and-unit-testing-for-scientific-s
---
related_urls: []
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i1.ytimg.com/vi/j36U6DTKVDY/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/j36U6DTKVDY?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/j36U6DTKVDY?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: None
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: None
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: None
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: None
---
whiteboard: needs editing