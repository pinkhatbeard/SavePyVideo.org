_model: video
---
title: Pioneering the Future of Computing Education
---
id: 2954
---
date_created: 2014-07-31T00:46:25.680
---
date_updated: 2014-07-31T22:54:43.958
---
date_recorded: 2014-07-24
---
event: EuroPython 2014
---
speakers: ['Ben Nuttall']
---
description_long: I'm Ben, from Raspberry Pi. I do development and outreach for the Foundation and I work with the rest of the education team to help make learning through computer science, coding and hardware hacking more accessible to all.

In this talk I explain the Raspberry Pi story: its mission - the reason the Pi exists; what happened before release - getting the board in to production; what happened in the first two years - the community birth and growth; and what's coming next - education focus, new hardware and improved software.

Python is the main language used (and advocated by us) in education with Raspberry Pi.

We're creating learning resources to match up with the new UK computing curriculum, where we teach young people programming and computer science concepts with Python on Pi, and help teachers deliver quality material in the classroom to work towards the objectives the curriculum sets out to achieve.

With Raspberry Pi we open up possibilities for connecting to the real world in an accessible way using the powerful, high level and human read/write -able language of Python.

We work closely with the community: hobbyists organising Raspberry Jam events; educators teaching with Raspberry Pi; the software communities and their contributions - and we welcome any interested parties to get involved with helping us provide for the wider community.
---
description_short: How the Raspberry Pi Foundation are leading the way in the computing in schools revolution by providing affordable open and connectable hardware to people of all levels of experience.

Now we have an education team, we're pushing forward with creating resources and training teachers to help deliver modern computing education around the world.

All our learning resources are Creative Commons licensed and available on GitHub. We write materials that match the UK computing curriculum.
---
tags: []
---
duration: None
---
language: English
---
source_url: http://www.youtube.com/watch?v=24eDPoJvmnI
---
copyright: http://creativecommons.org/licenses/by/3.0/
---
slug: pioneering-the-future-of-computing-education
---
related_urls: [{'description': 'Learning Resources and Projects provided by the Raspberry Pi Foundation Education Team', 'url': 'https://github.com/raspberrypilearning'}]
---
related_content: 
---
notes: 
---
state: 1
---
quality_notes: 
---
thumbnail_url: http://i.ytimg.com/vi/24eDPoJvmnI/hqdefault.jpg
---
embed: <object width="640" height="390"><param name="movie" value="http://youtube.com/v/24eDPoJvmnI?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://youtube.com/v/24eDPoJvmnI?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>
---
video_flv_download_only: False
---
video_flv_length: None
---
video_flv_url: 
---
video_mp4_download_only: False
---
video_mp4_length: None
---
video_mp4_url: 
---
video_ogv_download_only: False
---
video_ogv_length: None
---
video_ogv_url: 
---
video_webm_download_only: False
---
video_webm_length: None
---
video_webm_url: 
---
whiteboard: 