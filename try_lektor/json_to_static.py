import os
import shutil
import json
import yaml
from pprint import pprint
from jinja2 import Environment, FileSystemLoader

env = Environment(loader=FileSystemLoader('./templates'))
template = env.get_template('video.jinja')


def get_categories(data):
    categories = []
    no_category = []

    for item in data:
        if item['category']:
            categories.append(item['category'])
        else:
            no_category.append(item['title'])

    return categories, no_category


def json_to_file(data):
    output_type = 'yaml'  # 'json'
    for item in data:
        filename = './yaml/{}'.format(item['slug'])
        with open(filename, 'w') as f:
            if output_type == 'json':
                f.write(json.dumps(item))
            elif output_type == 'yaml':
                f.write(output_type.dump(item))


def json_to_ini(data):
    base_dir = './lektor-videos/'
    for video in data:
        new_dir = base_dir + video['slug']
        if not os.path.isdir(new_dir):
            os.mkdir(new_dir)
        filename = new_dir + '/contents.lr'

        with open(filename, 'w') as f:
            f.write(template.render(data=video))


if __name__ == "__main__":
    if os.listdir('./lektor-videos/'):
        for dir in os.listdir('./data/lektor-videos/'):
            shutil.rmtree('./data/lektor-videos/' + dir)

    with open('./flattened_output.json') as raw_json:
        data = json.load(raw_json)

        # categories, no_category = get_categories(data)
        # pprint(set(categories))
        # json_to_file(data)

        json_to_ini(data)
