import os
import json
import logging
import datetime
from contextlib import contextmanager

from sqlalchemy import (create_engine, Column, Integer, String, Boolean,
                        ForeignKey, DateTime, Table)
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy.orm.exc import NoResultFound


BASE_DIR = os.path.abspath(os.path.dirname(__file__))
DB_PATH = os.path.join(BASE_DIR, './data/raw_db.sqlite')
SQLALCHEMY_DATABASE_URI = ('sqlite:///' + DB_PATH)
engine = create_engine(SQLALCHEMY_DATABASE_URI)
Session = sessionmaker(bind=engine)
Base = declarative_base()


@contextmanager
def session_scope():
    session = Session()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()


class DBMixin(object):
    """
    Mixin for basic DB methods and attributes.
    """
    @declared_attr
    def __tablename__(cls):
        return "{}s".format(cls.__name__.lower())

    id = Column(Integer, primary_key=True)
    date_created = Column(DateTime, nullable=False,
                          default=datetime.datetime.utcnow)
    date_modified = Column(DateTime, nullable=False,
                           default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)

    @classmethod
    def get(cls, id=None, session=None, **kwargs):
        """
        If `id` passed queries for id and returns exactly 1 result.
        If anything else is passed, returns a list of items matching query.
        """
        if not id and not kwargs:
            obj = session.query(cls).all()
        if id:
            obj = session.query(cls).filter_by(id=id).first()
        if kwargs:
            # check if active
            obj = [item for item in
                   session.query(cls).filter_by(**kwargs).all()]
        if not obj:
            logging.info('No results found.')
            raise NoResultFound
        return obj

    @classmethod
    def create(cls, obj, session=None):
        """
        Adds object to sessoin and returns object from DB.

        Was used to simplifiy adding to DB and make it harder to forget to
        close connection. It also returned the object after it was added to
        the DB with it's `id`. However this may or may not work correctly now.
        """
        session.add(obj)
        session.flush()
        obj = cls.get(id=obj.id, session=session)
        logging.info('Created item: {}'.format(obj))
        return obj

    @classmethod
    def _unique(cls, session, arg, kwargs):
        """
        Creates or gets unique object from DB.

        Checks uniqueness based upon `cls.unique_hash` and `cls.unique_filter`.
        If unique it adds it to DB. If not unique it returns the previous item
        already in DB.
        """
        cache = getattr(session, '_unique_cache', None)

        if cache is None:
            session._unique_cache = cache = {}

        key = (cls, cls.unique_hash(*arg, **kwargs))
        if key in cache:
            return cache[key]
        else:
            with session.no_autoflush:
                q = session.query(cls)
                q = cls.unique_filter(q, *arg, **kwargs)
                obj = q.first()
                if not obj:
                    obj = cls(*arg, **kwargs)
                    session.add(obj)
            cache[key] = obj
            return obj

    @classmethod
    def unique_hash(cls, *arg, **kwargs):
        """
        Defines what makes an item unique.
        """
        raise NotImplementedError()

    @classmethod
    def unique_filter(cls, query, *arg, **kwargs):
        """
        Creates query to test uniqueness of object.
        """
        raise NotImplementedError()

    @classmethod
    def as_unique(cls, session, *arg, **kwargs):
        """
        Gets or creats a unique object in the DB.
        """
        return cls._unique(session, arg, kwargs)

    def __repr__(self):
        cls = self.__class__.__name__
        return '<{} id={}>'.format(cls, self.id)

    def __str__(self):
        cls = self.__class__.__name__
        return '<{} id={}>'.format(cls, self.id)


video_speaker = Table('video_speaker', Base.metadata,
                      Column('speaker_id', Integer, ForeignKey('speakers.id')),
                      Column('video_id', Integer, ForeignKey('videos.id')))


class Speaker(DBMixin, Base):
    name = Column(String(200), unique=True)

    def __init__(self, name):
        self.name = name

    @classmethod
    def unique_hash(cls, name):
        return name

    @classmethod
    def unique_filter(cls, query, name):
        return query.filter(Speaker.name == name)

    def __repr__(self):
        return '<Speaker id={}, name={}>'.format(self.id, self.name)


video_tag = Table('video_tag', Base.metadata,
                  Column('tag_id', Integer, ForeignKey('tags.id')),
                  Column('video_id', Integer, ForeignKey('videos.id')))


class Tag(DBMixin, Base):
    tag = Column(String(200), unique=True)

    def __init__(self, tag):
        self.tag = tag

    @classmethod
    def unique_hash(cls, tag):
        return tag

    @classmethod
    def unique_filter(cls, query, tag):
        return query.filter(Tag.tag == tag)

    def __repr__(self):
        return '<Tag id={}, tag={}>'.format(self.id, self.tag)


class RelatedURL(DBMixin, Base):
    video_id = Column(Integer, ForeignKey('videos.id'))
    description = Column(String(200))
    url = Column(String(2000))

    def __init__(self, description, url):
        self.description = description
        self.url = url

    def __repr__(self):
        return '<RelatedURL id={}, description={}>'.format(
                self.id, self.description)


class Video(DBMixin, Base):
    title = Column(String(200))
    source_url = Column(String(2000))
    speakers = relationship("Speaker", secondary=video_speaker,
                            lazy='subquery', backref="videos")
    old_id = Column(Integer)
    old_date_created = Column(String(200))
    old_date_recorded = Column(String(200))
    old_date_modified = Column(String(200))
    category = Column(String(200))
    description = Column(String(5000))
    summary = Column(String(5000))
    tags = relationship("Tag", secondary=video_tag,
                        lazy='subquery', backref="videos")
    duration = Column(Integer)
    language = Column(String(50))
    copyright = Column(String(200))
    slug = Column(String(2000))
    related_urls = relationship("RelatedURL", lazy='subquery')
    state = Column(String(200))
    quality_notes = Column(String(5000))
    thumbnail_url = Column(String(2000))
    embed = Column(String(5000))
    video_flv_download_only = Column(Boolean)
    video_flv_length = Column(String(20))
    video_flv_url = Column(String(2000))
    video_mp4_download_only = Column(Boolean)
    video_mp4_length = Column(String(20))
    video_mp4_url = Column(String(2000))
    video_ogv_download_only = Column(Boolean)
    video_ogv_length = Column(String(20))
    video_ogv_url = Column(String(2000))
    video_webm_download_only = Column(Boolean)
    video_webm_length = Column(String(20))
    video_webm_url = Column(String(2000))
    whiteboard = Column(String(5000))

    def __init__(self, old_id=None, title=None, source_url=None, speakers=[],
                 old_date_created=None, old_date_recorded=None,
                 old_date_modified=None,
                 slug=None, category=None, description=None, summary=None,
                 tags=[], duration=None, language=None, copyright=None,
                 related_urls=[], state=None, quality_notes=None,
                 thumbnail_url=None, embed=None, video_flv_download_only=None,
                 video_flv_length=None, video_flv_url=None,
                 video_mp4_download_only=None, video_mp4_length=None,
                 video_mp4_url=None, video_ogv_download_only=None,
                 video_ogv_length=None, video_ogv_url=None,
                 video_webm_download_only=None, video_webm_length=None,
                 video_webm_url=None, whiteboard=None):
        self.old_id = old_id
        self.title = title
        self.source_url = source_url
        self.speakers = []
        self.old_date_created = old_date_created
        self.old_date_recorded = old_date_recorded
        self.old_date_modified = old_date_modified
        self.category = category
        self.description = description
        self.summary = summary
        self.tags = []
        self.duration = duration
        self.language = language
        self.copyright = copyright
        self.slug = slug
        self.related_urls = [RelatedURL(item['description'], item['url'])
                             for item in related_urls]
        self.state = state
        self.quality_notes = quality_notes
        self.thumbnail_url = thumbnail_url
        self.embed = embed
        self.video_flv_download_only = video_flv_download_only
        self.video_flv_length = video_flv_length
        self.video_flv_url = video_flv_url
        self.video_mp4_download_only = video_mp4_download_only
        self.video_mp4_length = video_mp4_length
        self.video_mp4_url = video_mp4_url
        self.video_ogv_download_only = video_ogv_download_only
        self.video_ogv_length = video_ogv_length
        self.video_ogv_url = video_ogv_url
        self.video_webm_download_only = video_webm_download_only
        self.video_webm_length = video_webm_length
        self.video_webm_url = video_webm_url
        self.whiteboard = whiteboard

        with session_scope() as session:
            self.tags = [Tag.as_unique(session, tag=tag) for tag in tags]
            self.speakers = [Speaker.as_unique(session, name=speaker)
                             for speaker in speakers]

    def __repr__(self):
        return '<Video id={}, title={}>'.format(self.id, self.title)


def get_videos(filename='./data/raw_api_data.json'):
    """
    Gets videos from `filename` and yields them individually.
    """
    with open(filename) as f:
        data = json.load(f)

    for video in data:
        yield video


def create_db(videos):
    """
    Takes an iterable, `videos`, and populates the db.
    """
    if os.path.exists(DB_PATH):
        try:
            with session_scope() as session:
                existing_videos = Video.get(session=session)
                # session.expunge_all()
                if len(videos) == len(existing_videos):
                    logging.info("Raw data already exists. Skipping create.")
                    return existing_videos
                else:
                    logging.info("Deleting incomplete raw data.")
                    os.remove(DB_PATH)
        except NoResultFound:
            logging.info("Deleting incomplete raw data.")
            os.remove(DB_PATH)

    Base.metadata.create_all(engine)

    for video in videos:
        old_video = Video(
            old_id=video['id'],
            title=video['title'],
            source_url=video['source_url'],
            speakers=video['speakers'],
            old_date_created=video['added'],
            old_date_recorded=video['recorded'],
            old_date_modified=video['updated'],
            slug=video['slug'],
            category=video['category'],
            description=video['description'],
            summary=video['summary'],
            tags=video['tags'],
            duration=video['duration'],
            language=video['language'],
            copyright=video['copyright_text'],
            related_urls=video['related_urls'],
            state=video['state'],
            quality_notes=video['quality_notes'],
            thumbnail_url=video['embed'],
            embed=video['embed'],
            video_flv_download_only=video['video_flv_download_only'],
            video_flv_length=video['video_flv_length'],
            video_flv_url=video['video_flv_url'],
            video_mp4_download_only=video['video_mp4_download_only'],
            video_mp4_length=video['video_mp4_length'],
            video_mp4_url=video['video_mp4_url'],
            video_ogv_download_only=video['video_ogv_download_only'],
            video_ogv_length=video['video_ogv_length'],
            video_ogv_url=video['video_ogv_url'],
            video_webm_download_only=video['video_webm_download_only'],
            video_webm_length=video['video_webm_length'],
            video_webm_url=video['video_webm_url'],
            whiteboard=video['whiteboard']
            )
        with session_scope() as session:
            old_video = Video.create(old_video, session)

    # with session_scope() as session:
    #     videos = Video.get(session=session)
    # #     session.expunge_all()
    #     return videos

if __name__ == "__main__":
    # create_db()
    with session_scope() as session:
        q = Speaker.get(name='Clayton Parker', session=session)
        print(q)

        q = Video.get(title='So you think you can PDB?', session=session)
        print(q)

        q = Video.get(id=q[0].id, session=session)
        print(q.speakers)
