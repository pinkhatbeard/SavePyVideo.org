#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
download_non_youtube.py
~~~~~~~~~~~~~~~~~

**WARNGING** This was written and hopefully did it's job before a big refactor.
I cannot guarantee that it will work as written.

:copyright: (c) 2016
 by Cameron Dershem.
:license: see TOPMATTER
:source: github.com/cldershem/repo
"""

import requests
import time
import json
import os
import asyncio
import functools
import logging

logging.basicConfig(level=logging.INFO,
                    format='[%(levelname)s: %(message)s]',
                    filename='./data/download.log')
logger = logging.getLogger('requests').setLevel(logging.WARNING)


url_types = [
        'video_flv_url',
        'video_mp4_url',
        'video_ogv_url',
        'video_webm_url',
        'source_url',
        'embed'
        ]

sources = [
        'rackcdn.com',
        'archive.org',
        'linux.org',
        'blip.tv',
        'c3voc.de',
        'youtube',
        'youtu.be',
        'video.google',
        'vimeo',
        ]


def get_video_stats(videos):
    rackspace = 0
    archive = 0
    linux = 0
    blip = 0
    c3voc = 0
    other = 0
    no_source = 0
    no_url_no_source = 0
    youtube = 0
    google_video = 0
    vimeo = 0

    for video in videos:
        if video['video_flv_url']:
            video_url = video['video_flv_url']
        if video['video_mp4_url']:
            video_url = video['video_mp4_url']
        if video['video_ogv_url']:
            video_url = video['video_ogv_url']
        if video['video_webm_url']:
            video_url = video['video_webm_url']
        if video['source_url']:
            video_url = video['source_url']

        if video_url:
            if 'rackcdn.com' in video_url:
                rackspace += 1
            elif 'archive.org' in video_url:
                archive += 1
            elif 'linux.org' in video_url:
                linux += 1
            elif 'blip.tv' in video_url:
                blip += 1
            elif 'c3voc.de' in video_url:
                c3voc += 1
            elif 'youtube.com' in video_url or 'youtu.be' in video_url:
                youtube += 1
            elif 'video.google' in video_url:
                google_video += 1
            elif 'vimeo.com' in video_url:
                vimeo += 1
            else:
                other += 1
                print(video_url)
        else:
            no_url_no_source += 1
            print(video['title'])

    print('rackspace: {}'.format(rackspace))
    print('archive: {}'.format(archive))
    print('linux: {}'.format(linux))
    print('blip: {}'.format(blip))
    print('c3voc: {}'.format(c3voc))
    print('other: {}'.format(other))
    print('youtube: {}'.format(youtube))
    print('google: {}'.format(google_video))

    total_known = (rackspace + archive + other + linux + blip + c3voc +
                   youtube + google_video + vimeo)
    print('total known: {}'.format(total_known))
    print('total unknown: {}'.format(len(videos)-total_known))

    print(no_source)
    print(no_url_no_source)
    flatten_data()


def get_content_length(url):
    try:
        r = requests.head(url, timeout=1)
    except requests.exceptions.Timeout as e:
        return None

    return int(r.headers['content-length'])


def convert_to_mb(length):
    kb = (length / 1024)
    mb = (kb / 1024)
    # gb = (mb / 1024)

    return mb


def get_sizes_and_sort():
    if os.path.exists('./data/need_to_download.txt'):
        os.remove('./data/need_to_download.txt')
    if os.path.exists('./data/cannot_download.txt'):
        os.remove('./data/cannot_download.txt')

    # bad_urls = []

    with open('./data/non_youtube.json') as f:
        data = json.load(f)

    for video in data:
        for url_type in url_types:
            if video[url_type]:
                url = video[url_type]
                break
        if 'archive.org' in url:
            with open('./data/cannot_download.txt', 'a') as f:
                f.write('archive: {}\n'.format(url))
            continue

        content_length = get_content_length(url)

        if not content_length:
            with open('./data/cannot_download.txt', 'a') as f:
                f.write('bad: {}\n'.format(url))
            continue

        mb = convert_to_mb(content_length)

        with open('./data/need_to_download.txt', 'a') as f:
            f.write("{}; {}\n".format(url, mb))
        time.sleep(1)


async def download_video(url):
    filename = url.rsplit('/', 1)[-1]
    base_dir = '/media/cldershem/5TBonerz/PyVideos'

    r = await loop.run_in_executor(
            None, functools.partial(requests.get, url=url, stream=True))
    if r.status_code != requests.codes.ok:
        error = r.json()
        logging.warning(error, filename)
        return None
    else:
        with open('{}/{}'.format(base_dir, filename), 'wb') as f:
            for chunk in r.iter_content(1024):
                f.write(chunk)
    logging.info(filename)


def write_data(data, filename=None):
    data = json.dumps(data, indent=4, sort_keys=True)

    with open(filename, 'w') as f:
        f.write(data)


def find_non_youtube(videos):
    youtube_urls = [
            'youtube',
            'youtu.be',
            'video.google'
            ]
    non_youtube = []
    for video in videos:
        youtube = None
        for url_type in url_types:
            if video[url_type]:
                video_url = video[url_type]
                if any(True for url in youtube_urls if url in video_url):
                    youtube = True
        if not youtube:
            non_youtube.append(video)

    write_data(non_youtube, './data/non_youtube.json')
    return non_youtube


def get_video_to_download():
    with open('./data/need_to_download.txt') as f:
        data = f.readlines()
        data = [line.rstrip('\n') for line in data]

        for item in data[500:len(data)]:
            url, size = item.split('; ')
            yield url

async def main():
    for url in get_video_to_download():
        await download_video(url)


if __name__ == "__main__":
    # get_sizes_and_sort()

    # loop = asyncio.get_event_loop()
    # loop.run_until_complete(main())
    # loop.close()
    videos = './data/raw_api_data.json'
    with open('./data/non_youtube.json') as f:
        videos = json.load(f)
        get_video_stats(videos)
