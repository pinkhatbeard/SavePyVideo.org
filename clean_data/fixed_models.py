import os
import logging
import datetime
from contextlib import contextmanager
from urllib.parse import urlparse, parse_qs

from sqlalchemy import (create_engine, Column, Integer, String,
                        ForeignKey, DateTime, Table)
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.ext.hybrid import hybrid_property


BASE_DIR = os.path.abspath(os.path.dirname(__file__))
DB_PATH = os.path.join(BASE_DIR, './data/db.sqlite')
SQLALCHEMY_DATABASE_URI = ('sqlite:///' + DB_PATH)
engine = create_engine(SQLALCHEMY_DATABASE_URI)
Session = sessionmaker(bind=engine)
Base = declarative_base()


@contextmanager
def session_scope():
    session = Session()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()


class DBMixin(object):
    """
    Mixin for basic DB methods and attributes.
    """
    @declared_attr
    def __tablename__(cls):
        return "{}s".format(cls.__name__.lower())

    id = Column(Integer, primary_key=True)
    date_created = Column(DateTime, nullable=False,
                          default=datetime.datetime.utcnow)
    date_modified = Column(DateTime, nullable=False,
                           default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)

    @classmethod
    def get(cls, id=None, session=None, **kwargs):
        """
        If `id` passed queries for id and returns exactly 1 result.
        If anything else is passed, returns a list of items matching query.
        """
        if not id and not kwargs:
            obj = session.query(cls).all()
        if id:
            obj = session.query(cls).filter_by(id=id).first()
        if kwargs:
            # check if active
            obj = [item for item in
                   session.query(cls).filter_by(**kwargs).all()]
        if not obj:
            logging.info('No results found.')
            raise NoResultFound
        return obj

    @classmethod
    def create(cls, obj, session=None):
        """
        Adds object to sessoin and returns object from DB.

        Was used to simplifiy adding to DB and make it harder to forget to
        close connection. It also returned the object after it was added to
        the DB with it's `id`. However this may or may not work correctly now.
        """
        session.add(obj)
        session.flush()
        obj = cls.get(id=obj.id, session=session)
        logging.info('Created item: {}'.format(obj))
        return obj

    @classmethod
    def _unique(cls, session, arg, kwargs):
        """
        Creates or gets unique object from DB.

        Checks uniqueness based upon `cls.unique_hash` and `cls.unique_filter`.
        If unique it adds it to DB. If not unique it returns the previous item
        already in DB.
        """
        cache = getattr(session, '_unique_cache', None)

        if cache is None:
            session._unique_cache = cache = {}

        key = (cls, cls.unique_hash(*arg, **kwargs))
        if key in cache:
            return cache[key]
        else:
            with session.no_autoflush:
                q = session.query(cls)
                q = cls.unique_filter(q, *arg, **kwargs)
                obj = q.first()
                if not obj:
                    obj = cls(*arg, **kwargs)
                    session.add(obj)
            cache[key] = obj
            return obj

    @classmethod
    def unique_hash(cls, *arg, **kwargs):
        """
        Defines what makes an item unique.
        """
        raise NotImplementedError()

    @classmethod
    def unique_filter(cls, query, *arg, **kwargs):
        """
        Creates query to test uniqueness of object.
        """
        raise NotImplementedError()

    @classmethod
    def as_unique(cls, session, *arg, **kwargs):
        """
        Gets or creats a unique object in the DB.
        """
        return cls._unique(session, arg, kwargs)

    def __repr__(self):
        cls = self.__class__.__name__
        return '<{} id={}>'.format(cls, self.id)

    def __str__(self):
        cls = self.__class__.__name__
        return '<{} id={}>'.format(cls, self.id)


video_speaker = Table('video_speaker', Base.metadata,
                      Column('speaker_id', Integer, ForeignKey('speakers.id')),
                      Column('video_id', Integer, ForeignKey('videos.id')))


class Speaker(DBMixin, Base):
    name = Column(String(200), unique=True)

    def __init__(self, name):
        self.name = name

    @classmethod
    def unique_hash(cls, name):
        return name

    @classmethod
    def unique_filter(cls, query, name):
        return query.filter(Speaker.name == name)

    def __repr__(self):
        return '<Speaker id={}, name={}>'.format(self.id, self.name)


video_tag = Table('video_tag', Base.metadata,
                  Column('tag_id', Integer, ForeignKey('tags.id')),
                  Column('video_id', Integer, ForeignKey('videos.id')))


class Tag(DBMixin, Base):
    tag = Column(String(200), unique=True)

    def __init__(self, tag):
        self.tag = tag

    @classmethod
    def unique_hash(cls, tag):
        return tag

    @classmethod
    def unique_filter(cls, query, tag):
        return query.filter(Tag.tag == tag)

    def __repr__(self):
        return '<Tag id={}, tag={}>'.format(self.id, self.tag)


class RelatedURL(DBMixin, Base):
    video_id = Column(Integer, ForeignKey('videos.id'))
    description = Column(String(200))
    url = Column(String(2000))

    def __init__(self, description, url):
        self.description = description
        self.url = url

    def __repr__(self):
        return '<RelatedURL id={}, description={}>'.format(
                self.id, self.description)


class RelatedContent(DBMixin, Base):
    video_id = Column(Integer, ForeignKey('videos.id'))
    description = Column(String(200))
    content = Column(String(5000))

    def __init__(self, description, content):
        self.description = description
        self.content = content

    def __repr__(self):
        return '<RelatedURL id={}, description={}>'.format(
                self.id, self.description)


class Video(DBMixin, Base):
    title = Column(String(200))
    _source_url = Column("source_url", String(2000))
    speakers = relationship("Speaker", secondary=video_speaker,
                            backref="videos")
    # orig_date_created = Column(DateTime)
    # orig_date_recorded = Column(DateTime)
    # orig_date_modified = Column(DateTime)
    # event = relationship("Event")
    year = Column(DateTime)
    _event = Column(String(200))
    description_long = Column(String(5000))
    description_short = Column(String(5000))
    tags = relationship("Tag", secondary=video_tag, backref="videos")
    duration = Column(Integer)
    language = Column(String(50))
    copyright = Column(String(200))
    slug = Column(String(2000))
    related_urls = relationship("RelatedURL")
    related_content = relationship("RelatedContent")
    notes = Column(String(5000))
    quality_notes = Column(String(5000))
    _thumbnail_url = Column("thumbnail", String(2000))

    def __init__(self, title=None, source_url=None, speakers=[],
                 date_created=None, date_recorded=None, date_modified=None,
                 slug=None, event=None, description_long=None,
                 description_short=None, tags=[], duration=None,
                 language=None, copyright=None, related_urls=[],
                 related_content=[], notes=None, quality_notes=None,
                 thumbnail_url=None):
        self.title = title
        # self.orig_date_created = date_created
        # self.orig_date_recorded = date_recorded
        # self.orig_date_modified = date_modified
        self.event = event
        self.description_long = description_long
        self.description_short = description_short
        self.duration = duration
        self.language = language
        self.copyright = copyright
        self.slug = slug
        self.related_urls = [RelatedURL(url.description, url.url)
                             for url in related_urls]
        self.related_content = [RelatedContent(content)
                                for content in related_content]
        self.notes = notes
        self.quality_notes = quality_notes
        self.thumbnail_url = thumbnail_url
        self.source_url = source_url

        with session_scope() as session:
            self.tags = [Tag.as_unique(session, tag=tag) for tag in tags]
            self.speakers = [Speaker.as_unique(session, name=speaker)
                             for speaker in speakers]

    @property
    @hybrid_property
    def event(self):
        return self._event

    @event.setter
    def event(self, event):
        last_chars = event.split()[-1]

        try:
            year = datetime.datetime.strptime(last_chars, '%Y')
        except ValueError:
            # self._event = Event(event)
            self._event = event
        else:
            event = event[:-4].rstrip()
            # self._event = Event(event, year=year)
            self._event = event
            self.year = year

    @property
    def source_url(self):
        return self._source_url

    @source_url.setter
    def source_url(self, value):
        # TODO: if no source_url and video_download_mp4:
        if value:
            url = urlparse(value)

            if 'youtu.be' in url.netloc or 'youtube.com' in url.netloc:
                base_url = 'https://www.youtube.com'
                if 'youtu.be' in url.netloc:
                    video_id = url.path.strip('/')
                else:
                    video_id = parse_qs(url.query)['v'][0]

                self._source_url = "{}/watch?v={}".format(base_url, video_id)
            else:
                self._source_url = value
        else:
            with open('./data/no_source.txt', 'a+') as f:
                f.write(self.title + '\n')
            self._source_url = None

    @property
    def thumbnail_url(self):
        return self._thumbnail_url

    @thumbnail_url.setter
    def thumbnail_url(self, value):
        if value:
            self._thumbnail_url = value
            # url = self.source_url
            # if 'youtu.be' in url or 'youtube.com' in url:
            #     # base_url = 'http://img.youtube.com/vi/'
            #     # if 'youtu.be' in urlparse(url).netloc:
            #     #     video_id = url.path.strip('/')
            #     # else:
            #     #     video_id = parse_qs(url.query)['v'][0]
            #     # self._thumbnail_url = "{}/{}/0.jpg".format(
            #     #     base_url, video_id)
            #     # print(self.thumbnail_url)
            # else:
            #     print(value)
            #     self._thumbnail_url = value
        else:
            self._thumbnail_url = None

    def __repr__(self):
        return '<Video id={}, title={}>'.format(self.id, self.title)

    def __str__(self):
        return '<Video id={}, title={}>'.format(self.id, self.title)


# def get_videos():
#     with old_models.session_scope() as session:
#         videos = session.query(old_models.Video).all()
#         # videos = old_models.Video.get(session=session)

#         # videos = videos[:500]

#         for video in videos:
#             yield video


def create_db(videos):
    """
    Creates DB from data in `get_videos()`.
    """
    if os.path.exists(DB_PATH):
        try:
            with session_scope() as session:
                existing_videos = Video.get(session=session)
                session.expunge_all()
                if len(videos) == len(existing_videos):
                    logging.info("Clean data already exists. Skipping create.")
                    return existing_videos
                else:
                    logging.info("Deleting incomplete clean data.")
                    os.remove(DB_PATH)
        except NoResultFound:
            logging.info("Deleting incomplete clean data.")
            os.remove(DB_PATH)

    Base.metadata.create_all(engine)

    for video in videos:
        new_video = Video(
            title=video.title,
            source_url=video.source_url,
            speakers=[speaker.name for speaker in video.speakers],
            date_created=video.old_date_created,
            date_recorded=video.old_date_recorded,
            date_modified=video.old_date_modified,
            slug=video.slug,
            event=video.category,
            description_long=video.description,
            description_short=video.summary,
            tags=[tag.tag for tag in video.tags],
            duration=video.duration,
            language=video.language,
            copyright=video.copyright,
            related_urls=[url for url in video.related_urls],
            notes=video.whiteboard,
            quality_notes=video.quality_notes,
            thumbnail_url=video.thumbnail_url
            )
        with session_scope() as session:
            new_video = Video.create(new_video, session)

            # if not video.source_url:
            #     with open('./data/no_source.txt', 'a+') as f:
            #         f.write(video.title + '\n')
            #     # if video.video_flv_url:
            #     #     flv = video.video_flv_url
            #     #     print(flv)
            #     # if video.video_mp4_url:
            #     #     mp4 = video.video_mp4_url
            #     #     print(mp4)
            #     # if video.video_ogv_url:
            #     #     ogv = video.video_ogv_url
            #     #     print(ogv)
            #     # if video.video_webm_url:
            #     #     webm = video.video_webm_url
            #     #     print(webm)
            # # if new_video.source_url:
            # #     print(new_video.source_url)


def test_queries():
    with session_scope() as session:
        q = Video.get(id=97)
        print(q.source_url)

        q = Video.get(id=187)
        print(q.source_url)

        q = Video.get(id=189)
        print(q.source_url)

        q = Speaker.get(name='Clayton Parker', session=session)
        print(q)

        q = Video.get(title='So you think you can PDB?', session=session)
        print(q)

        q = Video.get(id=q[0].id, session=session)
        print(q.speakers)


if __name__ == "__main__":
    if os.path.exists('./data/no_source.txt'):
        os.remove('./data/no_source.txt')
    create_db()
    # test_queries()
