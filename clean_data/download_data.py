#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
download_data.py
~~~~~~~~~~~~~~~~~

Gets data from api and saves it to a flat file.

:copyright: (c) 2016 by Cameron Dershem.
:license: see TOPMATTER
:source:
"""
import os
import json
import logging

import requests

import raw_models
import fixed_models


logging.basicConfig(level=logging.INFO, format='[%(levelname)s]: %(message)s')
logging.getLogger('requests').setLevel(logging.WARNING)


class PyVideoAPI(object):
    base_url = 'http://pyvideo.org/api/v2/'

    @staticmethod
    def get_page(url=None, page=None):
        if page:
            url += '?page={}'.format(page)

        data = requests.get(url).json()
        logging.info("Got {}.".format(url))

        return data

    @staticmethod
    def get_all_pages(url=None):
        while True:
            page = PyVideoAPI.get_page(url=url)
            yield page['results']
            if page['next']:
                url = page['next']
            else:
                return

    @staticmethod
    def total_videos():
        url = PyVideoAPI.base_url + 'video/'
        return int(PyVideoAPI.get_page(url=url)['count'])


def write_data(data, filename=None):
    data = json.dumps(data, indent=4, sort_keys=True)

    with open(filename, 'w') as f:
        f.write(data)


def download_video_data(filename=None):
    logging.info("Downloading videos.")

    url = PyVideoAPI.base_url + 'video'
    if not os.path.exists(filename):
        write_data([], filename=filename)

    for page in PyVideoAPI.get_all_pages(url=url):
        for video in page:
            with open(filename) as f:
                data = json.load(f)
            data.append(video)
            write_data(data, filename=filename)


def get_api_data():
    filename = os.path.join('data', 'raw_api_data.json')

    if os.path.exists(filename):
        try:
            with open(filename) as f:
                existing_data = json.load(f)
        except json.decoder.JSONDecodeError:
            logging.info("Existing data is not valid JSON.")
            os.remove(filename)
        if len(existing_data) != PyVideoAPI.total_videos():
            logging.info("Deleting incomplete data.")
            os.remove(filename)
        else:
            logging.info("Data already exists. Skipping download.")
            return existing_data

    download_video_data(filename=filename)

    with open(filename) as f:
        return json.load(f)


if __name__ == "__main__":
    BASE_DIR = os.path.abspath(os.path.dirname(__file__))
    data_dir = os.path.join(BASE_DIR, 'data')
    if not data_dir:
        os.mkdir(data_dir)

    videos = get_api_data()
    assert len(videos) == PyVideoAPI.total_videos()

    with raw_models.session_scope() as raw_session:
        raw_models.create_db(videos)
        raw_db_videos = raw_models.Video.get(session=raw_session)
        raw_session.expunge_all()
    # assert random video == random db video
    assert len(videos) == len(raw_db_videos)

    with fixed_models.session_scope() as fixed_session:
        fixed_models.create_db(raw_db_videos)
        fixed_db_videos = fixed_models.Video.get(session=fixed_session)
    # assert random video == random db video

    assert len(videos) == len(fixed_db_videos)
